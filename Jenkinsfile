def resolveEnvironment(environment) {
    switch (environment) {
        case "development":
            return "dev"
        case "testing":
            return "tst"
        case "acceptance":
            return "uat"
        case "master":
            return "prd"
        default:
            return "dev"
    }
}

aws_env = resolveEnvironment(params.ENVIRONMENT)

def buildDeployLatest(args = [:]) {

  // call release function
  this.release(args)

  stage('publish built artifacts') {
    publishWebArtifacts()
  }

  // deploy application within commons build pipeline
  stage('deploy development snapshot') {
      deploy()
  }
}

/**
 * Invoked by release jobs.
*/
def release(args = [:]) {

  // call npm to install dependencies from shared ci cache
  // or private/public registries
  stage('install dependencies') {
    sh(script: 'npm install --prefer-offline')
  }

  stage('run build') {
    // env.PUBLIC_URL = args.baseHref ?: '/'
    def baseHref = args.baseHref ?: '/'
    sh(script: "npm run build -- --base-href ${baseHref} --env ${aws_env} --output-path build-prd")
    sh(script: "npm run build -- --base-href ${baseHref} --env ${aws_env} --output-path build-dev")
  }
}

/**
 * Invoked by quality-gate jobs.
*/
def qualityGate(args = [:]) {

    // call npm to install dependencies from shared ci cache
    // or private/public registries
    stage('install dependencies') {
      sh(script: 'npm install --prefer-offline')
    }

    // call npm to run tests
    // stage('run tests') {
    //     sh(script: 'npm run test')
    // }
}

/**
 * Invoked by release job to determine application's
 * non-production build directory.
*/
def applicationNonProductionBinDir() { 'build-dev' }

/**
 * Invoked by release job to determine application's
* production build directory.
//  */
def applicationProductionBinDir() { 'build-prd' }

/**
 * Invoked by release job to determine application's name.
*/
def applicationName() { 'accounting-management-frontend' }

/**
 * Invoked by release job to determine public application's base href.
 */
def publicWebApplicationBaseHref() { '/' }

/**
 * Invoked by release job to determine crm application's base href.
 */
def crmWebApplicationBaseHref() { '/gestao-contabil/' }

/**
 * Invoked by release job to determine internal application's base href.
 */
def internalWebApplicationBaseHref() { '/admin/gestao-contabil/' }

/**
 * Invoked by release job to determine private application's base href.
 */
def privateWebApplicationBaseHref() { '/gestao-contabil/' }

// make sure we return an instance of this scripts so jenkins pipeline
// can call our functions
return this
