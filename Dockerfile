FROM nginx
MAINTAINER Sicredi Digital
COPY docker-templates/default.conf /etc/nginx/conf.d/default.conf
COPY dist/ /usr/share/nginx/html
EXPOSE 8080
