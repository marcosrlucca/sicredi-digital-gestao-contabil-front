import {NgModule} from '@angular/core';
import {NgbCarouselModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashboardComponent} from './dashboard.component';
import {StatModule} from '../../shared';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/modules/shared/shared.module';

@NgModule({
    imports: [
        SharedModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        DashboardRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        StatModule
    ],
    declarations: [
        DashboardComponent
    ]
})
export class DashboardModule {
}
