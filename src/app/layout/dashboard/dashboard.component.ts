import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {Authorization} from '../../shared/domain/authorization.model';
import {UserService} from '../../configuration/user.service';
import {StatusManagementErrorService} from '../../shared/services/status-management-error.service';
import {Router} from '@angular/router';
import {StatusEvaluationIntegrationService} from '../../shared/services/status-evaluation-integration.service';
import {StatusTopazMovementService} from '../../shared/services/status-topaz-movement.service';
import {isNullOrUndefined} from 'util';
import {StatusMassiveRegistrationService} from '../../shared/services/status-massive-registration.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {

    pushRightClass = 'push-right';

    constructor(private userService: UserService,
                public statusManagementErrorService: StatusManagementErrorService,
                public statusTopazMovementService: StatusTopazMovementService,
                public statusEvaluationIntegrationService: StatusEvaluationIntegrationService,
                public statusMassiveRegistration: StatusMassiveRegistrationService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                public router: Router) {
    }

    ngOnInit(): void {
        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });
    }

    goToEvaluationIntegrationToViewErrors() {
        this.statusEvaluationIntegrationService.chooseError();
        this.router.navigate(['/gestao-erro/avaliacao-integracao']);
    }

    goToTopazMovementToViewNotIntegrated() {
        this.statusTopazMovementService.chooseNotIntegrated();
        this.router.navigate(['/gestao-erro/erro-falta-historico-topaz']);
    }

    goToManagementErrorToViewErrors() {
        this.statusManagementErrorService.chooseError();
        this.router.navigate(['/gestao-erro/avaliacao-lote']);
    }

    goToManagementErrorToViewNotSent() {
        this.statusManagementErrorService.chooseNotSent();
        this.router.navigate(['/gestao-erro/avaliacao-lote']);
    }

    goToManagementErrorToViewDisconsidered() {
        this.statusManagementErrorService.chooseDisconsidered();
        this.router.navigate(['/gestao-erro/avaliacao-lote']);
    }

    goToLogErrorKettle() {
        this.router.navigate(['/gestao-erro/erro-sistemico']);
    }

    goToMassiveRegistration() {
        this.statusMassiveRegistration.chooseError();
        this.router.navigate(['/engine/cadastramento-massivo-inconsistente']);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    getTotalErrorAllotment() {
        if (isNullOrUndefined(this.statusManagementErrorService.status) || isNullOrUndefined(this.statusManagementErrorService.status.totalError)) {
            return 0;
        }
        return this.statusManagementErrorService.status.totalError;
    }

    getTotalNotSentAllotment() {
        if (isNullOrUndefined(this.statusManagementErrorService.status) || isNullOrUndefined(this.statusManagementErrorService.status.totalNotSent)) {
            return 0;
        }
        return this.statusManagementErrorService.status.totalNotSent;
    }

    getTotalDisconsideredAllotment() {
        if (isNullOrUndefined(this.statusManagementErrorService.status) || isNullOrUndefined(this.statusManagementErrorService.status.totalDisconsidered)) {
            return 0;
        }
        return this.statusManagementErrorService.status.totalDisconsidered;
    }

    getTotalLogKettle() {
        if (isNullOrUndefined(this.statusManagementErrorService.status) || isNullOrUndefined(this.statusManagementErrorService.status.totalLogKettle)) {
            return 0;
        }
        return this.statusManagementErrorService.status.totalLogKettle;
    }

    getTotalErrorEvaluationIntegration() {
        if (isNullOrUndefined(this.statusEvaluationIntegrationService) || isNullOrUndefined(this.statusEvaluationIntegrationService.totalError)) {
            return 0;
        }
        return this.statusEvaluationIntegrationService.totalError;
    }

    getTotalNotIntegratedStatusTopazMovement() {
        if (isNullOrUndefined(this.statusTopazMovementService) || isNullOrUndefined(this.statusTopazMovementService.totalNotIntegrated)) {
            return 0;
        }
        return this.statusTopazMovementService.totalNotIntegrated;
    }

    getTotalMassiveRegistration() {
        if (isNullOrUndefined(this.statusMassiveRegistration) || isNullOrUndefined(this.statusMassiveRegistration.totalError)) {
            return 0;
        }
        return this.statusMassiveRegistration.totalError;
    }

}
