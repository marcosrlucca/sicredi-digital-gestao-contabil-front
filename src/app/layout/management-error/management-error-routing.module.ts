import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LogDetailComponent} from './analysis-kettle-log/log-detail/log-detail.component';
import {AnalysisKettleLogComponent} from './analysis-kettle-log/analysis-kettle-log.component';
import {ManagementErrorMainComponent} from './management-error-main.component';
import {AllotEvaluationComponent} from './allot-evaluation/allot-evaluation.component';
import {EvaluationIntegrationComponent} from './evaluation-integration/evaluation-integration.component';
import {TopazMovementComponent} from './topaz-movement/topaz-movement.component';
import { EventsEvaluationEngineComponent } from './events-evaluation-engine/events-evaluation-engine.component';
import { DetailsEvaluationEngineComponent } from './events-evaluation-engine/details-evaluation-engine/details-evaluation-engine.component';

const routes: Routes = [
    {
        path: '', component: ManagementErrorMainComponent
    },
    {
        path: 'avaliacao-lote', component: AllotEvaluationComponent
    },
    {
        path: 'erro-sistemico', component: AnalysisKettleLogComponent
    },
    {
        path: 'erro-sistemico/erro/:id', component: LogDetailComponent
    },
    {
        path: 'avaliacao-integracao', component: EvaluationIntegrationComponent
    },
    {
        path: 'erro-falta-historico-topaz', component: TopazMovementComponent
    },
    {
        path: 'avalicao-de-eventos-engine', component: EventsEvaluationEngineComponent
    },
    {
        path: 'avalicao-de-eventos-engine/detalhes', component: DetailsEvaluationEngineComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ManagementErrorRoutingModule {
}
