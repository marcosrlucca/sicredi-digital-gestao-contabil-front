export class TopazMovement {
    id: string;
    operation: string;
    codTransaction: string;
    dateDoc: string;
    dateDocToView: string;
    glAccount: string;
    itemText: string;
    debitCredit: string;
    integrate: string;
}
