import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MatDatepickerInputEvent, MatDialog, MatPaginator, MatSort} from '@angular/material';
import {tap} from 'rxjs/operators';
import {ToastsManager} from 'ng2-toastr';
import {isNullOrUndefined} from 'util';
import {merge} from 'rxjs/observable/merge';

import {TopazMovementDataSource} from './topaz-movement.data-source';
import {TopazMovementService} from './topaz-movement.service';
import {routerTransition} from '../../../router.animations';
import {StatusTopazMovementService} from '../../../shared/services/status-topaz-movement.service';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {UserService} from '../../../configuration/user.service';
import {Authorization} from '../../../shared/domain/authorization.model';

const SORT_ORDER_DEFAULT = 'dateDoc,desc';
const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
    selector: 'app-topaz-movement',
    templateUrl: './topaz-movement.component.html',
    styleUrls: ['./topaz-movement.component.scss'],
    animations: [routerTransition()]
})
export class TopazMovementComponent implements OnInit, AfterViewInit {

    displayedColumns = ['dateDoc', 'glAccount', 'itemText', 'operation', 'codTransaction', 'debitCredit'];
    dataSource: TopazMovementDataSource;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    form: FormGroup;
    selectedIntegratedType;
    selectedDebitCredit;
    integratedTypes = [];
    debitCreditTypes = [];
    startDate;
    endDate;
    startCalendar;
    endCalendar;
    operation;

    constructor(private formBuilder: FormBuilder,
                private topazMovementService: TopazMovementService,
                private statusTopazMovementService: StatusTopazMovementService,
                private toastr: ToastsManager,
                private userService: UserService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                public dialog: MatDialog) {
    }

    ngOnInit() {
        this.dataSource = new TopazMovementDataSource(this.topazMovementService);
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.startDate = new FormControl(new Date());
        this.endDate = new FormControl(new Date());

        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);

        this.sort.active = 'dateDoc';
        this.sort.direction = 'desc';

        if (this.statusTopazMovementService.isNotIntegratedChosed() && this.statusTopazMovementService.totalNotIntegrated > 0) {
            this.dataSource.loadContent('', 'N', '', '', '',
                SORT_ORDER_DEFAULT, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT);

            this.statusTopazMovementService.resetChoice();
        }

        this.populateSelects();

        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });

        this.form = this.formBuilder.group({operation: new FormControl('', []), });
    }

    ngAfterViewInit() {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page).pipe(tap(() => this.loadContentPage())).subscribe();
    }

    populateSelects() {
        this.topazMovementService.getIntegratedTypes().subscribe((item) => this.integratedTypes.push(item));
        this.topazMovementService.getDebitCreditTypes().subscribe((item) => this.debitCreditTypes.push(item));
    }

    loadContentPage() {
        this.dataSource = new TopazMovementDataSource(this.topazMovementService);

        if (!isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';

        this.dataSource.loadContent(
            !isNullOrUndefined(this.selectedDebitCredit) ? this.selectedDebitCredit : '',
            !isNullOrUndefined(this.selectedIntegratedType) ? this.selectedIntegratedType : '',
            !isNullOrUndefined(this.operation) ? this.operation : '',
            startDateSimplified !== '' ? startDateSimplified : '',
            endDateSimplified !== '' ? endDateSimplified : '',
            !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT,
            this.paginator.pageIndex, this.paginator.pageSize
        );
    }

    getDateOfMonth(dateToTransform: Date) {
        const dayStartDate = dateToTransform.getDate();
        const monthStartDate = dateToTransform.getMonth() + 1;
        const yearStartDate = dateToTransform.getFullYear();

        return `${dayStartDate}/${monthStartDate}/${yearStartDate}`;
    }

    onSubmit() {
        this.dataSource = new TopazMovementDataSource(this.topazMovementService);

        if (!isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';

        this.paginator.pageSize = PAGE_SIZE_DEFAULT;
        this.sort.active = 'dateDoc';
        this.sort.direction = 'desc';

        this.dataSource.loadContent(
            !isNullOrUndefined(this.selectedDebitCredit) ? this.selectedDebitCredit : '',
            !isNullOrUndefined(this.selectedIntegratedType) ? this.selectedIntegratedType : '',
            !isNullOrUndefined(this.operation) ? this.operation : '',
            startDateSimplified !== '' ? startDateSimplified : '',
            endDateSimplified !== '' ? endDateSimplified : '',
            SORT_ORDER_DEFAULT, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT
        );
    }

    startDateChange(chargeType: string, event: MatDatepickerInputEvent<Date>) {
        this.startDate.value = event.value;
    }

    endDateChange(chargeType: string, event: MatDatepickerInputEvent<Date>) {
        this.endDate.value = event.value;
    }

    reset() {
        this.selectedIntegratedType = '';
        this.selectedDebitCredit = '';
        this.operation = '';
        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
    }
}
