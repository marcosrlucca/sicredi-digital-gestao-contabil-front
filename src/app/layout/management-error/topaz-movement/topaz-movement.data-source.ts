import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {TopazMovement} from './topaz-movement.model';
import {TopazMovementService} from './topaz-movement.service';
import {Page} from '../../../shared/domain/page.model';

export class TopazMovementDataSource implements DataSource<TopazMovement> {

    itemsSubject = new BehaviorSubject<TopazMovement[]>([]);
    page = new Page();

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubjects.asObservable();

    public noDataFound = false;

    constructor(private evaluationIntegrationService: TopazMovementService) {
        this.page.pageSizeOptions = [5, 10, 20, 50];
    }

    connect(collectionViewer: CollectionViewer): Observable<TopazMovement[]> {
        return this.itemsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.itemsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadContent(debitcredit, integrated, operation, startDate, endDate, sortOrder, pageIndex, pageSize) {
        this.loadingSubjects.next(true);

        this.evaluationIntegrationService.find(debitcredit, integrated, operation, startDate, endDate, sortOrder, pageIndex, pageSize)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false)))
            .subscribe(response => {
                if (response !== null) {
                    this.itemsSubject.next(response['content']);
                    this.page = response;

                    if (this.page.totalElements >= 20) {
                        this.page.pageSizeOptions = [5, 10, 20, 50];
                    } else if (this.page.totalElements >= 10) {
                        this.page.pageSizeOptions = [5, 10, 20];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    } else {
                        this.page.pageSizeOptions = [5, 10];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    }

                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }
}
