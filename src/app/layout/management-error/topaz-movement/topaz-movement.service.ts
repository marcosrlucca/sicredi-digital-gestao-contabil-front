import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../../environments/environment';

@Injectable()
export class TopazMovementService {

    private pathGetEvaluations = `${environment._api_accounting_management_url}/topaz-movement`;

    constructor(private httpClient: HttpClient) {
    }

    public find(debitcredit, integrate, operation, startDate, endDate, sortOrder, pageIndex, pageSize): Observable<any> {
        return this.httpClient.get(this.pathGetEvaluations, {
            params: new HttpParams()
                .set('startdate', startDate)
                .set('enddate', endDate)
                .set('debitcredit', debitcredit)
                .set('integrate', integrate)
                .set('operation', operation)
                .set('sort', sortOrder)
                .set('page', pageIndex)
                .set('size', pageSize)
        });
    }

    public getIntegratedTypes() {
        return Observable.of(
            {value: 'S', viewValue: 'Sim'},
            {value: 'N', viewValue: 'Não'}
        );
    }

    public getDebitCreditTypes() {
        return Observable.of(
            {value: 'C', viewValue: 'Crédito'},
            {value: 'D', viewValue: 'Débito'}
        );
    }
}
