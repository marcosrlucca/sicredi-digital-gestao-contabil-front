import { EventsEvaluationService } from './events-evaluation-engine/events-evaluation-engine.service';
import {NgModule} from '@angular/core';

import {PageHeaderModule} from './../../shared';
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatInputModule, MatNativeDateModule, MatPaginatorIntl, MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule, MatSortModule, MatTableModule, MatChipsModule
} from '@angular/material';
import {NgbAlertModule, NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ManagementErrorRoutingModule} from './management-error-routing.module';
import {TextMaskModule} from 'angular2-text-mask';
import {PurgeDialogComponent} from './allot-evaluation/purge-dialog/purge-dialog.component';
import {UndoPurgeDialogComponent} from './allot-evaluation/undo-purge-dialog/undo-purge-dialog.component';
import {RemovalConfirmationDialogComponent} from './analysis-kettle-log/removal-confirmation-dialog/removal-confirmation-dialog.component';
import {LogDetailService} from './analysis-kettle-log/log-detail/log-detail.service';
import {AnalysisKettleLogService} from './analysis-kettle-log/analysis-kettle-log.service';
import {AnalysisKettleLogComponent} from './analysis-kettle-log/analysis-kettle-log.component';
import {LogDetailComponent} from './analysis-kettle-log/log-detail/log-detail.component';
import {ManagementErrorMainComponent} from './management-error-main.component';
import {SharedModule} from '../../shared/modules/shared/shared.module';
import {StatModule} from '../../shared/modules/stat/stat.module';
import {AllotEvaluationService} from './allot-evaluation/allot-evaluation.service';
import {AllotEvaluationComponent} from './allot-evaluation/allot-evaluation.component';
import {EvaluationIntegrationComponent} from './evaluation-integration/evaluation-integration.component';
import {EvaluationIntegrationService} from './evaluation-integration/evaluation-integration.service';
import {TopazMovementService} from './topaz-movement/topaz-movement.service';
import {TopazMovementComponent} from './topaz-movement/topaz-movement.component';
import {MatPaginatorIntlBr} from '../../shared/services/mat-paginator-intl-br';
import { ActNotActService } from '../act-not-act/act-not-act.service';
import { DateUtilService } from '../../../util/date-utils.service';
import { EventsEvaluationEngineComponent } from './events-evaluation-engine/events-evaluation-engine.component';
import { DetailsEvaluationEngineComponent } from './events-evaluation-engine/details-evaluation-engine/details-evaluation-engine.component';
import { PayloadDialogComponent } from './events-evaluation-engine/details-evaluation-engine/payload-dialog/payload-dialog.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
    imports:
        [
            SharedModule,
            FormsModule,
            TextMaskModule,
            ReactiveFormsModule,
            MatSelectModule,
            MatNativeDateModule,
            MatDatepickerModule,
            MatChipsModule,
            MatInputModule,
            MatDialogModule,
            MatButtonModule,
            MatTableModule,
            MatSortModule,
            MatCheckboxModule,
            MatTooltipModule,
            MatProgressSpinnerModule,
            MatPaginatorModule,
            NgbCarouselModule.forRoot(),
            NgbAlertModule.forRoot(),
            ManagementErrorRoutingModule,
            PageHeaderModule,
            StatModule
        ],
    entryComponents: [
        PurgeDialogComponent,
        UndoPurgeDialogComponent,
        RemovalConfirmationDialogComponent,
        PayloadDialogComponent,
    ],
    providers: [
        EvaluationIntegrationService,
        TopazMovementService,
        AllotEvaluationService,
        AnalysisKettleLogService,
        LogDetailService,
        ActNotActService,
        DateUtilService,
        EventsEvaluationService,
        {provide: MatPaginatorIntl, useClass: MatPaginatorIntlBr}
    ],
    declarations:
    [
        EvaluationIntegrationComponent,
        TopazMovementComponent,
        ManagementErrorMainComponent,
        AllotEvaluationComponent,
        PurgeDialogComponent,
        UndoPurgeDialogComponent,
        AnalysisKettleLogComponent,
        LogDetailComponent,
        RemovalConfirmationDialogComponent,
        EventsEvaluationEngineComponent,
        DetailsEvaluationEngineComponent,
        PayloadDialogComponent,
    ]
})
export class ManagementErrorModule {
}
