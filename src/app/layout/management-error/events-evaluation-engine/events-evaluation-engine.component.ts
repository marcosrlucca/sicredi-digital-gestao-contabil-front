import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {routerTransition} from '../../../router.animations';
import {ActNotActService} from '../../act-not-act/act-not-act.service';
import {DateUtilService} from '../../../../util/date-utils.service';

import {isNullOrUndefined} from 'util';
import * as moment from 'moment';
import {MatPaginator} from '@angular/material';
import {EventsEvaluationDataSource} from './events-evalution.data-source';
import {EventsEvaluationService} from './events-evaluation-engine.service';
import {SelectionModel} from '@angular/cdk/collections';
import {Router} from '@angular/router';

const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
  selector: 'app-events-evaluation-engine',
  templateUrl: './events-evaluation-engine.component.html',
  styleUrls: ['./events-evaluation-engine.component.scss'],
  animations: [routerTransition()]
})

export class EventsEvaluationEngineComponent implements OnInit {

  eventsEvaluationForm: FormGroup;
  displayedColumns = ['markedToDetails', 'company', 'system', 'baseDate', 'eventType', 'total',
                      'valids', 'errors', 'timestampIn', 'status'];
  selectedLogDetails = new Array<any>();
  dateIsValid = true;
  // checks = false;
  count = 0;
  data;

  dataSource: EventsEvaluationDataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  selection = new SelectionModel<any>(true, []);

  companies = [];
  systems = [];
  types = [];
  allStatus = [];
  selectedRow = [];

  constructor(
    private formBuilder: FormBuilder,
    private dateUtil: DateUtilService,
    private eventsEvaluationService: EventsEvaluationService,
    private actNotActService: ActNotActService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.dataSource = new EventsEvaluationDataSource(this.eventsEvaluationService);
    this.setEventsEvaluationEngineForm();
    this.populateSelects();
  }

  setEventsEvaluationEngineForm() {
    this.eventsEvaluationForm = this.formBuilder.group({
      company: this.formBuilder.control(''),
      system: this.formBuilder.control(''),
      type: this.formBuilder.control(''),
      status: this.formBuilder.control(''),
      startDate: this.formBuilder.control(''),
      endDate: this.formBuilder.control(''),
    });
  }

  populateSelects() {
    this.companies = this.actNotActService.getCompanies();
    this.systems = this.actNotActService.getSystems();
    this.eventsEvaluationService.getEvaluationEngineTypes().subscribe(item => this.types.push(item));
    this.eventsEvaluationService.getStatus().subscribe(item => this.allStatus.push(item));
  }

  onSubmit() {
    this.dataSource = new EventsEvaluationDataSource(this.eventsEvaluationService);

    this.data = {
      size: PAGE_SIZE_DEFAULT,
      page: PAGE_INDEX_DEFAULT,
      system: !isNullOrUndefined(this.eventsEvaluationForm.get('system').value) && this.eventsEvaluationForm.get('system').value !== '' &&
            this.eventsEvaluationForm.get('system').value ? this.eventsEvaluationForm.get('system').value : null,
      company: !isNullOrUndefined(this.eventsEvaluationForm.get('company').value) && this.eventsEvaluationForm.get('company').value !== '' &&
            this.eventsEvaluationForm.get('company').value ? this.eventsEvaluationForm.get('company').value : null,
      startDate: !isNullOrUndefined(this.eventsEvaluationForm.get('startDate').value) && this.eventsEvaluationForm.get('startDate').value !== '' &&
            this.eventsEvaluationForm.get('startDate').value ? moment(this.eventsEvaluationForm.get('startDate').value).format('YYYY-MM-DD') : null,
      endDate: !isNullOrUndefined(this.eventsEvaluationForm.get('endDate').value) && this.eventsEvaluationForm.get('endDate').value !== '' &&
            this.eventsEvaluationForm.get('endDate').value ? moment(this.eventsEvaluationForm.get('endDate').value).format('YYYY-MM-DD') : null,
      type: !isNullOrUndefined(this.eventsEvaluationForm.get('type').value) && this.eventsEvaluationForm.get('type').value !== '' &&
            this.eventsEvaluationForm.get('type').value ? this.eventsEvaluationForm.get('type').value : null,
      status: !isNullOrUndefined(this.eventsEvaluationForm.get('status').value) && this.eventsEvaluationForm.get('status').value !== '' &&
            this.eventsEvaluationForm.get('status').value ? this.eventsEvaluationForm.get('status').value : null,
    };

    this.dataSource.loadContent(
      this.data.size,
      this.data.page,
      this.data.system,
      this.data.company,
      null,
      this.data.endDate,
      this.data.startDate,
      this.data.type,
      this.data.status,
    );

    this.eventsEvaluationService.getGroupCount(
      null,
      this.data.company,
      this.data.endDate,
      this.data.startDate,
      this.data.system,
      this.data.status,
      this.data.type,
     ).subscribe(response => {
      this.count = response;
    });
  }

  reset() {
    this.formBuilder.control['company'].value = [];
    this.formBuilder.control['system'].value = [];
    this.formBuilder.control['type'].value = [];
    this.formBuilder.control['status'].value = [];
    this.formBuilder.control['startDate'].value = [];
    this.formBuilder.control['endDate'].value = [];
  }

  isValidDate(startDate: string, endDate: string): boolean {
    if (startDate === '' || endDate === '') {
      this.dateIsValid = true;
      return this.dateIsValid;
    } else {
      const startDateFormated = this.dateUtil.formatMomentDate(this.dateUtil.getDateString(this.dateUtil.formatDate(startDate)));
      const endDateFormated = this.dateUtil.formatMomentDate(this.dateUtil.getDateString(this.dateUtil.formatDate(endDate)));
      this.dateIsValid = this.dateUtil.dateIsValid(startDateFormated, endDateFormated);
      return this.dateIsValid;
    }
  }

  public handlePage(event: any) {
    this.dataSource.loadContent(
      event.pageSize,
      event.pageIndex,
      this.data.system,
      this.data.company,
      null,
      this.data.startDate,
      this.data.endDate,
      this.data.type,
      this.data.status
    );
  }

  checkboxLabel(row: any): boolean {
    if (row) {
      this.clearRows();
      this.populateRows(row);
    }
    return this.selection.isSelected(row);
  }

  clearRows() {
    this.selection.clear();
  }

  populateRows(row) {
    if (row !== this.selectedRow[0]) {
      this.selectedRow = [];
      this.selectedRow.push(row);
      this.selection.select(row);
    } else {
      this.selectedRow = [];
    }
  }

  sendGroupDetails() {

    this.eventsEvaluationService.sendEventEngineDetails(this.selectedRow[0]);
    this.router.navigate(['/gestao-erro/avalicao-de-eventos-engine/detalhes'], {
      queryParams: {
        page: this.selectedRow[0].page,
        size: this.selectedRow[0].size,
        baseDate: this.selectedRow[0].baseDate,
        company: this.selectedRow[0].company,
        postDate: this.selectedRow[0].postDate,
        system: this.selectedRow[0].system,
        type: this.selectedRow[0].eventType,
      }
    });
  }

}

