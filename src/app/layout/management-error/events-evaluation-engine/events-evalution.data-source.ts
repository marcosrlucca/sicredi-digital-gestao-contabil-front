import { EventsEvaluationService } from './events-evaluation-engine.service';
import { EventsEvaluation } from './events-evaluation.model';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Page} from '../../../shared/domain/page.model';

export class EventsEvaluationDataSource implements DataSource<EventsEvaluation> {

    itemsSubject = new BehaviorSubject<EventsEvaluation[]>([]);
    page = new Page();

    private loadingSubjects = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubjects.asObservable();
    public noDataFound = false;

    constructor(private eventsEvaluationService: EventsEvaluationService) {
        this.page.pageSizeOptions = [5, 20, 50, 100];
    }

    connect(collectionViewer: CollectionViewer): Observable<EventsEvaluation[]> {
        return this.itemsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.itemsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadContent(size, actualPage, system, company, baseDate, startDate, endDate, type, status) {
        this.loadingSubjects.next(true);

        this.eventsEvaluationService.find(size, actualPage, system, company, baseDate, startDate, endDate, type, status)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false)))
            .subscribe(response => {
                if (response.length !== 0) {
                    this.itemsSubject.next(response);
                    this.page = response;
                    this.page.pageSizeOptions = [5, 20, 50, 100];

                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }

}
