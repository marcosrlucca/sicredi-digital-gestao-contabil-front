import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {DetailsEvaluation} from './details-evaluation.model';
import {EventsEvaluationService} from '../events-evaluation-engine.service';
import {Page} from '../../../../shared/domain/page.model';

export class DetailsEvalutionEngineDataSource implements DataSource<DetailsEvaluation> {

    itemsSubject = new BehaviorSubject<DetailsEvaluation[]>([]);
    page = new Page();

    private loadingSubjects = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubjects.asObservable();
    public noDataFound = false;

    constructor(private eventsEvaluationService: EventsEvaluationService) {
        this.page.pageSizeOptions = [5, 20, 50, 100];
    }

    connect(collectionViewer: CollectionViewer): Observable<DetailsEvaluation[]> {
        return this.itemsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.itemsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadContent(size, actualPage, system, company, baseDate, postDate, type) {
        this.loadingSubjects.next(true);

        this.eventsEvaluationService.getDetails(actualPage, size, baseDate, company, postDate, system, type)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false)))
            .subscribe(response => {
                if (response.length !== 0) {
                    this.itemsSubject.next(response);
                    this.page = response;
                    this.page.pageSizeOptions = [5, 20, 50, 100];
                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }

}
