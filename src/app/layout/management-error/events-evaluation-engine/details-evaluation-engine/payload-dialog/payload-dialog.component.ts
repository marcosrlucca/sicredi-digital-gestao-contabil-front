import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { EventsEvaluationService } from '../../events-evaluation-engine.service';

const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
  selector: 'app-payload-dialog',
  templateUrl: './payload-dialog.component.html',
  styleUrls: ['./payload-dialog.component.scss']
})
export class PayloadDialogComponent implements OnInit {

  payload;
  data;
  dataDetails;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    public dialogPayload: MatDialogRef<any>,
    private eventsEvaluationService: EventsEvaluationService,
  ) { }

  ngOnInit() {
    this.setPayloadDialogData();
    this.getPayloadDetailsData();
  }

  setPayloadDialogData() {
    this.data = this.dialogData;
    this.payload = JSON.stringify(this.dialogData, null, 4);
  }

  getPayloadDetailsData() {
    this.eventsEvaluationService.getDetails(
      PAGE_INDEX_DEFAULT,
      PAGE_SIZE_DEFAULT,
      this.data.baseDate,
      this.data.company,
      this.data.postDate,
      this.data.system,
      this.data.eventType
    ).subscribe(response => {
      this.dataDetails = response;
    });
  }

  closeDialog() {
    this.dialogPayload.close();
  }

}
