import { PayloadDialogComponent } from './payload-dialog/payload-dialog.component';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../../router.animations';
import { MatDialog } from '@angular/material';
import { EventsEvaluationService } from '../events-evaluation-engine.service';
import { DetailsEvalutionEngineDataSource } from './details-evalution-engine.data-source';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
    selector: 'app-details-evaluation-engine',
    templateUrl: './details-evaluation-engine.component.html',
    styleUrls: ['./details-evaluation-engine.component.scss'],
    animations: [routerTransition()]
})
export class DetailsEvaluationEngineComponent implements OnInit {

    displayedColumns = ['details', 'company', 'system', 'baseDate', 'eventType', 'eventCode', 'eventId', 'reversal', 'document', 'postingDate', 'message', 'status'];
    dataSource: DetailsEvalutionEngineDataSource;
    currentGroup;
    count = 0;

    system;
    company;
    baseDate;
    postDate;
    type;
    hasData;

    constructor(
      private dialog: MatDialog,
      private eventsEvaluationService: EventsEvaluationService,
      private _route: ActivatedRoute,
      private location: Location
    ) { }

    ngOnInit() {
      this.dataSource = new DetailsEvalutionEngineDataSource(this.eventsEvaluationService);
      this.getQueryParams();
      this.getDetails();
    }

    getQueryParams() {
      this._route.queryParams.subscribe(params => {
        this.system = params['system'];
        this.company = params['company'];
        this.baseDate = params['baseDate'];
        this.postDate = params['postDate'];
        this.type = params['type'];
      });
    }

    getDetails() {
      this.dataSource.loadContent(
        PAGE_SIZE_DEFAULT,
        PAGE_INDEX_DEFAULT,
        this.system,
        this.company,
        this.baseDate,
        this.postDate,
        this.type
      );

      this.eventsEvaluationService.getDetailsCount(
        this.baseDate,
        this.company,
        this.postDate,
        this.system,
        this.type
        ).subscribe(response => {
        this.count = response;
      });
    }

    openPayloadDialog(row) {
      const dialogRef = this.dialog.open(PayloadDialogComponent, {
        width: '800px',
        height: '600px;',
        data: {
          company: row.company,
          system: row.system,
          baseDate: row.baseDate,
          eventType: row.eventType,
          eventCode: row.eventCode,
          uniqueIndex: row.uniqueIndex,
          reversal: row.reversal,
          document: row.document,
          postDate: row.postDate,
          operationDate: row.operationDate,
          message: row.message,
          status: row.status,
        }
      });
    }

    public handlePage(event: any) {
      this.dataSource.loadContent(
        event.pageSize,
        event.pageIndex,
        this.system,
        this.company,
        this.baseDate,
        this.postDate,
        this.type
      );
    }

    backToEventsEvaluation() {
      this.location.back();
    }
}
