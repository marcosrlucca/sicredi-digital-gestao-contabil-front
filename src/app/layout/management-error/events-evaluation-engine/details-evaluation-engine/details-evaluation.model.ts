export class DetailsEvaluation {
    baseDate: string;
    company: string;
    document: string;
    eventCode: string;
    eventType: string;
    message: string;
    operationDate: string;
    postDate: string;
    reversal: boolean;
    status: string;
    system: string;
    uniqueIndex: string;
}
