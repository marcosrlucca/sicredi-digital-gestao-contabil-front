import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {isNullOrUndefined} from 'util';
import {environment} from '../../../../environments/environment';

@Injectable()
export class EventsEvaluationService {

    private pathGroup = `${environment._api_backoffice_status}/status/group?`;
    private pathGroupCount = `${environment._api_backoffice_status}/status/group/count?`;
    private pathDetail = `${environment._api_backoffice_status}/status/detail?`;
    private pathDetailCount = `${environment._api_backoffice_status}/status/detail/count?`;

    public currentGroup;
    public systems;

    constructor(private httpClient: HttpClient) {}

    public find(size, page, system, company, baseDate, endDate, startDate, type, status): Observable<any> {
        let params = new HttpParams();
        params = !isNullOrUndefined(size) ? params.set('size', size.toString()) : params;
        params = !isNullOrUndefined(page) ? params.set('page', page.toString()) : params;
        params = !isNullOrUndefined(system) ? params.set('system', system) : params;
        params = !isNullOrUndefined(company) ? params.set('company', company) : params;
        params = !isNullOrUndefined(baseDate) ? params.set('baseDate', baseDate) : params;
        params = !isNullOrUndefined(endDate) ? params.set('endDate', endDate) : params;
        params = !isNullOrUndefined(startDate) ? params.set('startDate', startDate) : params;
        params = !isNullOrUndefined(type) ? params.set('eventType', type) : params;
        params = !isNullOrUndefined(status) ? params.set('status', status) : params;

        return this.httpClient.get<any>(this.pathGroup, {params}).catch(this.errorHandler);
    }

    public getGroupCount(baseDate, company, endDate, startDate, system, status, type): Observable<any> {
        let params = new HttpParams();
        params = !isNullOrUndefined(baseDate) ? params.set('baseDate', baseDate) : params;
        params = !isNullOrUndefined(company) ? params.set('company', company) : params;
        params = !isNullOrUndefined(endDate) ? params.set('endDate', endDate) : params;
        params = !isNullOrUndefined(startDate) ? params.set('startDate', startDate) : params;
        params = !isNullOrUndefined(system) ? params.set('system', system) : params;
        params = !isNullOrUndefined(status) ? params.set('status', status) : params;
        params = !isNullOrUndefined(type) ? params.set('type', type) : params;

        return this.httpClient.get<any>(this.pathGroupCount, {params}).catch(this.errorHandler);
    }

    public getDetails(page, size, baseDate, company, postDate , system, type): Observable<any> {
        let params = new HttpParams();
        params = !isNullOrUndefined(size) ? params.set('size', size.toString()) : params;
        params = !isNullOrUndefined(page) ? params.set('page', page.toString()) : params;
        params = !isNullOrUndefined(system) ? params.set('system', system) : params;
        params = !isNullOrUndefined(company) ? params.set('company', company) : params;
        params = !isNullOrUndefined(baseDate) ? params.set('baseDate', baseDate) : params;
        params = !isNullOrUndefined(postDate) ? params.set('postDate', postDate) : params;
        params = !isNullOrUndefined(type) ? params.set('type', type) : params;

        return this.httpClient.get<any>(this.pathDetail, {params}).catch(this.errorHandler);
    }

    public getDetailsCount(baseDate, company, postDate , system, type): Observable<any> {
        let params = new HttpParams();
        params = !isNullOrUndefined(system) ? params.set('system', system) : params;
        params = !isNullOrUndefined(company) ? params.set('company', company) : params;
        params = !isNullOrUndefined(baseDate) ? params.set('baseDate', baseDate) : params;
        params = !isNullOrUndefined(postDate) ? params.set('postDate', postDate) : params;
        params = !isNullOrUndefined(type) ? params.set('type', type) : params;

        return this.httpClient.get<any>(this.pathDetailCount, {params}).catch(this.errorHandler);
    }

    public getSystems() {
        if (isNullOrUndefined(this.systems)) {
            this.systems = [];

            this.httpClient.get(this.pathGroup).toPromise().then((a: Array<any>) => {
                if (!isNullOrUndefined(a)) {
                    a.forEach(b => this.systems.push({value: b['system'], viewValue: b['description']}));
                }
            }).catch(() => null);
        }

        return this.systems;
    }

    public sendEventEngineDetails(group: any) {
        this.currentGroup = group;
    }

    public getStatus() {
        return Observable.of(
            {value: 'success', viewValue: 'Sucesso'},
            {value: 'error', viewValue: 'Erro'}
        );
    }

    public getEvaluationEngineTypes() {
        return Observable.of(
            {value: 'movement', viewValue: 'Movimento'},
            {value: 'balance', viewValue: 'Saldo'}
        );
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }
}
