export class EventsEvaluation {

    baseDate: string;
    company: string;
    errors: number;
    eventType: string;
    postDate: string;
    processed: number;
    status: string;
    system: string;
    total: number;
    valids: number;
}
