import { Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {Authorization} from '../../shared/domain/authorization.model';
import {UserService} from '../../configuration/user.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-management-error-main',
    templateUrl: './management-error-main.html',
    styleUrls: ['./management-error-main.scss'],
    animations: [routerTransition()]
})
export class ManagementErrorMainComponent implements OnInit {

    constructor(private userService: UserService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                public router: Router) {
    }

    ngOnInit(): void {
        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });
    }

    goToEvaluationIntegration() {
        this.router.navigate(['/gestao-erro/avaliacao-integracao']);
    }

    goToAllotEvaluation() {
        this.router.navigate(['/gestao-erro/avaliacao-lote']);
    }

    goToSystemicError() {
        this.router.navigate(['/gestao-erro/erro-sistemico']);
    }

    goToTopazErrors() {
        this.router.navigate(['/gestao-erro/erro-falta-historico-topaz']);
    }
}
