import {MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-undo-purge-dialog',
    templateUrl: 'undo-purge-dialog.component.html',
    styleUrls: ['undo-purge-dialog.component.scss']
})
export class UndoPurgeDialogComponent {
    @Output() undoPurgeEventEmitter = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<UndoPurgeDialogComponent>) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    undoPurge() {
        this.dialogRef.close();
        this.undoPurgeEventEmitter.emit();
    }

}
