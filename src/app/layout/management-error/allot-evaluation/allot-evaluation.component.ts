import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../../router.animations';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDatepickerInputEvent, MatDialog, MatPaginator, MatSort} from '@angular/material';
import {tap} from 'rxjs/operators';
import {Allotment, AllotmentDetail, AllotmentDetailXLS, System} from './allot-evaluation.model';
import {ToastsManager} from 'ng2-toastr';
import {HttpStatus} from '../../../shared/domain/http-status';
import {PurgeDialogComponent} from './purge-dialog/purge-dialog.component';
import {UserSessionService} from '../../../shared/services/user-session.service';
import {UndoPurgeDialogComponent} from './undo-purge-dialog/undo-purge-dialog.component';
import {isNullOrUndefined} from 'util';
import {merge} from 'rxjs/observable/merge';

import {utils, write, WorkBook} from 'xlsx';
// noinspection TypeScriptCheckImport
import {saveAs} from 'file-saver';
import {StatusManagementErrorService} from '../../../shared/services/status-management-error.service';
import {AllotEvaluationDataSource} from './allot-evaluation.data-source';
import {AllotEvaluationService} from './allot-evaluation.service';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {Authorization} from '../../../shared/domain/authorization.model';
import {UserService} from '../../../configuration/user.service';
import {Subscription} from 'rxjs/Subscription';

const SORT_ORDER_DEFAULT = 'actionDate,desc';
const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
    selector: 'app-allot-evaluation',
    templateUrl: './allot-evaluation.component.html',
    styleUrls: ['./allot-evaluation.component.scss'],
    animations: [routerTransition()]
})
export class AllotEvaluationComponent implements OnInit, OnDestroy, AfterViewInit {
    displayedColumns = ['markedToAnalyze', 'id', 'chargeType', 'system', 'company', 'allotmentId', 'actionDate',
        'sapDocument', 'status', 'returnMessage', 'disconsidered'];
    dataSource: AllotEvaluationDataSource;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    form: FormGroup;
    selectedAllotments = new Array<Allotment>();
    selectedChargeType;
    selectedCompany = [];
    selectedStatus;
    chargeTypes = [];
    companies = [];
    status = [];
    startDate;
    endDate;
    sapDocument;
    originDocument;
    chargeType;
    startCalendar;
    endCalendar;
    checks = false;
    justification: string;
    resending = false;
    sapDocumentHidden = false;
    statusChosed = 'error';

    selectedSystem = [];
    systems: Array<System>;
    system;
    systemSubscription: Subscription;

    constructor(private formBuilder: FormBuilder,
                private managementErrorService: AllotEvaluationService,
                private toastr: ToastsManager,
                public dialog: MatDialog,
                private userSessionService: UserSessionService,
                private userService: UserService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                private statusService: StatusManagementErrorService) {
    }

    ngOnInit() {
        this.dataSource = new AllotEvaluationDataSource(this.managementErrorService);
        this.system = new FormControl('', []);
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.startDate = new FormControl(new Date());
        this.endDate = new FormControl(new Date());

        if (this.statusService.isAnyChoice()) {
            this.chargeType = new FormControl('', [Validators.required]);

            this.endDate.value = null;
            this.startDate.value = null;
            this.startCalendar = new FormControl('', []);
            this.endCalendar = new FormControl('', []);

            if (this.statusService.errorChosed) {
                this.statusChosed = 'error';
            } else if (this.statusService.notSentChosed) {
                this.statusChosed = 'not-sent';
            } else if (this.statusService.disconsideredChosed) {
                this.statusChosed = 'disconsidered';
            }

            this.statusService.resetChoices();

            this.selectedStatus = this.statusChosed;
            this.sort.active = 'actionDate';
            this.sort.direction = 'desc';

            this.dataSource.loadAllotments(
                '', '', '',
                '', '', '',
                '', this.statusChosed,
                SORT_ORDER_DEFAULT, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT
            );
        } else {
            this.chargeType = new FormControl('1', [Validators.required]);
            this.selectedChargeType = '1';
            this.statusChosed = '';
        }

        this.populateSelects();
        this.checks = false;

        this.form = this.formBuilder.group(
            {
                sapDocument: [''],
                originDocument: ['']
            }
        );

        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });
    }

    ngOnDestroy() {
        this.systemSubscription.unsubscribe();
    }

    ngAfterViewInit() {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                        this.loadAllotmentsPage();
                        this.checks = false;
                        this.selectedAllotments = [];
                    }
                )
            )
            .subscribe();
    }

    populateSelects() {
        this.managementErrorService.getChargeTypes().subscribe((item) => this.chargeTypes.push(item));
        this.systemSubscription = this.managementErrorService.getSystems().subscribe(systems => this.systems = systems);
        this.managementErrorService.getStatus().subscribe((item) => this.status.push(item));

        this.companies = this.managementErrorService.getCompanies();
    }

    loadAllotmentsPage() {
        this.dataSource = new AllotEvaluationDataSource(this.managementErrorService);

        if (!isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';
        // const today = this.getDateOfMonth(new Date());

        this.dataSource.loadAllotments(
            !isNullOrUndefined(this.selectedChargeType) ? this.selectedChargeType : '',
            this.selectedSystem !== undefined && this.selectedSystem !== null && this.selectedSystem.length > 0
                ? this.selectedSystem : '',
            this.selectedCompany !== undefined && this.selectedCompany !== null
                ? this.selectedCompany : '',
            startDateSimplified !== '' ? startDateSimplified : '',
            endDateSimplified !== '' ? endDateSimplified : '',
            this.sapDocument !== undefined && this.sapDocument !== null ? this.sapDocument : '',
            this.originDocument !== undefined && this.originDocument !== null ? this.originDocument : '',
            this.selectedStatus !== undefined && this.selectedStatus !== null ? this.selectedStatus : '',
            !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT,
            this.paginator.pageIndex, this.paginator.pageSize
        );
    }

    onChangeSapDocument(value: string) {
        if (!isNullOrUndefined(value) && value.trim() !== '') {
            this.selectedChargeType = '1';
            this.chargeType.reset('1');
        }
    }

    getDateOfMonth(dateToTransform: Date) {
        const dayStartDate = dateToTransform.getDate();
        const monthStartDate = dateToTransform.getMonth() + 1;
        const yearStartDate = dateToTransform.getFullYear();

        return `${dayStartDate}/${monthStartDate}/${yearStartDate}`;
    }

    onSubmit() {
        if (isNullOrUndefined(this.selectedChargeType) || this.selectedChargeType === '') {
            return;
        }

        this.dataSource = new AllotEvaluationDataSource(this.managementErrorService);

        if (!isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';

        this.paginator.pageSize = PAGE_SIZE_DEFAULT;
        this.sort.active = 'actionDate';
        this.sort.direction = 'desc';

        this.dataSource.loadAllotments(
            this.selectedChargeType,
            this.selectedSystem !== undefined && this.selectedSystem !== null && this.selectedSystem.length > 0
                ? this.selectedSystem : '',
            this.selectedCompany !== undefined && this.selectedCompany !== null
                ? this.selectedCompany : '',
            startDateSimplified !== '' ? startDateSimplified : '',
            endDateSimplified !== '' ? endDateSimplified : '',
            this.sapDocument !== undefined && this.sapDocument !== null ? this.sapDocument : '',
            this.originDocument !== undefined && this.originDocument !== null ? this.originDocument : '',
            this.selectedStatus !== undefined && this.selectedStatus !== null ? this.selectedStatus : '',
            SORT_ORDER_DEFAULT, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT
        );

        this.selectedAllotments = [];
    }

    startDateChange(chargeType: string, event: MatDatepickerInputEvent<Date>) {
        this.startDate.value = event.value;
    }

    endDateChange(chargeType: string, event: MatDatepickerInputEvent<Date>) {
        this.endDate.value = event.value;
    }

    checkall(event) {
        this.checks = !this.checks;

        this.dataSource.allotmentsSubject.getValue().forEach(allot => {
            if (event.currentTarget !== null) {
                allot.markedToAnalyze = event.currentTarget.checked;
                this.checkMarkedToAnalyze(allot, event);
            }
        });
    }

    checkMarkedToAnalyze(allotment: Allotment, event) {
        const index = this.selectedAllotments.findIndex(item => item.id === allotment.id);
        const checked = event.currentTarget.checked;

        if (index !== -1) {
            if (!checked) {
                this.selectedAllotments.splice(index, 1);
                allotment.markedToAnalyze = false;
            }
        } else {
            if (checked) {
                this.selectedAllotments.push(allotment);
                allotment.markedToAnalyze = true;
            }
        }
    }

    reset() {
        this.selectedChargeType = '';
        this.selectedSystem = [];
        this.selectedCompany = [];
        this.selectedStatus = '';
        this.selectedAllotments = [];
        this.sapDocument = '';
        this.originDocument = '';
        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.chargeType = new FormControl('', [Validators.required]);
        this.checks = false;
    }

    generateFileForReview() {
        if (this.selectedAllotments === null ||
            this.selectedAllotments === undefined ||
            this.selectedAllotments.length < 1) {
            this.toastr.warning('Não há registros selecionados', null, {toastLife: 3000});
            return;
        }

        this.managementErrorService.findAllotmentsByAllotmentIdAndChargeId(this.selectedAllotments).subscribe(
            (itens: AllotmentDetail[]) => {
                const dataXLS: AllotmentDetailXLS[] = [];

                if (itens === null || itens === undefined || itens.length < 1) {
                    this.toastr.warning('Não há dados para serem vistos', null, {toastLife: 3000});
                    return;
                }

                for (const item of itens) {
                    const xls = new AllotmentDetailXLS();
                    xls.id_carga = item.chargeId;
                    xls.id_lote = item.allotmentId;
                    xls.empresa = item.company;
                    xls.data_movimento = item.actionDateString;
                    xls.data_lancamento = item.entryDateString;
                    xls.tipo_documento = item.documentType;
                    xls.documento_topaz = item.topazDocument;
                    xls.conta = item.account;
                    xls.descricao = item.description;
                    xls.centro_custo = item.costCenter;
                    xls.centro_lucro = item.profitCenter;
                    xls.valor = item.value;
                    xls.documento_sap = item.docSap;
                    xls.mensagem_retorno = item.returnMessage;
                    xls.codigo_retorno = item.returnCode;
                    xls.motivo_desconsiderar = item.reasonDisregard;
                    xls.usuario_responsavel_desconsiderar = item.userDisregard;
                    xls.agencia_payload = item.agencyPaload;
                    xls.agencia_topaz = item.topazAgency;
                    xls.debito_credito = item.debitCredit;

                    dataXLS.push(xls);
                }

                const ws_name = 'Detalhes';
                const wb: WorkBook = {SheetNames: [], Sheets: {}};
                const ws: any = utils.json_to_sheet(dataXLS);
                wb.SheetNames.push(ws_name);
                wb.Sheets[ws_name] = ws;
                const wbout = write(wb, {bookType: 'xlsx', bookSST: true, type: 'binary'});

                function s2ab(s) {
                    const buf = new ArrayBuffer(s.length);
                    const view = new Uint8Array(buf);
                    for (let i = 0; i !== s.length; ++i) {
                        // tslint:disable-next-line: no-bitwise
                        view[i] = s.charCodeAt(i) & 0xFF;
                    }

                    return buf;
                }

                saveAs(new Blob([s2ab(wbout)], {type: 'application/octet-stream'}), 'Detalhes de Lote.xlsx');
            }, (res: Response) => {
                if (res.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                    this.toastr.error('Não há dados suficientes para realizar a busca', null, {toastLife: 3000});
                } else if (res.status === HttpStatus.NO_CONTENT) {
                    this.toastr.error('Registros não contém detalhes', null, {toastLife: 3000});
                }
            }
        );
    }

    disregardSelectedRecords() {
        if (!this.validateAllotments()) {
            return;
        }
        const index = this.selectedAllotments.findIndex(c => c.disconsidered);
        if (index !== -1) {
            this.toastr.warning('Não é possível desconsiderar registros já desconsiderados', null, {toastLife: 3500});
            return;
        }

        // noinspection TypeScriptValidateTypes
        const dialogRef = this.dialog.open(PurgeDialogComponent, {
            width: '80ex',
            data: {justification: this.justification}
        });

        dialogRef.beforeClose().subscribe(result => {
            this.justification = result;
        });

        dialogRef.componentInstance.doPurge.subscribe((event) => {
            this.purge(event);
        });
    }

    reconsiderSelectedRecords() {
        if (this.selectedAllotments.length <= 0) {
            this.toastr.warning('Não há registros selecionados', null, {toastLife: 3000});
            return;
        }

        const index = this.selectedAllotments.findIndex(c => !c.disconsidered);
        if (index !== -1) {
            this.toastr.warning('Não é possível reconsiderar registros que ainda não foram desconsiderados', null, {toastLife: 3500});
            return;
        }

        for (const s of this.selectedAllotments) {
            if (!isNullOrUndefined(s.actionDate)) {
                const periodOf30Days = new Date(Date.now() - (30 * 24 * 3600 * 1000));
                const day = s.actionDate.substring(0, 2);
                const month = s.actionDate.substring(3, 5);
                const year = s.actionDate.substring(6, 10);

                const dateAllot = new Date(`${year}/${month}/${day}`);

                if (dateAllot < periodOf30Days) {
                    this.toastr.warning('Não é permitido reconsiderar registros com mais de 30 dias', null, {toastLife: 3000});
                    return;
                }
            }
        }

        // noinspection TypeScriptValidateTypes
        const dialogRef = this.dialog.open(UndoPurgeDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.undoPurgeEventEmitter.subscribe(() => {
            this.undoPurge();
        });
    }

    undoPurge() {
        this.managementErrorService.undoPurge(this.selectedAllotments).subscribe(
            () => {
                this.selectedAllotments = [];
                this.checks = false;

                this.loadAllotmentsPage();

                setTimeout(() => {
                    this.toastr.info('Registros reconsiderados com sucesso', null, {toastLife: 3000});
                }, 500);

                this.statusService.updateStatus();
            }, (res: Response) => {
                if (res.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                    this.toastr.error('Não há dados suficientes para realizar a busca', null, {toastLife: 3000});
                }

                this.selectedAllotments = [];
                this.checks = false;
            });
    }

    validateAllotments(): boolean {
        let res = true;

        if (this.selectedAllotments.length <= 0) {
            this.toastr.warning('Não há registros selecionados', null, {toastLife: 3000});
            res = false;
        }

        for (const s of this.selectedAllotments) {
            if (!isNullOrUndefined(s.returnCode)) {
                const returnCodeStr: String = '' + s.returnCode;

                if (returnCodeStr.search('605') !== -1 || returnCodeStr.search('068') !== -1) {
                    this.toastr.warning('Registros executados com sucesso não podem ser desconsiderados', null, {toastLife: 3000});
                    res = false;
                }
            }
        }

        return res;
    }

    purge(justification) {
        this.managementErrorService.purge(this.selectedAllotments, justification, this.userSessionService.currentUser.username).subscribe(
            () => {
                this.selectedAllotments = [];
                this.checks = false;
                this.justification = '';

                this.loadAllotmentsPage();

                setTimeout(() => {
                    this.toastr.info('Registros desconsiderados com sucesso', null, {toastLife: 3000});
                }, 500);

                this.statusService.updateStatus();
            }, (res: Response) => {
                if (res.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                    this.toastr.error('Não há dados suficientes para realizar a busca', null, {toastLife: 3000});
                }

                this.selectedAllotments = [];
                this.checks = false;
            });
    }

    resendSelectedRecords() {
        if (this.selectedAllotments.length <= 0) {
            this.toastr.warning('Não há registros selecionados', null, {toastLife: 3000});
            return;
        }
        /*if (this.selectedAllotments.length > 1) {
            this.toastr.warning('Somente um registro pode ser reenviado por vez', null, {toastLife: 3000});
            return;
        }*/
        if (this.selectedAllotments.filter((a: Allotment) => isNullOrUndefined(a.allotmentId) || isNullOrUndefined(a.chargeId)).length > 0) {
            this.toastr.warning('Não há dados suficientes para realizar a busca', null, {toastLife: 3000});
            return;
        }
        if (this.selectedAllotments.filter((a: Allotment) => a.returnCode === '605' || a.returnCode === '068').length > 0) {
            this.toastr.warning('Registros executados com sucesso não podem ser reenviados', null, {toastLife: 3000});
            return;
        }
        if (this.selectedAllotments.filter((a: Allotment) => a.disconsidered).length > 0) {
            this.toastr.warning('Não é permitido reenviar registros desconsiderados', null, {toastLife: 3000});
            return;
        }

        this.resending = true;

        this.managementErrorService.resendAllotments(this.selectedAllotments).subscribe(
            () => {
                this.selectedAllotments = [];
                this.checks = false;

                this.loadAllotmentsPage();

                this.resending = false;

                setTimeout(() => {
                    this.toastr.info('Registros reenviados com sucesso', null, {toastLife: 3000});
                }, 500);

                this.statusService.updateStatus();
            }, (res: Response) => {
                if (res.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                    this.toastr.error('Registros executados com sucesso não podem ser reenviados', null, {toastLife: 3000});
                } else if (res.status === HttpStatus.PRECONDITION_FAILED) {
                    this.toastr.error('Não há dados suficientes para realizar a busca', null, {toastLife: 3000});
                }

                this.selectedAllotments = [];
                this.checks = false;
                this.resending = false;
                this.loadAllotmentsPage();
            }
        );
    }

    onSelectionTypeChange(event) {
        if (!isNullOrUndefined(event) && event.value === '2') {
            this.sapDocumentHidden = true;
            this.sapDocument = '';
        } else {
            this.sapDocumentHidden = false;
        }
    }
}
