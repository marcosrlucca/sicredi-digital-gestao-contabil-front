import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Company, Event, RequestToAllotmentDetail, RequestToPurging, System} from './allot-evaluation.model';
import {environment} from '../../../../environments/environment';
import {isNullOrUndefined} from 'util';
import {CompanyService} from '../../../shared/services/company.service';

@Injectable()
export class AllotEvaluationService {

    private _api_url = environment._api_accounting_management_url;

    private pathGetLots = `${this._api_url}/avaliacao-lotes`;
    private pathGetSystems = `${this._api_url}/sistema`;
    private pathPostDetailOfLots = `${this._api_url}/avaliacao-lotes/detalhar`;
    private pathPostResendAllotments = `${this._api_url}/avaliacao-lotes/reenviar`;
    private pathPostPurge = `${this._api_url}/avaliacao-lotes/desconsiderar`;
    private pathPostUndoPurge = `${this._api_url}/avaliacao-lotes/reconsiderar`;

    public systems: Observable<Array<System>>;

    page;

    constructor(private httpClient: HttpClient, private companyService: CompanyService) {
    }

    public findAllotments(chargeType, system, company, startDate, endDate, sapDocument, originDocument, status,
                          sortOrder, pageIndex, pageSize): Observable<any> {
        return this.httpClient.get(this.pathGetLots, {
            params: new HttpParams()
                .set('chargeType', chargeType)
                .set('system', system)
                .set('company', company)
                .set('startDate', startDate)
                .set('endDate', endDate)
                .set('sapDocument', sapDocument)
                .set('originDocument', originDocument)
                .set('status', status)
                .set('sort', sortOrder)
                .set('page', pageIndex.toString())
                .set('size', pageSize.toString())
        });
    }

    public undoPurge(selectedAllotments) {
        const ids: RequestToAllotmentDetail[] = [];
        const req = new RequestToPurging();

        selectedAllotments.forEach((item) => {
            const purgeDetails = new RequestToAllotmentDetail();
            purgeDetails.chargeId = item.chargeId;
            purgeDetails.allotmentId = item.allotmentId;
            purgeDetails.chargeType = item.chargeType;

            ids.push(purgeDetails);
        });

        req.purgeDetails = ids;

        return this.httpClient.post(this.pathPostUndoPurge, JSON.stringify(req));
    }

    public purge(selectedAllotments, justification, currentUser) {
        const ids: RequestToAllotmentDetail[] = [];
        const req = new RequestToPurging();

        selectedAllotments.forEach((item) => {
            const purgeDetails = new RequestToAllotmentDetail();
            purgeDetails.chargeId = item.chargeId;
            purgeDetails.allotmentId = item.allotmentId;
            purgeDetails.chargeType = item.chargeType;

            ids.push(purgeDetails);
        });

        req.purgeDetails = ids;
        req.justificationToPurging = justification;
        req.userName = currentUser;

        return this.httpClient.post(this.pathPostPurge, JSON.stringify(req));
    }

    public findAllotmentsByAllotmentIdAndChargeId(selectedAllotments): Observable<any> {
        const ids: RequestToAllotmentDetail[] = [];

        selectedAllotments.forEach((item) => {
            const req = new RequestToAllotmentDetail();
            req.allotmentId = item.allotmentId;
            req.chargeId = item.chargeId;
            req.chargeType = item.chargeType;

            ids.push(req);
        });

        return this.httpClient.post(this.pathPostDetailOfLots, JSON.stringify(ids));
    }

    public resendAllotments(selectedAllotments): Observable<any> {
        const ids: RequestToAllotmentDetail[] = [];

        selectedAllotments.forEach((item) => {
            const req = new RequestToAllotmentDetail();
            req.allotmentId = item.allotmentId;
            req.chargeId = item.chargeId;
            req.chargeType = item.chargeType;

            ids.push(req);
        });

        return this.httpClient.post(this.pathPostResendAllotments, JSON.stringify(ids));
    }

    public getChargeTypes() {
        return Observable.of(
            {value: '1', viewValue: 'Movimentos'},
            {value: '2', viewValue: 'Saldos'}
        );
    }

    public getSystems() {
        if (isNullOrUndefined(this.systems)) {
            this.systems = this.httpClient.get<Array<System>>(`${this.pathGetSystems}`).catch(this.errorHandler);
        }
        return this.systems;
    }

    public getStatus() {
        return Observable.of(
            {value: 'all', viewValue: 'Todos os registros'},
            {value: 'success', viewValue: 'Sucesso'},
            {value: 'error', viewValue: 'Erro'},
            {value: 'not-sent', viewValue: 'Não enviado'},
            {value: 'disconsidered', viewValue: 'Desconsiderado'},
            {value: 'processing', viewValue: 'Processando'}
        );
    }

    public getCompanies() {
        if (isNullOrUndefined(this.companyService.companies) || this.companyService.companies.length <= 0) {
            this.httpClient.get(this.companyService.pathGetCompany).subscribe((a: Array<Company>) => {
                if (!isNullOrUndefined(a)) {
                    a.forEach(b => {
                        this.companyService.companies.push({value: b['id'], viewValue: `${b['id']} - ${b['fantasyName']}`});
                    });
                }
            });
        }

        return this.companyService.companies;
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }
}
