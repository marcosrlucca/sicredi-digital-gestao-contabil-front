import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {Allotment, System} from './allot-evaluation.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {AllotEvaluationService} from './allot-evaluation.service';
import {Page} from '../../../shared/domain/page.model';

export class AllotEvaluationDataSource implements DataSource<Allotment> {

    /**
     * Its subscribers will always get its latest emitted value
     */
    allotmentsSubject = new BehaviorSubject<Allotment[]>([]);
    page = new Page();
    systems: Array<System>;

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubjects.asObservable();

    public noDataFound = false;

    constructor(private managementErrorService: AllotEvaluationService) {
        this.page.pageSizeOptions = [5, 10, 20, 50];
    }

    connect(collectionViewer: CollectionViewer): Observable<Allotment[]> {
        return this.allotmentsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.allotmentsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadAllotments(chargeType, system, company, startDate, endDate, sapDocument, originDocument, status,
                   sortOrder, pageIndex, pageSize) {
        this.loadingSubjects.next(true);

        this.managementErrorService.findAllotments(
            chargeType, system, company, startDate, endDate,
            sapDocument, originDocument, status,
            sortOrder, pageIndex, pageSize
        ).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubjects.next(false))
        )
            .subscribe(response => {
                if (response !== null) {
                    this.allotmentsSubject.next(response['content']);
                    this.page = response;

                    this.allotmentsSubject.getValue().forEach(a => {

                        this.managementErrorService.systems
                            .subscribe(systems => this.systems = systems).unsubscribe();

                        const index = this.systems.findIndex(b => b['value'] === a.system);

                        if (index !== -1) {
                            a.system = this.managementErrorService.systems[index]['viewValue'];
                        }
                    });

                    if (this.page.totalElements >= 20) {
                        this.page.pageSizeOptions = [5, 10, 20, 50];
                    } else if (this.page.totalElements >= 10) {
                        this.page.pageSizeOptions = [5, 10, 20];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    } else {
                        this.page.pageSizeOptions = [5, 10];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    }

                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }
}
