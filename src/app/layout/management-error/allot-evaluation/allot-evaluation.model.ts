export class Allotment {
    id: number;
    company: number;
    allotmentId: number;
    chargeId: number;
    actionDate: string;
    sapDocument: string;
    returnCode: string;
    status: string;
    system: string;
    returnMessage: string;
    reasonDisregard: string;
    disconsidered: boolean;
    markedToAnalyze: boolean;
}

export class AllotmentDetail {
    actionDate: string;
    entryDate: string;
    actionDateString: string;
    entryDateString: string;
    company: string;
    documentType: string;
    topazDocument: string;
    account: string;
    description: string;
    costCenter: string;
    profitCenter: string;
    docSap: string;
    returnCode: string;
    returnMessage: string;
    reasonDisregard: string;
    userDisregard: string;
    value: number;
    chargeId: number;
    allotmentId: number;
    agencyPaload: string;
    topazAgency: string;
    debitCredit: string;
}

export class AllotmentDetailXLS {
    id_carga: number;
    id_lote: number;
    empresa: string;
    tipo_documento: string;
    data_movimento: string;
    data_lancamento: string;
    documento_topaz: string;
    conta: string;
    descricao: string;
    centro_custo: string;
    centro_lucro: string;
    documento_sap: string;
    codigo_retorno: string;
    mensagem_retorno: string;
    motivo_desconsiderar: string;
    usuario_responsavel_desconsiderar: string;
    valor: number;
    agencia_payload: string;
    agencia_topaz: string;
    debito_credito: string;
}

export class Enum {
    value: string;
    viewValue: string;
}

export class Company extends Enum {
    id: number;
    fantasyName: string;
}

export class Event {
    id: number;
    code: string;
    system: System;
}

export class EventType {
    id: number;
    code: string;
}

export class RequestToAllotmentDetail {
    allotmentId: number;
    chargeId: number;
    chargeType: string;
}

export class RequestToPurging {
    purgeDetails: Array<RequestToAllotmentDetail>;
    justificationToPurging;
    userName;
}

export class Status extends Enum {
}

export class System extends Enum {
    id: number;
    system: string;
    description: string;
    code: string;
}

export class ChargeType extends Enum {
}

export class Constants {
    static readonly DATE_FMT = 'dd/MMM/yyyy';
    static readonly DATE_TIME_FMT = `${Constants.DATE_FMT} hh:mm:ss`;
}
