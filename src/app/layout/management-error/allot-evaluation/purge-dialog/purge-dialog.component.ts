import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Inject, Output} from '@angular/core';
import {UndoPurgeDialogComponent} from '../undo-purge-dialog/undo-purge-dialog.component';

@Component({
    selector: 'app-purge-dialog',
    templateUrl: 'purge-dialog.component.html',
    styleUrls: ['purge-dialog.component.scss']
})
export class PurgeDialogComponent {
    @Output() doPurge = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<UndoPurgeDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onPurge() {
        this.doPurge.emit(this.data.justification);
    }

}
