import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {LogErrorKettle, LogErrorKettleDetail} from '../analysis-kettle-log.model';
import {LogDetailService} from './log-detail.service';
import {isNullOrUndefined} from 'util';

export class LogDetailDataSource implements DataSource<LogErrorKettleDetail> {

    /**
     * Its subscribers will always get its latest emitted value
     */
    allotmentsSubject = new BehaviorSubject<LogErrorKettleDetail[]>([]);

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubjects.asObservable();

    logErrorCurrent: LogErrorKettle;

    public noDataFound = false;

    constructor(private service: LogDetailService) {
    }

    connect(collectionViewer: CollectionViewer): Observable<LogErrorKettleDetail[]> {
        return this.allotmentsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.allotmentsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadLogErrorsDetail(id) {
        this.loadingSubjects.next(true);

        this.service.getLogDetail(id)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false))
            )
            .subscribe(response => {
                if (response !== null) {
                    this.allotmentsSubject.next(response['description']);
                    this.logErrorCurrent = response['denomination'];

                    if (isNullOrUndefined(this.logErrorCurrent.date)) {
                        this.logErrorCurrent.date = '';
                    }
                } else {
                    this.noDataFound = true;
                }
            });
    }
}
