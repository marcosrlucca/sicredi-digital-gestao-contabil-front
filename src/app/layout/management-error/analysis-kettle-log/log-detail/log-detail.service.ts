import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class LogDetailService {

    private _api_url = environment._api_accounting_management_url;

    private pathGetLogDetail = `${this._api_url}/analise-log-erros`;

    constructor(private http: HttpClient) {
    }

    getLogDetail(id): Observable<any> {
        return this.http.get(`${this.pathGetLogDetail}/${id}`);
    }
}
