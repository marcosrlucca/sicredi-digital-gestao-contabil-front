import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {LogDetailService} from './log-detail.service';
import {LogDetailDataSource} from './log-detail.data-source';
import {routerTransition} from '../../../../router.animations';

@Component({
    selector: 'app-log-detail',
    templateUrl: './log-detail.component.html',
    styleUrls: ['./log-detail.component.scss'],
    animations: [routerTransition()]
})
export class LogDetailComponent implements OnInit {

    displayedColumns = ['description'];

    dataSource: LogDetailDataSource;

    constructor(private route: ActivatedRoute,
                private service: LogDetailService) {
    }

    ngOnInit() {
        this.dataSource = new LogDetailDataSource(this.service);

        this.route.params.subscribe(params => {
            const id = +params['id'];
            this.dataSource.loadLogErrorsDetail(id);
        });
    }
}
