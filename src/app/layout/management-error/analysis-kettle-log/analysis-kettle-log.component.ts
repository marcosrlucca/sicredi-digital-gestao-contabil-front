import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../../router.animations';
import {MatDialog, MatPaginator, MatSort} from '@angular/material';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import {ToastsManager} from 'ng2-toastr';
import {isNullOrUndefined} from 'util';
import {merge} from 'rxjs/observable/merge';
import {AnalysisKettleLogDataSource} from './analysis-kettle-log.data-source';
import {AnalysisKettleLogService} from './analysis-kettle-log.service';
import {fromEvent} from 'rxjs/observable/fromEvent';
import {LogErrorKettle} from './analysis-kettle-log.model';
import {HttpStatus} from '../../../shared/domain/http-status';
import {RemovalConfirmationDialogComponent} from './removal-confirmation-dialog/removal-confirmation-dialog.component';
import {Authorization} from '../../../shared/domain/authorization.model';
import {UserService} from '../../../configuration/user.service';

const SORT_ORDER_DEFAULT = 'date,desc';
const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
    selector: 'app-analysis-kettle-log',
    templateUrl: './analysis-kettle-log.component.html',
    styleUrls: ['./analysis-kettle-log.component.scss'],
    animations: [routerTransition()]
})
export class AnalysisKettleLogComponent implements OnInit, AfterViewInit {
    displayedColumns = ['markedToDelete', 'date', 'file', 'error', 'detail'];

    dataSource: AnalysisKettleLogDataSource;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('input') input: ElementRef;

    selectedLogError = new Array<LogErrorKettle>();
    checks = false;

    public userAuthorized = false;

    constructor(private service: AnalysisKettleLogService,
                private toastr: ToastsManager,
                public dialog: MatDialog,
                private userService: UserService) {

        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            authorities.permission.forEach(a => {
                if (a === 'CN=DIG_CONTABIL_ADM,CN=SOFTWARE,CN=GROUPS,DC=SICREDI,DC=COM,DC=BR' ||
                    a === 'CN=DIG_CONTABIL_CONSULTA,CN=SOFTWARE,CN=GROUPS,DC=SICREDI,DC=COM,DC=BR') {
                    this.userAuthorized = true;
                }
            });
        });
    }

    ngOnInit() {
        this.checks = false;
        this.dataSource = new AnalysisKettleLogDataSource(this.service);
        this.dataSource.loadLogErrors(SORT_ORDER_DEFAULT, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT, '');

        this.sort.active = 'date';
        this.sort.direction = 'desc';
    }

    ngAfterViewInit() {
        fromEvent(this.input.nativeElement, 'keyup')
            .pipe(
                debounceTime(700),
                distinctUntilChanged(),
                tap(() => {
                    // const value: string = this.input.nativeElement.value;
                    // if (value.trim() !== '') {
                    this.paginator.pageIndex = 0;
                    this.paginator.pageSize = PAGE_SIZE_DEFAULT;
                    this.loadLogErrorsPage();
                    // }
                })
            )
            .subscribe();

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                tap(() => {
                        this.loadLogErrorsPage();
                        this.checks = false;
                        this.selectedLogError = [];
                    }
                )
            )
            .subscribe();
    }

    loadLogErrorsPage() {
        this.dataSource = new AnalysisKettleLogDataSource(this.service);

        this.dataSource.loadLogErrors(
            !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT,
            this.paginator.pageIndex, this.paginator.pageSize, this.input.nativeElement.value
        );
    }

    remove() {
        if (this.selectedLogError === null ||
            this.selectedLogError === undefined ||
            this.selectedLogError.length < 1) {
            this.toastr.info('Não há itens selecionados!', null, {toastLife: 3000});
            return;
        }

        // noinspection TypeScriptValidateJSTypes
        // noinspection TypeScriptValidateTypes
        const dialogRef = this.dialog.open(RemovalConfirmationDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.deleteEventEmitter.subscribe(() => {
            this.service.delete(this.selectedLogError).subscribe(
                () => {
                    this.toastr.info('Remoção realizada com sucesso!', null, {toastLife: 3000});
                    this.loadLogErrorsPage();
                }, (res: Response) => {
                    if (res.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                        this.toastr.error('Dados insuficientes!', null, {toastLife: 3000});
                    }
                }
            );
        });
    }

    checkall(event) {
        this.checks = !this.checks;
        this.dataSource.allotmentsSubject.getValue().forEach(e => {
            if (event.currentTarget !== null) {
                e.markedToDelete = event.currentTarget.checked;
                this.checkMarkedToDelete(e, event);
            }
        });
    }

    checkMarkedToDelete(logError: LogErrorKettle, event) {
        const index = this.selectedLogError.findIndex(item => item.id === logError.id);
        const checked = event.currentTarget.checked;

        if (index !== -1) {
            if (!checked) {
                this.selectedLogError.splice(index, 1);
                logError.markedToDelete = false;
            }
        } else {
            if (checked) {
                this.selectedLogError.push(logError);
                logError.markedToDelete = true;
            }
        }
    }
}
