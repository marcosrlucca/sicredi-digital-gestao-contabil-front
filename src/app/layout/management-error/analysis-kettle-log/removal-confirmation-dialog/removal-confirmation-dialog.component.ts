import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Inject, Output} from '@angular/core';

@Component({
    selector: 'app-removal-confirmation-dialog',
    templateUrl: 'removal-confirmation-dialog.component.html',
    styleUrls: ['removal-confirmation-dialog.component.scss']
})
export class RemovalConfirmationDialogComponent {
    @Output() deleteEventEmitter = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<RemovalConfirmationDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    delete() {
        this.dialogRef.close();
        this.deleteEventEmitter.emit();
    }

}
