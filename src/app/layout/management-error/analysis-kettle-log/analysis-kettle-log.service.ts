import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {UserSessionService} from '../../../shared/services/user-session.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class AnalysisKettleLogService {

    private _api_url = environment._api_accounting_management_url;

    private pathGetAll = `${this._api_url}/analise-log-erros`;
    private pathDelete = `${this._api_url}/analise-log-erros`;

    page;

    constructor(private httpClient: HttpClient,
                private userService: UserSessionService) {
    }

    public findError(filter, sortOrder, pageIndex, pageSize): Observable<any> {
        return this.httpClient.get(this.pathGetAll, {
            params: new HttpParams()
                .set('filter', filter)
                .set('sort', sortOrder)
                .set('page', pageIndex.toString())
                .set('size', pageSize.toString())
        });
    }

    public delete(selectedErrors) {
        const ids = [];
        selectedErrors.forEach((item) => {
            ids.push('' + item.id);
        });

        return this.httpClient.delete(this.pathDelete, {
            params: new HttpParams()
                .set('ids', JSON.stringify(ids))
                .set('username', this.userService.currentUser.username)
        });
    }
}
