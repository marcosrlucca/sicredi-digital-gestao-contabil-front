import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {AnalysisKettleLogService} from './analysis-kettle-log.service';
import {LogErrorKettle} from './analysis-kettle-log.model';
import {Page} from '../../../shared/domain/page.model';

export class AnalysisKettleLogDataSource implements DataSource<LogErrorKettle> {

    /**
     * Its subscribers will always get its latest emitted value
     */
    allotmentsSubject = new BehaviorSubject<LogErrorKettle[]>([]);
    page = new Page();

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubjects.asObservable();

    public noDataFound = false;

    constructor(private analysisKettleLogService: AnalysisKettleLogService) {
        this.page.pageSizeOptions = [5, 10, 20, 50];
    }

    connect(collectionViewer: CollectionViewer): Observable<LogErrorKettle[]> {
        return this.allotmentsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.allotmentsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadLogErrors(sortOrder, pageIndex, pageSize, filter) {
        this.loadingSubjects.next(true);

        this.analysisKettleLogService.findError(filter, sortOrder, pageIndex, pageSize)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false))
            )
            .subscribe(response => {
                if (response !== null) {
                    this.allotmentsSubject.next(response['content']);
                    this.page = response;

                    if (this.page.totalElements >= 20) {
                        this.page.pageSizeOptions = [5, 10, 20, 50];
                    } else if (this.page.totalElements >= 10) {
                        this.page.pageSizeOptions = [5, 10, 20];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    } else {
                        this.page.pageSizeOptions = [5, 10];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    }

                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }
}
