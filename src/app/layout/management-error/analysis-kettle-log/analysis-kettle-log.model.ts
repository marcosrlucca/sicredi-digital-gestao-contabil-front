export class LogErrorKettle {
    id: number;
    date: string;
    file: string;
    error: string;
    markedToDelete: boolean;
}

export class LogErrorKettleDetail {
    logErrorId: number;
    lineNumber: number;
    description: number;
}

export class Constants {
    static readonly DATE_FMT = 'dd/MMM/yyyy';
    static readonly DATE_TIME_FMT = `${Constants.DATE_FMT} hh:mm:ss`;
}
