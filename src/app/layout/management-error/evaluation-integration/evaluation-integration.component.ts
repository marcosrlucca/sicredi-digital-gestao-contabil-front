import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MatDatepickerInputEvent, MatDialog, MatPaginator, MatSort} from '@angular/material';
import {tap} from 'rxjs/operators';
import {EvaluationIntegrationDataSource} from './evaluation-integration.data-source';
import {ToastsManager} from 'ng2-toastr';
import {isNullOrUndefined} from 'util';
import {merge} from 'rxjs/observable/merge';

import {EvaluationIntegrationService} from './evaluation-integration.service';
import {StatusEvaluationIntegrationService} from '../../../shared/services/status-evaluation-integration.service';
import {routerTransition} from '../../../router.animations';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {UserService} from '../../../configuration/user.service';
import {Authorization} from '../../../shared/domain/authorization.model';

const SORT_ORDER_DEFAULT = 'baseDate,desc';
const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
    selector: 'app-evaluation-integration',
    templateUrl: './evaluation-integration.component.html',
    styleUrls: ['./evaluation-integration.component.scss'],
    animations: [routerTransition()]
})
export class EvaluationIntegrationComponent implements OnInit, AfterViewInit {
    displayedColumns = ['company', 'system', 'baseDate', 'pendingEvents', 'processedEvents', 'quantityIntegrate', 'quantityNotIntegrate',
        'syntheticMovement', 'quantityDocument', 'syntheticMovementDocument', 'analyticalMovementDocument', 'message'];
    dataSource: EvaluationIntegrationDataSource;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    form: FormGroup;
    selectedSystem = [];
    selectedCompany = [];
    selectedStatus;
    systems = [];
    companies = [];
    status = [];
    startDate;
    endDate;
    system;
    startCalendar;
    endCalendar;

    constructor(private formBuilder: FormBuilder,
                private evaluationIntegrationService: EvaluationIntegrationService,
                private statusEvaluationIntegrationService: StatusEvaluationIntegrationService,
                private toastr: ToastsManager,
                private userService: UserService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                public dialog: MatDialog) {
    }

    ngOnInit() {
        this.dataSource = new EvaluationIntegrationDataSource(this.evaluationIntegrationService);
        this.system = new FormControl('', []);
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.startDate = new FormControl(new Date());
        this.endDate = new FormControl(new Date());

        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);

        if (this.statusEvaluationIntegrationService.isErrorChosed() && this.statusEvaluationIntegrationService.totalError > 0) {
            this.dataSource.loadContent('', '', 'ERROR', '', '',
                SORT_ORDER_DEFAULT, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT);

            this.statusEvaluationIntegrationService.resetChoice();
        }

        this.sort.active = 'baseDate';
        this.sort.direction = 'desc';

        this.populateSelects();

        this.form = this.formBuilder.group(
            {
                sapDocument: [''],
                originDocument: ['']
            }
        );

        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });
    }

    ngAfterViewInit() {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page).pipe(tap(() => this.loadContentPage())).subscribe();
    }

    populateSelects() {
        this.systems = this.evaluationIntegrationService.getSystems();
        this.companies = this.evaluationIntegrationService.getCompanies();
        this.evaluationIntegrationService.getStatus().subscribe((item) => this.status.push(item));
    }

    loadContentPage() {
        this.dataSource = new EvaluationIntegrationDataSource(this.evaluationIntegrationService);

        if (!isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';
        // const today = this.getDateOfMonth(new Date());

        this.dataSource.loadContent(
            this.selectedSystem !== undefined && this.selectedSystem !== null && this.selectedSystem.length > 0 ? this.selectedSystem : '',
            this.selectedCompany !== undefined && this.selectedCompany !== null ? this.selectedCompany : '',
            this.selectedStatus !== undefined && this.selectedStatus !== null ? this.selectedStatus : '',
            startDateSimplified !== '' ? startDateSimplified : '',
            endDateSimplified !== '' ? endDateSimplified : '',
            !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT,
            this.paginator.pageIndex, this.paginator.pageSize
        );
    }

    getDateOfMonth(dateToTransform: Date) {
        const dayStartDate = dateToTransform.getDate();
        const monthStartDate = dateToTransform.getMonth() + 1;
        const yearStartDate = dateToTransform.getFullYear();

        return `${dayStartDate}/${monthStartDate}/${yearStartDate}`;
    }

    onSubmit() {
        this.dataSource = new EvaluationIntegrationDataSource(this.evaluationIntegrationService);

        if (!isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';

        this.paginator.pageSize = PAGE_SIZE_DEFAULT;
        this.sort.active = 'baseDate';
        this.sort.direction = 'desc';

        this.dataSource.loadContent(
            this.selectedSystem !== undefined && this.selectedSystem !== null && this.selectedSystem.length > 0 ? this.selectedSystem : '',
            this.selectedCompany !== undefined && this.selectedCompany !== null ? this.selectedCompany : '',
            this.selectedStatus !== undefined && this.selectedStatus !== null ? this.selectedStatus : '',
            startDateSimplified !== '' ? startDateSimplified : '',
            endDateSimplified !== '' ? endDateSimplified : '',
            SORT_ORDER_DEFAULT, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT
        );
    }

    startDateChange(chargeType: string, event: MatDatepickerInputEvent<Date>) {
        this.startDate.value = event.value;
    }

    endDateChange(chargeType: string, event: MatDatepickerInputEvent<Date>) {
        this.endDate.value = event.value;
    }

    reset() {
        this.selectedSystem = [];
        this.selectedCompany = [];
        this.selectedStatus = '';
        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
    }
}
