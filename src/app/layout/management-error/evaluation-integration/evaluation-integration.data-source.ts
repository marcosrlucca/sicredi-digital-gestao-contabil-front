import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {EvaluationIntegrationService} from './evaluation-integration.service';
import {EvaluationIntegration} from './evaluation-integration.model';
import {Page} from '../../../shared/domain/page.model';

export class EvaluationIntegrationDataSource implements DataSource<EvaluationIntegration> {

    /**
     * Its subscribers will always get its latest emitted value
     */
    itemsSubject = new BehaviorSubject<EvaluationIntegration[]>([]);
    page = new Page();

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubjects.asObservable();

    public noDataFound = false;

    constructor(private evaluationIntegrationService: EvaluationIntegrationService) {
        this.page.pageSizeOptions = [5, 10, 20, 50];
    }

    connect(collectionViewer: CollectionViewer): Observable<EvaluationIntegration[]> {
        return this.itemsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.itemsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadContent(system, company, message, startDate, endDate, sortOrder, pageIndex, pageSize) {
        this.loadingSubjects.next(true);

        this.evaluationIntegrationService.find(system, company, message, startDate, endDate, sortOrder, pageIndex, pageSize)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false)))
            .subscribe(response => {
                if (response !== null) {
                    this.itemsSubject.next(response['content']);
                    this.page = response;

                    this.itemsSubject.getValue().forEach(a => {
                        const index = this.evaluationIntegrationService.systems.findIndex(b => b['value'] === a.system);

                        if (index !== -1) {
                            a.system = this.evaluationIntegrationService.systems[index]['viewValue'];
                        }
                    });

                    if (this.page.totalElements >= 20) {
                        this.page.pageSizeOptions = [5, 10, 20, 50];
                    } else if (this.page.totalElements >= 10) {
                        this.page.pageSizeOptions = [5, 10, 20];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    } else {
                        this.page.pageSizeOptions = [5, 10];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    }

                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }
}
