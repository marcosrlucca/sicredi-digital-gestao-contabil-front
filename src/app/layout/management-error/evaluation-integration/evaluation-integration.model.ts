export class EvaluationIntegration {
    id: number;
    company: number;
    system: string;
    baseDate: number;
    baseDateToView: number;
    quantityIntegrate: string;
    quantityNotIntegrate: string;
    syntheticMovement: string;
    quantityDocument: string;
    syntheticMovementDocument: string;
    analyticalMovementDocument: string;
    pendingEvents: string;
    processedEvents: string;
    message: string;
}

export class Company {
    id: number;
    fantasyName: string;
}

export class Enum {
    value: string;
    viewValue: string;
}

export class System extends Enum {
}
