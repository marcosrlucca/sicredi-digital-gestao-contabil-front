import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Company, System} from './evaluation-integration.model';
import {isNullOrUndefined} from 'util';
import {environment} from '../../../../environments/environment';
import {CompanyService} from '../../../shared/services/company.service';

@Injectable()
export class EvaluationIntegrationService {

    private pathGetEvaluations = `${environment._api_accounting_evaluation_integration_process}/evaluation-integration`;
    private pathGetSystems = `${environment._api_accounting_management_url}/sistema`;

    public systems;

    constructor(private httpClient: HttpClient, private companyService: CompanyService) {
    }

    public find(system, company, message, startDate, endDate, sortOrder, pageIndex, pageSize): Observable<any> {
        return this.httpClient.get(this.pathGetEvaluations, {
            params: new HttpParams()
                .set('system', system)
                .set('company', company)
                .set('status', message)
                .set('startDate', startDate)
                .set('endDate', endDate)
                .set('sort', sortOrder)
                .set('page', pageIndex.toString())
                .set('size', pageSize.toString())
        });
    }

    public getStatus() {
        return Observable.of(
            {value: 'SUCCESS', viewValue: 'Sucesso'},
            {value: 'ERROR', viewValue: 'Erro'}
        );
    }

    public getSystems() {
        if (isNullOrUndefined(this.systems)) {
            this.systems = [];

            this.httpClient.get(this.pathGetSystems).toPromise().then((a: Array<System>) => {
                if (!isNullOrUndefined(a)) {
                    a.forEach(b => this.systems.push({value: b['system'], viewValue: b['description']}));
                }
            }).catch(() => null);
        }

        return this.systems;
    }

    public getCompanies() {
        if (isNullOrUndefined(this.companyService.companies) || this.companyService.companies.length <= 0) {
            this.httpClient.get(this.companyService.pathGetCompany).subscribe((a: Array<Company>) => {
                if (!isNullOrUndefined(a)) {
                    a.forEach(b => {
                        this.companyService.companies.push({value: b['id'], viewValue: `${b['id']} - ${b['fantasyName']}`});
                    });
                }
            });
        }

        return this.companyService.companies;
    }
}
