import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MatDatepickerInputEvent, MatDialog, MatPaginator, MatSort} from '@angular/material';
import {tap} from 'rxjs/operators';
import {ToastsManager} from 'ng2-toastr';
import {isNullOrUndefined} from 'util';
import {merge} from 'rxjs/observable/merge';

import {ActNotActDataSource} from './act-not-act.data-source';
import {ActNotActService} from './act-not-act.service';
import {ActNotAct, ActNotActXLS} from './model/act-not-act.model';
import {utils, WorkBook, write} from 'xlsx';
// noinspection TypeScriptCheckImport
import {saveAs} from 'file-saver';
import {ExcelDialogComponent} from './excel-dialog/excel-dialog.component';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';

const SORT_ORDER_DEFAULT = 'date,desc';
const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
    selector: 'app-act-not-act',
    templateUrl: './act-not-act.component.html',
    styleUrls: ['./act-not-act.component.scss'],
    animations: [routerTransition()]
})
export class ActNotActComponent implements OnInit, AfterViewInit {

    displayedColumns = ['company', 'agency', 'system', 'date', 'type', 'account', 'historic', 'center',
        'value', 'debitCredit', 'document', 'docSap', 'associated', 'actNotActType'];

    dataSource: ActNotActDataSource;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    form: FormGroup;
    selectedCompany;
    selectedSystem;
    selectedDebitCredit;
    selectedActNotAct;
    selectedAssociated;
    companies = [];
    systems = [];
    debitCreditTypes = [];
    actNotActTypes = [];
    associatedTypes = [];
    startDate;
    endDate;
    startCalendar;
    endCalendar;
    entity: ActNotAct;

    constructor(private formBuilder: FormBuilder,
                private actNotActService: ActNotActService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                private toastr: ToastsManager,
                public dialog: MatDialog) {
    }

    ngOnInit() {
        this.dataSource = new ActNotActDataSource(this.actNotActService);
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.startDate = new FormControl(new Date());
        this.endDate = new FormControl(new Date());

        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);

        this.sort.active = 'date';
        this.sort.direction = 'desc';

        this.entity = new ActNotAct();

        this.populateSelects();

        this.form = this.formBuilder.group({
            agency: new FormControl('', []),
            idOrigin: new FormControl('', []),
            account: new FormControl('', []),
            docSap: new FormControl('', []),
            center: new FormControl('', []),
            document: new FormControl('', []),
            type: new FormControl('', []),
            debitCredit: new FormControl('', []),
            actOrNotAct: new FormControl('', [])
        });
    }

    ngAfterViewInit() {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page).pipe(tap(() => this.loadContent())).subscribe();
    }

    populateSelects() {
        this.companies = this.actNotActService.getCompanies();
        this.systems = this.actNotActService.getSystems();

        this.actNotActService.getDebitCreditTypes().subscribe((item) => this.debitCreditTypes.push(item));
        this.actNotActService.getActNotActTypes().subscribe((item) => this.actNotActTypes.push(item));
        this.actNotActService.getAssociatedTypes().subscribe((item) => this.associatedTypes.push(item));
    }

    loadContent() {
        this.dataSource = new ActNotActDataSource(this.actNotActService);

        if (!isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';

        this.dataSource.loadContent(
            !isNullOrUndefined(startDateSimplified) && startDateSimplified !== '' && startDateSimplified ? startDateSimplified : '',
            !isNullOrUndefined(endDateSimplified) && endDateSimplified !== '' ? endDateSimplified : '',
            !isNullOrUndefined(this.selectedSystem) ? this.selectedSystem : '',
            !isNullOrUndefined(this.selectedCompany) ? this.selectedCompany : '',
            !isNullOrUndefined(this.entity.agency) ? this.entity.agency : '',
            !isNullOrUndefined(this.entity.docSap) ? this.entity.docSap : '',
            !isNullOrUndefined(this.entity.idOrigin) ? this.entity.idOrigin : '',
            !isNullOrUndefined(this.entity.account) ? this.entity.account : '',
            !isNullOrUndefined(this.entity.center) ? this.entity.center : '',
            !isNullOrUndefined(this.entity.document) ? this.entity.document : '',
            !isNullOrUndefined(this.entity.type) ? this.entity.type : '',
            !isNullOrUndefined(this.selectedDebitCredit) ? this.selectedDebitCredit : '',
            !isNullOrUndefined(this.selectedActNotAct) ? this.selectedActNotAct : '',
            !isNullOrUndefined(this.selectedAssociated) ? this.selectedAssociated : '',
            !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT,
            this.paginator.pageIndex,
            this.paginator.pageSize
        );
    }

    getDateOfMonth(dateToTransform: Date) {
        const dayStartDate = dateToTransform.getDate();
        const monthStartDate = dateToTransform.getMonth() + 1;
        const yearStartDate = dateToTransform.getFullYear();

        return `${dayStartDate}/${monthStartDate}/${yearStartDate}`;
    }

    onSubmit() {
        this.dataSource = new ActNotActDataSource(this.actNotActService);
        let validated = true;

        if (isNullOrUndefined(this.startDate.value)) {
            this.toastr.warning('O campo Data início é obrigatório', null, {toastLife: 3000});
            validated = false;
        }

        if (isNullOrUndefined(this.selectedActNotAct)) {
            this.toastr.warning('O campo Ato/Ato Não é obrigatório', null, {toastLife: 3000});
            validated = false;
        }

        if (!isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            validated = false;
        }

        if (!validated) {
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';

        this.paginator.pageSize = PAGE_SIZE_DEFAULT;
        this.sort.active = 'date';
        this.sort.direction = 'desc';

        this.dataSource.loadContent(
            !isNullOrUndefined(startDateSimplified) && startDateSimplified !== '' && startDateSimplified ? startDateSimplified : '',
            !isNullOrUndefined(endDateSimplified) && endDateSimplified !== '' ? endDateSimplified : '',
            !isNullOrUndefined(this.selectedSystem) ? this.selectedSystem : '',
            !isNullOrUndefined(this.selectedCompany) ? this.selectedCompany : '',
            !isNullOrUndefined(this.entity.agency) ? this.entity.agency : '',
            !isNullOrUndefined(this.entity.docSap) ? this.entity.docSap : '',
            !isNullOrUndefined(this.entity.idOrigin) ? this.entity.idOrigin : '',
            !isNullOrUndefined(this.entity.account) ? this.entity.account : '',
            !isNullOrUndefined(this.entity.center) ? this.entity.center : '',
            !isNullOrUndefined(this.entity.document) ? this.entity.document : '',
            !isNullOrUndefined(this.entity.type) ? this.entity.type : '',
            !isNullOrUndefined(this.selectedDebitCredit) ? this.selectedDebitCredit : '',
            !isNullOrUndefined(this.selectedActNotAct) ? this.selectedActNotAct : '',
            !isNullOrUndefined(this.selectedAssociated) ? this.selectedAssociated : '',
            SORT_ORDER_DEFAULT,
            PAGE_INDEX_DEFAULT,
            PAGE_SIZE_DEFAULT
        );
    }

    startDateChange(chargeType: string, event: MatDatepickerInputEvent<Date>) {
        this.startDate.value = event.value;
    }

    endDateChange(chargeType: string, event: MatDatepickerInputEvent<Date>) {
        this.endDate.value = event.value;
    }

    exportToCSV() {
        if (this.dataSource.page.totalElements >= 50000) {
            // noinspection TypeScriptValidateTypes
            const dialogRef = this.dialog.open(ExcelDialogComponent, {
                width: '80ex'
            });
        } else {
            this.generateReport();
        }
    }

    private generateReport() {
        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';

        this.actNotActService.find(
            !isNullOrUndefined(startDateSimplified) && startDateSimplified !== '' && startDateSimplified ? startDateSimplified : '',
            !isNullOrUndefined(endDateSimplified) && endDateSimplified !== '' ? endDateSimplified : '',
            !isNullOrUndefined(this.selectedSystem) ? this.selectedSystem : '',
            !isNullOrUndefined(this.selectedCompany) ? this.selectedCompany : '',
            !isNullOrUndefined(this.entity.agency) ? this.entity.agency : '',
            !isNullOrUndefined(this.entity.docSap) ? this.entity.docSap : '',
            !isNullOrUndefined(this.entity.idOrigin) ? this.entity.idOrigin : '',
            !isNullOrUndefined(this.entity.account) ? this.entity.account : '',
            !isNullOrUndefined(this.entity.center) ? this.entity.center : '',
            !isNullOrUndefined(this.entity.document) ? this.entity.document : '',
            !isNullOrUndefined(this.entity.type) ? this.entity.type : '',
            !isNullOrUndefined(this.selectedDebitCredit) ? this.selectedDebitCredit : '',
            !isNullOrUndefined(this.selectedActNotAct) ? this.selectedActNotAct : '',
            !isNullOrUndefined(this.selectedAssociated) ? this.selectedAssociated : '',
            SORT_ORDER_DEFAULT,
            PAGE_INDEX_DEFAULT,
            50000
        ).subscribe(
            (response) => {
                const dataXLS: ActNotActXLS[] = [];
                const itens = response['content'];

                for (const item of itens) {
                    const xls = new ActNotActXLS();
                    xls.sistema = item.system;
                    xls.empresa = item.company;
                    xls.agencia = item.agency;
                    xls.data = item.dateToView;
                    xls.tipo = item.type;
                    xls.id_origem = item.idOrigin;
                    xls.conta = item.account;
                    xls.historico = item.historic;
                    xls.centro = item.center;
                    xls.valor = item.value;
                    xls.debito_credito = item.debitCredit;
                    xls.documento = item.document;
                    xls.sap_documento = item.docSap;

                    if (item.actNotActType === 'ATO') {
                        xls.ato_coop_ato_nao_coop = 'Ato';
                    } else if (item.actNotActType === 'ATO NAO') {
                        xls.ato_coop_ato_nao_coop = 'Ato Não';
                    } else {
                        xls.ato_coop_ato_nao_coop = item.actNotActType;
                    }

                    xls.associado = item.associated;

                    dataXLS.push(xls);
                }

                const ws_name = 'Ato Coop e Ato Não Coop';
                const wb: WorkBook = {SheetNames: [], Sheets: {}};
                const ws: any = utils.json_to_sheet(dataXLS);
                wb.SheetNames.push(ws_name);
                wb.Sheets[ws_name] = ws;
                const wbout = write(wb, {bookType: 'xlsx', bookSST: true, type: 'binary'});

                function s2ab(s) {
                    const buf = new ArrayBuffer(s.length);
                    const view = new Uint8Array(buf);
                    for (let i = 0; i !== s.length; ++i) {
                        // tslint:disable-next-line: no-bitwise
                        view[i] = s.charCodeAt(i) & 0xFF;
                    }

                    return buf;
                }

                saveAs(new Blob([s2ab(wbout)], {type: 'application/octet-stream'}), 'Tabela de atos e atos nao.xlsx');
            });
    }

    reset() {
        this.selectedCompany = '';
        this.selectedActNotAct = '';
        this.selectedSystem = '';
        this.selectedDebitCredit = '';
        this.selectedAssociated = '';
        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.entity = new ActNotAct();
    }
}
