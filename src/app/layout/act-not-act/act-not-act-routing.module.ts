import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ActNotActComponent} from './act-not-act.component';

const routes: Routes = [
    {
        path: '', component: ActNotActComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ActNotActRoutingModule {
}
