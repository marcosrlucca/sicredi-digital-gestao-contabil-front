import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {CompanyService} from '../../shared/services/company.service';
import {isNullOrUndefined} from 'util';
import {System} from '../management-error/evaluation-integration/evaluation-integration.model';

@Injectable()
export class ActNotActService {

    private pathGetAll = `${environment._api_accounting_act_not_act_url}/act-not-act`;
    private pathGetSystems = `${environment._api_accounting_management_url}/sistema`;

    public systems;

    constructor(private httpClient: HttpClient, private companyService: CompanyService) {
    }

    public find(startDate, endDate, system, company, agency, sapCode, idOrigin, account, center, document, type, debitCredit, actOrNotAct, associated,
                sortOrder, pageIndex, pageSize): Observable<any> {
        return this.httpClient.get(this.pathGetAll, {
            params: new HttpParams()
                .set('startDate', startDate)
                .set('endDate', endDate)
                .set('system', system)
                .set('company', company)
                .set('agency', agency)
                .set('sapCode', sapCode)
                .set('idOrigin', idOrigin)
                .set('account', account)
                .set('center', center)
                .set('document', document)
                .set('type', type)
                .set('debitCredit', debitCredit)
                .set('actOrNotAct', actOrNotAct)
                .set('associated', associated)
                .set('sort', sortOrder)
                .set('page', pageIndex)
                .set('size', pageSize)
        });
    }

    public getSystems() {
        if (isNullOrUndefined(this.systems)) {
            this.systems = [];

            this.httpClient.get(this.pathGetSystems).toPromise().then((a: Array<System>) => {
                if (!isNullOrUndefined(a)) {
                    a.forEach(b => this.systems.push({value: b['system'], viewValue: b['description']}));
                }
            }).catch(() => null);
        }

        return this.systems;
    }

    public getCompanies() {
        const companies = this.companyService.companies;

        companies.filter(a =>
            this.notContainsZero(a)).filter(a => +(a['value']) < 1000).forEach(a => a['value'] = '0' + a['value']
        );

        return companies;
    }

    private notContainsZero(a) {
        return !isNullOrUndefined(a['value']) && ('' + a['value']).trim()[0] !== '0';
    }

    public getActNotActTypes() {
        return Observable.of(
            {value: 'ATO', viewValue: 'Ato'},
            {value: 'ATO NAO', viewValue: 'Ato Não'}
        );
    }

    public getAssociatedTypes() {
        return Observable.of(
            {value: 'S', viewValue: 'Sim'},
            {value: 'N', viewValue: 'Não'}
        );
    }

    public getDebitCreditTypes() {
        return Observable.of(
            {value: 'C', viewValue: 'Crédito'},
            {value: 'D', viewValue: 'Débito'}
        );
    }
}
