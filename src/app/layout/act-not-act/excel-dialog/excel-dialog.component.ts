import {MatDialogRef} from '@angular/material';
import {Component} from '@angular/core';

@Component({
    selector: 'app-excel-dialog-dialog',
    templateUrl: 'excel-dialog.component.html',
    styleUrls: ['excel-dialog.component.scss']
})
export class ExcelDialogComponent {

    constructor(public dialogRef: MatDialogRef<ExcelDialogComponent>) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
