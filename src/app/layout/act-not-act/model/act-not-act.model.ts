export class ActNotAct {
    id: string;
    system: string;
    company: string;
    agency: string;
    date: string;
    dateToView: string;
    type: string;
    idOrigin: string;
    account: string;
    historic: string;
    center: string;
    value: string;
    debitCredit: string;
    document: string;
    docSap: string;
    actNotActType: string;
    associated: string;
}

export class ActNotActXLS {
    sistema: string;
    empresa: string;
    agencia: string;
    data: string;
    tipo: string;
    id_origem: string;
    conta: string;
    historico: string;
    centro: string;
    valor: string;
    debito_credito: string;
    documento: string;
    sap_documento: string;
    ato_coop_ato_nao_coop: string;
    associado: string;
}
