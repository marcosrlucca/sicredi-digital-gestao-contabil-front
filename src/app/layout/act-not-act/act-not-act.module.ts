import {NgModule} from '@angular/core';

import {PageHeaderModule} from './../../shared';
import {
    MatButtonModule, MatDatepickerModule, MatDialogModule, MatInputModule, MatNativeDateModule, MatPaginatorIntl, MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSelectModule, MatSortModule, MatTableModule
} from '@angular/material';
import {NgbAlertModule, NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';
import {ActNotActComponent} from './act-not-act.component';
import {ActNotActService} from './act-not-act.service';
import {ActNotActRoutingModule} from './act-not-act-routing.module';
import {ExcelDialogComponent} from './excel-dialog/excel-dialog.component';
import {SharedModule} from '../../shared/modules/shared/shared.module';
import {MatPaginatorIntlBr} from '../../shared/services/mat-paginator-intl-br';

@NgModule({
    imports:
        [
            SharedModule,
            FormsModule,
            TextMaskModule,
            ReactiveFormsModule,
            MatSelectModule,
            MatNativeDateModule,
            MatDatepickerModule,
            MatInputModule,
            MatDialogModule,
            MatButtonModule,
            MatTableModule,
            MatSortModule,
            MatProgressSpinnerModule,
            MatPaginatorModule,
            NgbCarouselModule.forRoot(),
            NgbAlertModule.forRoot(),
            ActNotActRoutingModule,
            PageHeaderModule
        ],
    entryComponents: [
        ExcelDialogComponent
    ],
    providers: [
        ActNotActService,
        {provide: MatPaginatorIntl, useClass: MatPaginatorIntlBr}
    ],
    declarations:
        [
            ExcelDialogComponent,
            ActNotActComponent
        ]
})
export class ActNotActModule {
}
