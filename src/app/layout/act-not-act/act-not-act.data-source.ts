import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Page} from '../../shared/domain/page.model';
import {ActNotAct} from './model/act-not-act.model';
import {ActNotActService} from './act-not-act.service';

export class ActNotActDataSource implements DataSource<ActNotAct> {

    itemsSubject = new BehaviorSubject<ActNotAct[]>([]);
    page = new Page();

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubjects.asObservable();

    public noDataFound = false;

    constructor(private actNotActService: ActNotActService) {
        this.page.pageSizeOptions = [5, 10, 20, 50];
    }

    connect(collectionViewer: CollectionViewer): Observable<ActNotAct[]> {
        return this.itemsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.itemsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadContent(startDate, endDate, system, company, agency, sapCode, idOrigin, account, center, document, type, debitCredit, actOrNotAct, associated,
                sortOrder, pageIndex, pageSize) {
        this.loadingSubjects.next(true);

        this.actNotActService.find(startDate, endDate, system, company, agency, sapCode, idOrigin, account, center, document, type, debitCredit, actOrNotAct,
            associated, sortOrder, pageIndex, pageSize)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false)))
            .subscribe(response => {
                if (response !== null) {
                    this.itemsSubject.next(response['content']);
                    this.page = response;

                    if (this.page.totalElements >= 20) {
                        this.page.pageSizeOptions = [5, 10, 20, 50];
                    } else if (this.page.totalElements >= 10) {
                        this.page.pageSizeOptions = [5, 10, 20];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    } else {
                        this.page.pageSizeOptions = [5, 10];
                        this.page.numberOfElementsPerPage = this.page.totalElements;
                    }

                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }
}
