import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'gestao-erro',
                loadChildren: './management-error/management-error.module#ManagementErrorModule'
            },
            {
                path: 'parametrizacao',
                loadChildren: './parameterization/parameterization.module#ParameterizationModule'
            },
            {
                path: 'ato-nao-ato',
                loadChildren: './act-not-act/act-not-act.module#ActNotActModule'
            },
            {
                path: 'engine',
                loadChildren: './engine/engine.module#EngineModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {
}
