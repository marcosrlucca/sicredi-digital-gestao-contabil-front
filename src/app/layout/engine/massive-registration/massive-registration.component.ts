import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MassiveRegistrationDataSource} from './massive-registration.data-source';
import {MatDatepickerInputEvent, MatDialog, MatPaginator, MatSort} from '@angular/material';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MassiveRegistrationModel} from './massive-registration.model';
import {MassiveRegistrationService} from './massive-registration.service';
import {ToastsManager} from 'ng2-toastr';
import {merge} from 'rxjs/observable/merge';
import {tap} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {routerTransition} from '../../../router.animations';
import {StatusMassiveRegistrationService} from '../../../shared/services/status-massive-registration.service';

@Component({
    selector: 'app-massive-registration',
    templateUrl: './massive-registration.component.html',
    styleUrls: ['./massive-registration.component.scss'],
    animations: [routerTransition()]
})
export class MassiveRegistrationComponent implements OnInit, AfterViewInit {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    public displayedColumns = [
        'system', 'baseDate', 'eventId', 'event', 'debitUnit', 'creditUnit', 'reversal', 'inputError', 'identificationOrigin',
        'transactionId', 'personDocument', 'rule1', 'rule2', 'rule3', 'rule4', 'rule5', 'rule6', 'rule7', 'rule8', 'rule9', 'rule10',
        'value1', 'value2', 'value3', 'value4', 'value5', 'value6', 'value7', 'value8', 'value9', 'value10'];

    public dataSource: MassiveRegistrationDataSource;
    public form: FormGroup;
    public selectedSystem;
    public systems = [];
    public startDate;
    public endDate;
    public startCalendar;
    public endCalendar;
    public entity: MassiveRegistrationModel;

    constructor(private formBuilder: FormBuilder,
                private service: MassiveRegistrationService,
                private toastr: ToastsManager,
                private statusService: StatusMassiveRegistrationService,
                public dialog: MatDialog) {
    }

    ngOnInit() {
        this.dataSource = new MassiveRegistrationDataSource(this.service);
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.startDate = new FormControl(new Date());
        this.endDate = new FormControl(new Date());

        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);

        this.entity = new MassiveRegistrationModel();

        this.systems = this.service.systems;

        this.form = this.formBuilder.group({
            eventId: new FormControl('', []),
            event: new FormControl('', []),
            debitUnit: new FormControl('', []),
            creditUnit: new FormControl('', []),
            reversal: new FormControl('', [])
        });

        this.sort.active = 'baseDate';
        this.sort.direction = 'desc';

        if (this.statusService.isErrorChosed()) {
            this.loadContent();
            this.statusService.resetChoice();
        }
    }

    ngAfterViewInit(): void {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page).pipe(tap(() => this.loadContent())).subscribe();
    }

    private loadContent() {
        this.dataSource = new MassiveRegistrationDataSource(this.service);

        if (this.isDatesInvalid()) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.getDateOfMonth(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.getDateOfMonth(this.endDate.value) : '';
        const sort = !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
            `${this.sort.active},${this.sort.direction}` : 'baseDate,desc';
        const systems = !isNullOrUndefined(this.selectedSystem) ? this.selectedSystem : '';
        const eventId = !isNullOrUndefined(this.entity.eventId) ? this.entity.eventId : '';
        const event = !isNullOrUndefined(this.entity.event) ? this.entity.event : '';
        const debitUnit = !isNullOrUndefined(this.entity.debitUnit) ? this.entity.debitUnit : '';
        const creditUnit = !isNullOrUndefined(this.entity.creditUnit) ? this.entity.creditUnit : '';
        const reversal = !isNullOrUndefined(this.entity.reversal) ? this.entity.reversal : '';
        const startDate = !isNullOrUndefined(startDateSimplified) && startDateSimplified !== '' && startDateSimplified ? startDateSimplified : '';
        const endDate = !isNullOrUndefined(endDateSimplified) && endDateSimplified !== '' ? endDateSimplified : '';
        const pageIndex = this.paginator.pageIndex;
        const pageSize = this.paginator.pageSize;

        this.dataSource.loadContent(systems, eventId, event, debitUnit, creditUnit, reversal, startDate, endDate, sort, pageIndex, pageSize);
    }

    public onSubmit() {
        this.loadContent();
        this.reset();
    }

    public startDateChange(event: MatDatepickerInputEvent<Date>) {
        this.startDate.value = event.value;
    }

    public endDateChange(event: MatDatepickerInputEvent<Date>) {
        this.endDate.value = event.value;
    }

    private isDatesInvalid() {
        return !isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) && this.startDate.value > this.endDate.value;
    }

    private getDateOfMonth(dateToTransform: Date) {
        const dayStartDate = dateToTransform.getDate();
        const monthStartDate = dateToTransform.getMonth() + 1;
        const yearStartDate = dateToTransform.getFullYear();

        return `${dayStartDate}/${monthStartDate}/${yearStartDate}`;
    }

    public reset() {
        this.selectedSystem = '';
        this.endDate.value = null;
        this.startDate.value = null;
        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.entity = new MassiveRegistrationModel();
    }

}
