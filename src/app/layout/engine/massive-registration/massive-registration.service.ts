import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {isNullOrUndefined} from 'util';
import {SystemService} from '../../../shared/services/system.service';

@Injectable()
export class MassiveRegistrationService {

    private urlGet = `${environment._api_inconsistence_massive}/massive-registration`;
    public systems;

    constructor(private httpClient: HttpClient, private systemService: SystemService) {
        this.systems = this.systemService.systems;
    }

    findAll(systems, eventId, event, debitUnit, creditUnit, reversal, startDate, endDate, sort, page, size) {
        let params = new HttpParams();

        params = !isNullOrUndefined(systems) ? params.set('systems', systems) : params;
        params = !isNullOrUndefined(systems) ? params.set('eventId', eventId) : params;
        params = !isNullOrUndefined(systems) ? params.set('event', event) : params;
        params = !isNullOrUndefined(systems) ? params.set('debitUnit', debitUnit) : params;
        params = !isNullOrUndefined(systems) ? params.set('creditUnit', creditUnit) : params;
        params = !isNullOrUndefined(systems) ? params.set('reversal', reversal) : params;
        params = !isNullOrUndefined(systems) ? params.set('startDate', startDate) : params;
        params = !isNullOrUndefined(systems) ? params.set('endDate', endDate) : params;
        params = !isNullOrUndefined(systems) ? params.set('sort', sort) : params;
        params = !isNullOrUndefined(systems) ? params.set('page', page) : params;
        params = !isNullOrUndefined(systems) ? params.set('size', size) : params;

        return this.httpClient.get(this.urlGet, {params});
    }
}
