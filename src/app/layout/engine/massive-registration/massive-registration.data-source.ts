import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {MassiveRegistrationModel} from './massive-registration.model';
import {Page} from '../../../shared/domain/page.model';
import {MassiveRegistrationService} from './massive-registration.service';

export class MassiveRegistrationDataSource implements DataSource<MassiveRegistrationModel> {

    itemsSubject = new BehaviorSubject<MassiveRegistrationModel[]>([]);
    page = new Page();

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubjects.asObservable();

    public noDataFound = false;

    constructor(private service: MassiveRegistrationService) {
        this.page.pageSizeOptions = [5, 10, 20, 50];
    }

    connect(collectionViewer: CollectionViewer): Observable<MassiveRegistrationModel[]> {
        return this.itemsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.itemsSubject.complete();
        this.loadingSubjects.complete();
    }

    loadContent(systems, eventId, event, debitUnit, creditUnit, reversal, startDate, endDate, sort, page, size) {
        this.loadingSubjects.next(true);

        this.service.findAll(systems, eventId, event, debitUnit, creditUnit, reversal, startDate, endDate, sort, page, size)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false)))
            .subscribe(response => {
                if (response !== null) {
                    this.itemsSubject.next(response['content']);

                    this.itemsSubject.value.forEach((m: MassiveRegistrationModel) => {
                        m.baseDate = this.getDateOfMonth(new Date(m.baseDate));
                    });

                    this.page.totalElements = response['totalElements'];
                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }

    private getDateOfMonth(dateToTransform: Date) {
        const dayStartDate = dateToTransform.getDate();
        const monthStartDate = dateToTransform.getMonth() + 1;
        const yearStartDate = dateToTransform.getFullYear();

        return `${dayStartDate}/${monthStartDate}/${yearStartDate}`;
    }
}
