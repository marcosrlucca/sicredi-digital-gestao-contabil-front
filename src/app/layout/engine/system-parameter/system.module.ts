import {NgModule} from '@angular/core';
import {NgbAlertModule, NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TabModule} from 'angular-tabs-component';
import {SharedModule} from '../../../shared/modules/shared/shared.module';
import {PageHeaderModule, SharedPipesModule, StatModule} from '../../../shared';
import {MaterialSharedModule} from '../../../shared/modules/shared/MaterialSharedModule';
import {SystemRoutingModule} from './system-routing.module';
import {ApproveDialogComponent} from './management/approve-dialog/approve-dialog.component';
import {DisapproveDialogComponent} from './management/disapprove-dialog/disapprove-dialog.component';
import {ManagementService} from './management/management.service';
import {SystemComponent} from './system.component';
import {ManagementComponent} from './management/management.component';
import {RegisterMaintenanceComponent} from './register-maintenance/register-maintenance.component';
import {EditRequestDialogComponent} from './register-maintenance/edit-request-dialog/edit-request-dialog.component';
import {FinalizeDialogComponent} from './register-maintenance/finalize-dialog/finalize-dialog.component';
import {RemoveDraftDialogComponent} from './register-maintenance/remove-draft-dialog/remove-draft-dialog.component';
import {RemoveRequestDialogComponent} from './register-maintenance/remove-request-dialog/remove-request-dialog.component';
import {SendDialogComponent} from './register-maintenance/send-dialog/send-dialog.component';
import {SaveDialogComponent} from './register-maintenance/save-dialog/save-dialog.component';
import {SystemParameterService} from './register-maintenance/system-parameter.service';
import {SystemParameterControlService} from './register-maintenance/system-parameter-control.service';

@NgModule({
    imports:
        [
            SharedModule,
            MaterialSharedModule,
            TabModule,
            FormsModule,
            NgbCarouselModule.forRoot(),
            NgbAlertModule.forRoot(),
            SystemRoutingModule,
            ReactiveFormsModule,
            PageHeaderModule,
            StatModule
        ],
    entryComponents: [
        ApproveDialogComponent,
        DisapproveDialogComponent,
        EditRequestDialogComponent,
        FinalizeDialogComponent,
        RemoveDraftDialogComponent,
        RemoveRequestDialogComponent,
        SaveDialogComponent,
        SendDialogComponent
    ],
    providers: [
        SystemParameterService,
        SystemParameterControlService,
        ManagementService
    ],
    declarations:
        [
            SystemComponent,
            ManagementComponent,
            RegisterMaintenanceComponent,
            ApproveDialogComponent,
            DisapproveDialogComponent,
            EditRequestDialogComponent,
            FinalizeDialogComponent,
            RemoveDraftDialogComponent,
            RemoveRequestDialogComponent,
            SaveDialogComponent,
            SendDialogComponent
        ]
})
export class SystemModule {
}
