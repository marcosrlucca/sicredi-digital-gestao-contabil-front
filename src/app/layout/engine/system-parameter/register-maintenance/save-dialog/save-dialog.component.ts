import {MAT_DIALOG_DATA, MatDatepickerInputEvent, MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {MyErrorStateMatcher} from '../../../../../shared/services/my-error-state-matcher';
import {DateUtils} from '../../../../../shared/utils/date.utils';
import {ISystem} from '../../system.model';
import * as constants from '../../../constants';

@Component({
    selector: 'app-save-dialog',
    templateUrl: 'save-dialog.component.html',
    styleUrls: ['save-dialog.component.scss']
})
export class SaveDialogComponent implements OnInit {

    @Output() doSave = new EventEmitter();
    @Output() doCancel = new EventEmitter();
    startDate;
    endDate;
    matcher;
    startCalendarFormControl: FormControl;
    endCalendarFormControl: FormControl;
    systemFormControl: FormControl;
    systemDescriptionFormControl: FormControl;
    sapCodeFormControl: FormControl;
    emailFormControl: FormControl;
    system: ISystem;
    canFillEndDate: boolean;

    constructor(private dateUtils: DateUtils,
                public dialogRef: MatDialogRef<SaveDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: ISystem) {
    }

    ngOnInit(): void {
        this.startCalendarFormControl = new FormControl('', [Validators.required]);
        this.endCalendarFormControl = new FormControl('', []);
        this.systemFormControl = new FormControl('', [Validators.required, Validators.maxLength(20)]);
        this.systemDescriptionFormControl = new FormControl('', [Validators.required, Validators.maxLength(100)]);
        this.sapCodeFormControl = new FormControl('', [Validators.required, Validators.maxLength(2)]);
        this.emailFormControl = new FormControl('', [Validators.required, Validators.maxLength(100)]);

        this.system = this.data['parameter'];
        this.matcher = new MyErrorStateMatcher();

        // Data fim é permitida somente depois da aprovação do cadastro
        this.canFillEndDate = !isNullOrUndefined(this.system.id);

        if (!isNullOrUndefined(this.data['parameter'].validityStart)) {
            this.startDate = this.dateUtils.parseStringDateToMaterial(this.data['parameter'].validityStart);
        }

        if (!isNullOrUndefined(this.data['parameter'].validityEnd)) {
            this.endDate = this.dateUtils.parseStringDateToMaterial(this.data['parameter'].validityEnd);
        }

        if (isNullOrUndefined(this.data['parameter'].validityStart)) {
            this.startCalendarFormControl = new FormControl('', Validators.required);
            this.startDate = '';
        }

        if (isNullOrUndefined(this.data['parameter'].validityEnd)) {
            this.endCalendarFormControl = new FormControl('');
            this.endDate = '';
        }
    }

    onNoClick(): void {
        this.doCancel.emit();
        this.dialogRef.close();
    }

    onReset() {
        this.startCalendarFormControl.reset();
        this.endCalendarFormControl.reset();
        this.systemFormControl.reset();
        this.systemDescriptionFormControl.reset();
        this.sapCodeFormControl.reset();
        this.emailFormControl.reset();
    }

    onSave() {
        if (!this.validate()) {
            return;
        }

        this.system.validityStart = this.startDate;
        this.system.validityEnd = this.endDate;
        this.system.status = constants.DRAFT_VALUE;

        this.doSave.emit(this.system);
        this.dialogRef.close();
    }

    onSend() {
        if (!this.validate()) {
            return;
        }

        this.system.validityStart = this.startDate;
        this.system.validityEnd = this.endDate;
        this.system.status = constants.SENT_VALUE;

        this.doSave.emit(this.system);
        this.dialogRef.close();
    }

    private validate(): boolean {
        if (!this.startCalendarFormControl.valid || !this.endCalendarFormControl.valid || !this.systemFormControl.valid ||
            !this.systemDescriptionFormControl.valid || !this.sapCodeFormControl.valid || !this.emailFormControl.valid) {
            return false;
        }

        if (!isNullOrUndefined(this.endDate) && this.endDate !== '' && this.startDate > this.endDate) {
            // noinspection TypeScriptValidateTypes
            this.endCalendarFormControl.setErrors(Validators.compose);
            return false;
        } else {
            this.endCalendarFormControl.setErrors(null);
        }

        return true;
    }

    startDateChange(event: MatDatepickerInputEvent<Date>) {
        if (!isNullOrUndefined(event.value)) {
            this.startDate.value = event.value;
            this.data['parameter'].validityStart = this.getDateAsString(event);
        }
    }

    endDateChange(event: MatDatepickerInputEvent<Date>) {
        if (!isNullOrUndefined(event.value)) {
            this.endDate.value = event.value;
            this.data['parameter'].validityEnd = this.getDateAsString(event);
        } else {
            this.data['parameter'].validityEnd = null;
        }
    }

    getDateAsString(event: MatDatepickerInputEvent<Date>) {
        const date = event.value.getDate() < 10 ? `0${event.value.getDate()}` : event.value.getDate();
        const month = event.value.getMonth() < 10 ? `0${event.value.getMonth() + 1}` : event.value.getMonth() + 1;
        const year = event.value.getFullYear();

        return `${date}/${month}/${year}`;
    }
}
