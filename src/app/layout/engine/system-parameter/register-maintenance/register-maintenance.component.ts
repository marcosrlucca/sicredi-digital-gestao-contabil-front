import {AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {MatDatepickerInputEvent, MatDialog, MatPaginator, MatSort, SortDirection} from '@angular/material';
import {FormBuilder, FormControl, FormGroupDirective} from '@angular/forms';
import {ToastsManager} from 'ng2-toastr';
import {tap} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {merge} from 'rxjs/observable/merge';
import {HttpStatus} from '../../../../shared/domain/http-status';
import {HttpErrorResponse} from '@angular/common/http';
import {UserService} from '../../../../configuration/user.service';
import {RegisterMaintenanceDataSource} from './register-maintenance.data-source';
import {MessageHandler} from '../../../../shared/utils/message.handler';
import {DateUtils} from '../../../../shared/utils/date.utils';
import {AbstractEngineController} from '../../abstract-engine-controller';
import {ISystem, ISystemControl, SystemImpl} from '../system.model';
import {SaveDialogComponent} from './save-dialog/save-dialog.component';
import {FinalizeDialogComponent} from './finalize-dialog/finalize-dialog.component';
import {RemoveDraftDialogComponent} from './remove-draft-dialog/remove-draft-dialog.component';
import {SendDialogComponent} from './send-dialog/send-dialog.component';
import {RemoveRequestDialogComponent} from './remove-request-dialog/remove-request-dialog.component';
import {EditRequestDialogComponent} from './edit-request-dialog/edit-request-dialog.component';
import {SystemParameterService} from './system-parameter.service';
import {SystemParameterControlService} from './system-parameter-control.service';
import * as constants from '../../constants';
import {PermissionService} from '../../../../shared/services/permission.service';

const SORT_ACTIVE_DEFAULT = 'validityStart';
const SORT_DIRECTION_DEFAULT = 'asc';
const SORT_ORDER_DEFAULT = 'validityStart,asc';

const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
    selector: 'app-register-maintenance',
    templateUrl: './register-maintenance.component.html',
    styleUrls: ['./register-maintenance.component.scss']
})
export class RegisterMaintenanceComponent extends AbstractEngineController implements OnInit, AfterViewInit {
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @Output() doToastMessage = new EventEmitter();

    dataSource: RegisterMaintenanceDataSource;
    displayedColumns = ['validityStart', 'validityEnd', 'system', 'systemDescription', 'sapCode', 'registerResponsible',
        'emailResponsible', 'status'];
    highlightedRows = [];
    systems = [];
    allStatus = [];
    system;
    startCalendar;
    endCalendar;
    startDate;
    endDate;
    form;
    selectedSystem;
    selectedStatus;
    currentObj: ISystem;

    constructor(private dateUtils: DateUtils,
                private formBuilder: FormBuilder,
                private messageHandler: MessageHandler,
                private service: SystemParameterService,
                private controlService: SystemParameterControlService,
                private userService: UserService,
                private toastr: ToastsManager,
                public permission: PermissionService,
                public dialog: MatDialog) {
        super();
    }

    ngOnInit() {
        this.dataSource = new RegisterMaintenanceDataSource(this.dateUtils, this.service);

        this.startCalendar = new FormControl('', []);
        this.endCalendar = new FormControl('', []);
        this.startDate = new FormControl(new Date());
        this.endDate = new FormControl(new Date());

        this.form = this.formBuilder.group({
            system: ['']
        });

        this.endDate.value = null;
        this.startDate.value = null;

        this.sort.active = SORT_ACTIVE_DEFAULT;
        this.sort.direction = <SortDirection>SORT_DIRECTION_DEFAULT;

        // load the selects
        this.systems = this.service.getSystemsName();
        this.service.getStatus().subscribe(item => this.allStatus.push(item));
    }

    ngAfterViewInit() {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page).pipe(tap(() => this.loadData())).subscribe();
    }

    isUserAuthorized(): boolean {
        if (isNullOrUndefined(this.currentObj.control) || this.currentObj.control.registerResponsible !== this.userService.currentUser.username) {
            return false;
        }

        return true;
    }

    reset(formDirective: FormGroupDirective) {
        this.startCalendar.reset();
        this.endCalendar.reset();

        this.startDate.value = '';
        this.endDate.value = '';
        this.highlightedRows = [];
        this.selectedSystem = '';
        this.selectedStatus = '';

        if (!isNullOrUndefined(formDirective)) {
            formDirective.resetForm();
        }
    }

    reloadData() {
        if (this.dataSource.isDataSourceEmpty()) {
            this.dataSource = new RegisterMaintenanceDataSource(this.dateUtils, this.service);
        } else {
            this.loadData();
        }
    }

    loadData() {
        this.dataSource = new RegisterMaintenanceDataSource(this.dateUtils, this.service);

        if (this.validateDate()) {
            this.toastr.warning('Data início não pode ser maior que a data fim', null, {toastLife: 3000});
            return;
        }

        const startDateSimplified = !isNullOrUndefined(this.startDate.value) ? this.dateUtils.parseDateToLocaleEnUS(this.startDate.value) : '';
        const endDateSimplified = !isNullOrUndefined(this.endDate.value) ? this.dateUtils.parseDateToLocaleEnUS(this.endDate.value) : '';
        const startDateValue = !isNullOrUndefined(startDateSimplified) && startDateSimplified !== '' && startDateSimplified ? startDateSimplified : '';
        const endDateValue = !isNullOrUndefined(endDateSimplified) && endDateSimplified !== '' ? endDateSimplified : '';
        const systemValue = !isNullOrUndefined(this.selectedSystem) ? this.selectedSystem : '';
        const statusValue = !isNullOrUndefined(this.selectedStatus) ? this.selectedStatus : '';
        const sortValue = !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
            `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT;
        const pageIndexValue = !isNullOrUndefined(this.paginator) && !isNullOrUndefined(this.paginator.pageIndex) ? this.paginator.pageIndex :
            PAGE_INDEX_DEFAULT;
        const pageSizeValue = !isNullOrUndefined(this.paginator) && !isNullOrUndefined(this.paginator.pageSize) ? this.paginator.pageSize :
            PAGE_SIZE_DEFAULT;

        this.dataSource.loadData(startDateValue, endDateValue, systemValue, statusValue, sortValue, pageIndexValue, pageSizeValue);
    }

    onSelectRow(row: ISystem) {
        const index = this.highlightedRows.findIndex((a: ISystem) => a.id === row.id);

        if (index === -1 && this.highlightedRows.length < 1) { // permite adicionar somente 1 elemento por vez
            this.highlightedRows.push(row);
        } else {
            this.highlightedRows.splice(index, 1);
            this.highlightedRows.push(row);
        }

        this.currentObj = row;
    }

    startDateChange(event: MatDatepickerInputEvent<Date>) {
        this.startDate.value = event.value;
    }

    endDateChange(event: MatDatepickerInputEvent<Date>) {
        this.endDate.value = event.value;
    }

    onSubmitSearch() {
        this.loadData();
    }

    edit(formDirective: FormGroupDirective) {
        const newObjToEdit = this.clone(this.currentObj);

        const dialogRef = this.dialog.open(SaveDialogComponent, {
            width: '100ex',
            data: {parameter: newObjToEdit}
        });

        dialogRef.componentInstance.doSave.subscribe((result) => this.doSave(formDirective, result));
    }

    create(formDirective: FormGroupDirective) {
        const newObjToEdit = new SystemImpl();

        const dialogRef = this.dialog.open(SaveDialogComponent, {
            width: '100ex',
            data: {parameter: newObjToEdit}
        });

        dialogRef.componentInstance.doSave.subscribe((result) => this.doSave(formDirective, result));
    }

    finalize(formDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1) {
            return;
        }

        // noinspection TypeScriptValidateTypes
        const dialogRef = this.dialog.open(FinalizeDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.finalizeEventEmitter.subscribe(() => this.doFinalize(formDirective));
    }

    removeDraft(formDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1 ||
            isNullOrUndefined(this.currentObj) || isNullOrUndefined(this.currentObj.control)) {
            return;
        }

        const dialogRef = this.dialog.open(RemoveDraftDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.removeDraftEventEmitter.subscribe(() => this.doRemoveDraft(formDirective));
    }

    editDraft(formDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1 ||
            isNullOrUndefined(this.currentObj.control)) {
            return;
        }

        const newObjToEdit = this.clone(this.currentObj);

        const dialogRef = this.dialog.open(EditRequestDialogComponent, {
            width: '100ex',
            data: {parameter: newObjToEdit.control}
        });

        dialogRef.componentInstance.doSave.subscribe((result: ISystemControl) => this.doSaveSystemParameterControl(formDirective, result));
    }

    sendDraftToApprove(formDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1 ||
            isNullOrUndefined(this.currentObj.control)) {
            return;
        }

        const dialogRef = this.dialog.open(SendDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.sendEventEmitter.subscribe(() => this.doSendDraftToApprove(formDirective));
    }

    cancelRequest(formDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1) {
            return;
        }

        const dialogRef = this.dialog.open(RemoveRequestDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.removeRequestEventEmitter.subscribe(() => this.doRemoveRequestEventEmitter(formDirective));
    }

    reviewRequest(formDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1 ||
            isNullOrUndefined(this.currentObj.control)) {
            return;
        }

        const newObjToEdit = this.clone(this.currentObj);

        const dialogRef = this.dialog.open(EditRequestDialogComponent, {
            width: '100ex',
            data: {parameter: newObjToEdit.control}
        });

        dialogRef.componentInstance.doSave.subscribe((result: ISystemControl) => {
            if (result.status === constants.SENT_VALUE) {
                this.currentObj.control = result;
                this.doSendDraftToApprove(formDirective);
            } else {
                this.doSaveSystemParameterControl(formDirective, result);
            }
        });
    }

    private doRemoveRequestEventEmitter(formDirective: FormGroupDirective) {
        this.setUserResponsible(this.currentObj.control);

        this.controlService.remove(this.currentObj.control).subscribe(() => {
            this.loadData();
            this.reset(formDirective);

            setTimeout(() => {
                this.toastr.info('Solicitação cancelada com sucesso', null, {toastLife: 3000});
            }, 500);
        }, (error: HttpErrorResponse) => {
            if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                this.toastr.error(error.error.msg, null, {toastLife: 3000});
            }
        });
    }

    private doSendDraftToApprove(formDirective: FormGroupDirective) {
        this.setSystemControl(this.currentObj.control);

        this.currentObj.control.status = constants.SENT_VALUE;

        this.controlService.saveOrUpdate(this.currentObj.control).subscribe(() => {
            this.loadData();
            this.reset(formDirective);

            setTimeout(() => {
                this.toastr.info('Rascunho enviado para aprovação com sucesso', null, {toastLife: 3000});
            }, 500);
        }, (error: HttpErrorResponse) => {
            if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                this.toastr.error(error.error.msg, null, {toastLife: 3000});
            }
        });
    }

    private doRemoveDraft(formDirective) {
        this.setUserResponsible(this.currentObj.control);

        this.controlService.remove(this.currentObj.control).subscribe(() => {
            this.loadData();
            this.reset(formDirective);

            setTimeout(() => {
                this.toastr.info('Rascunho removido com sucesso', null, {toastLife: 3000});
            }, 500);
        }, (error: HttpErrorResponse) => {
            if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                this.toastr.error(error.error.msg, null, {toastLife: 3000});
            }
        });
    }

    private doSaveSystemParameterControl(formDirective: FormGroupDirective, control: ISystemControl) {
        this.setSystemControl(control);

        this.controlService.saveOrUpdate(control).subscribe(() => {
            this.loadData();
            this.reset(formDirective);

            setTimeout(() => {
                this.toastr.info('Rascunho salvo com sucesso', null, {toastLife: 3000});
            }, 500);
        }, (error: HttpErrorResponse) => {
            if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                this.toastr.error(error.error.msg, null, {toastLife: 3000});
            }
        });
    }

    private setUserResponsible(obj) {
        obj.registerResponsible = this.userService.currentUser['username'];
    }

    private doSave(formDirective: FormGroupDirective, system: ISystem) {
        this.setSystem(system);

        this.service.saveOrUpdate(system).subscribe(() => {
            this.loadData();
            this.reset(formDirective);

            setTimeout(() => {

                if (!isNullOrUndefined(system) && system.status === constants.DRAFT_VALUE) {
                    this.toastr.info('Rascunho salvo com sucesso', null, {toastLife: 3000});
                } else {
                    this.toastr.info('Parâmetro enviado para aprovação com sucesso', null, {toastLife: 3000});
                }

            }, 500);
        }, (error: HttpErrorResponse) => {
            if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                this.toastr.error(error.error.msg, null, {toastLife: 3000});
            }
        });
    }

    private doFinalize(formDirective: FormGroupDirective) {
        const system = this.highlightedRows.pop();
        this.setSystem(system);

        this.service.remove(system).subscribe(
            () => {
                this.loadData();
                this.reset(formDirective);

                setTimeout(() => {
                    this.toastr.info('Parâmetro enviado para finalização com sucesso', null, {toastLife: 3000});
                }, 500);
            }, (error: HttpErrorResponse) => {
                if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                    this.toastr.error(error.error.msg, null, {toastLife: 3000});
                }
            });
    }

    private validateDate() {
        return !isNullOrUndefined(this.startDate.value) && !isNullOrUndefined(this.endDate.value) &&
            this.startDate.value !== '' && this.endDate.value !== '' && this.startDate.value > this.endDate.value;
    }

    private clone(toClone: ISystem): ISystem {
        const clone = Object.assign({}, toClone);
        clone.control = Object.assign({}, toClone.control);

        return clone;
    }

    private setSystemControl(control: ISystemControl) {
        if (!isNullOrUndefined(control.validityEnd) && control.validityEnd !== '') {
            if (control.validityStart instanceof Date) {
                control.validityEnd = this.dateUtils.parseDateToLocaleEnUS(control.validityEnd);
            }
        }

        this.setUserResponsible(control);

        if (!isNullOrUndefined(control.validityStart) && control.validityStart !== '') {
            if (control.validityStart instanceof Date) {
                control.validityStart = this.dateUtils.parseDateToLocaleEnUS(control.validityStart);
            }
        }
    }

    private setSystem(system: ISystem) {
        if (!isNullOrUndefined(system.validityEnd) && system.validityEnd !== '') {
            if (system.validityStart instanceof Date) {
                system.validityEnd = this.dateUtils.parseDateToLocaleEnUS(system.validityEnd);
            }
        }

        this.setUserResponsible(system);

        if (!isNullOrUndefined(system.validityStart) && system.validityStart !== '') {
            if (system.validityStart instanceof Date) {
                system.validityStart = this.dateUtils.parseDateToLocaleEnUS(system.validityStart);
            }
        }
    }
}
