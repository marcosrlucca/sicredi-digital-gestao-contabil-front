import {MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-remove-request-dialog',
    templateUrl: 'remove-request-dialog.component.html',
    styleUrls: ['remove-request-dialog.component.scss']
})
export class RemoveRequestDialogComponent {
    @Output() removeRequestEventEmitter = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<RemoveRequestDialogComponent>) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onYesClick() {
        this.dialogRef.close();
        this.removeRequestEventEmitter.emit();
    }

}
