import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';
import {ISystem, ISystemDescription, ISystemResponse} from '../system.model';

@Injectable()
export class SystemParameterService {

    private path = `${environment._api_engine_system_par}/system`;
    public systems = [];

    constructor(private httpClient: HttpClient) {
    }

    findAll(startDate, endDate, systemValue, statusValue, sortOrder, pageIndex, pageSize): Observable<ISystemResponse> {
        let params = new HttpParams();

        params = !isNullOrUndefined(systemValue) ? params.set('system', systemValue) : params;
        params = !isNullOrUndefined(statusValue) ? params.set('status', statusValue) : params;
        params = !isNullOrUndefined(startDate) ? params.set('startDate', startDate) : params;
        params = !isNullOrUndefined(endDate) ? params.set('endDate', endDate) : params;
        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;
        params = !isNullOrUndefined(pageIndex) ? params.set('page', pageIndex.toString()) : params;
        params = !isNullOrUndefined(pageSize) ? params.set('size', pageSize.toString()) : params;

        return this.httpClient.get<ISystemResponse>(this.path, {params}).catch(this.errorHandler);
    }

    public getSystemsName() {
        if (isNullOrUndefined(this.systems) || this.systems.length === 0) {
            this.httpClient.get<Array<ISystemDescription>>(`${this.path}/description`).toPromise().then(result =>
                result
                    .filter((value, index, array) => !array
                        .filter((v, i) => {
                            const valueOutter = value['system'].toUpperCase();
                            const valueInner = v['system'].toUpperCase();

                            return JSON.stringify(valueOutter) === JSON.stringify(valueInner) && i < index;
                        }).length)
                    .sort((v1, v2) => {
                        const val1 = v1['system'].toUpperCase();
                        const val2 = v2['system'].toUpperCase();

                        return val1 > val2 ? 1 : val1 < val2 ? -1 : 0;
                    })
                    .forEach(systemDescriptionInst => this.systems.push({
                        value: systemDescriptionInst['systemDescription'],
                        viewValue: systemDescriptionInst['system']
                    })))
                .catch(this.errorHandler);
        }

        return this.systems;
    }

    public getStatus() {
        return Observable.of(
            {value: 'actual', viewValue: 'Ativo'},
            {value: 'sent', viewValue: 'Enviado'},
            {value: 'finalized', viewValue: 'Finalizado'},
            {value: 'draft', viewValue: 'Rascunho'},
            {value: 'disapproved', viewValue: 'Reprovado'}
        );
    }

    saveOrUpdate(currentObj: ISystem): Observable<any> {
        if (isNullOrUndefined(currentObj.id)) {
            return this.httpClient.post(this.path, currentObj).catch(this.errorHandler);
        }

        return this.httpClient.put(this.path, currentObj).catch(this.errorHandler);
    }

    remove(currentObj: ISystem): Observable<any> {
        if (isNullOrUndefined(currentObj) || isNullOrUndefined(currentObj.id)) {
            return Observable.throw('ID não pode estar vazio');
        }
        if (isNullOrUndefined(currentObj.registerResponsible)) {
            return Observable.throw('Usuário não pode estar vazio');
        }

        const options = {body: currentObj};

        return this.httpClient.request('DELETE', `${this.path}/${currentObj.id}`, options).catch(this.errorHandler);
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }
}
