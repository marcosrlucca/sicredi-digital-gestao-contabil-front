import {MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-remove-draft-dialog',
    templateUrl: 'remove-draft-dialog.component.html',
    styleUrls: ['remove-draft-dialog.component.scss']
})
export class RemoveDraftDialogComponent {
    @Output() removeDraftEventEmitter = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<RemoveDraftDialogComponent>) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onYesClick() {
        this.dialogRef.close();
        this.removeDraftEventEmitter.emit();
    }

}
