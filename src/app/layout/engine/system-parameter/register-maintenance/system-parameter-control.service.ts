import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ISystemControl} from '../system.model';
import {isNullOrUndefined} from 'util';
import {environment} from '../../../../../environments/environment';

@Injectable()
export class SystemParameterControlService {

    private path = `${environment._api_engine_system_par}/system/control`;

    constructor(private httpClient: HttpClient) {
    }

    saveOrUpdate(currentObj: ISystemControl): Observable<any> {
        if (isNullOrUndefined(currentObj.registerResponsible)) {
            return Observable.throw('Usuário não pode estar vazio');
        }

        return this.httpClient.put(this.path, currentObj).catch(this.errorHandler);
    }

    remove(currentObj: ISystemControl): Observable<any> {
        if (isNullOrUndefined(currentObj) || isNullOrUndefined(currentObj.id)) {
            return Observable.throw('ID não pode estar vazio');
        }

        const options = {body: currentObj};

        return this.httpClient.request('DELETE', `${this.path}/${currentObj.id}`, options).catch(this.errorHandler);
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }

}
