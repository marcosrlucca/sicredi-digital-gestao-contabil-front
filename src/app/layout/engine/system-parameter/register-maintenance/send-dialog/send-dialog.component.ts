import {MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-send-dialog',
    templateUrl: 'send-dialog.component.html',
    styleUrls: ['send-dialog.component.scss']
})
export class SendDialogComponent {
    @Output() sendEventEmitter = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<SendDialogComponent>) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onYesClick() {
        this.dialogRef.close();
        this.sendEventEmitter.emit();
    }

}
