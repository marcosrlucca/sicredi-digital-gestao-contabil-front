import {Pageable, PageResponse, Sort} from '../../../shared/domain/page.model';

// tslint:disable-next-line: no-use-before-declare
export class SystemControlResponseImpl implements ISystemControlResponse {
    content: ISystemControl[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    pageable: Pageable;
    size: number;
    sort: Sort;
    totalElements: number;
    totalPages: boolean;
}

// tslint:disable-next-line: no-use-before-declare
export class SystemResponseImpl implements ISystemResponse {
    content: ISystem[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    pageable: Pageable;
    size: number;
    sort: Sort;
    totalElements: number;
    totalPages: boolean;
}

// tslint:disable-next-line: no-use-before-declare
export class SystemImpl implements ISystem {
    id: number;
    system: string;
    systemDescription: string;
    sapCode: string;
    registerResponsible: string;
    emailResponsible: string;
    validityStart: any;
    validityEnd: any;
    validityStartLocalePtBR: any;
    validityEndLocalePtBR: any;
    status: string;
    control: ISystemControl;
}

// tslint:disable-next-line: no-use-before-declare
export class SystemControlImpl implements ISystemControl {
    id: number;
    system: string;
    systemDescription: string;
    registerResponsible: string;
    emailResponsible: string;
    sapCode: string;
    validityStart: any;
    validityEnd: any;
    validityStartLocalePtBR: any;
    validityEndLocalePtBR: any;
    status: string;
    feedback: string;
    editingStatus: string;
    approvalResponsible: string;
}

export interface ISystemDescription {
    system: string;
    systemDescription: string;
}

export interface ISystemResponse extends PageResponse {
    content: ISystem[];
}

export interface ISystemControlResponse extends PageResponse {
    content: ISystemControl[];
}

export interface ISystem {
    id: number;
    system: string;
    systemDescription: string;
    sapCode: string;
    registerResponsible: string;
    emailResponsible: string;
    validityStart: any;
    validityEnd: any;
    validityStartLocalePtBR: any;
    validityEndLocalePtBR: any;
    status: string;
    control: ISystemControl;
}

export interface ISystemControl {
    id: number;
    system: string;
    systemDescription: string;
    registerResponsible: string;
    emailResponsible: string;
    sapCode: string;
    validityStart: any;
    validityEnd: any;
    validityStartLocalePtBR: any;
    validityEndLocalePtBR: any;
    status: string;
    feedback: string;
    editingStatus: string;
    approvalResponsible: string;
}
