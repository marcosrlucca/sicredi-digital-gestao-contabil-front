import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {isNullOrUndefined} from 'util';
import {ISystemControl, ISystemControlResponse} from '../system.model';

@Injectable()
export class ManagementService {

    private pathManagement = `${environment._api_engine_system_par}/system/management`;

    constructor(private httpClient: HttpClient) {
    }

    findAll(sortOrder: any, pageIndex: any, pageSize: any): Observable<any> {
        let params = new HttpParams();

        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;
        params = !isNullOrUndefined(pageIndex) ? params.set('page', pageIndex) : params;
        params = !isNullOrUndefined(pageSize) ? params.set('size', pageSize) : params;

        return this.httpClient.get<ISystemControlResponse>(this.pathManagement, {params}).catch(this.errorHandler);
    }

    approve(dto: ISystemControl) {
        return this.httpClient.put(`${this.pathManagement}/actions/approve`, dto);
    }

    disapprove(dto: ISystemControl) {
        return this.httpClient.put(`${this.pathManagement}/actions/disapprove`, dto).catch(this.errorHandler);
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }
}
