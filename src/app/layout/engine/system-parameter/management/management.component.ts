import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort, SortDirection} from '@angular/material';
import {ToastsManager} from 'ng2-toastr';
import {tap} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {merge} from 'rxjs/observable/merge';
import {HttpStatus} from '../../../../shared/domain/http-status';
import {HttpErrorResponse} from '@angular/common/http';
import {UserService} from '../../../../configuration/user.service';
import {DateUtils} from '../../../../shared/utils/date.utils';
import {ManagementDataSource} from './management.data-source';
import {AbstractEngineController} from '../../abstract-engine-controller';
import {ISystemControl} from '../system.model';
import {ManagementService} from './management.service';
import {ApproveDialogComponent} from './approve-dialog/approve-dialog.component';
import {DisapproveDialogComponent} from './disapprove-dialog/disapprove-dialog.component';
import {PermissionService} from '../../../../shared/services/permission.service';

const SORT_ACTIVE_DEFAULT = 'validityStart';
const SORT_DIRECTION_DEFAULT = 'asc';
const SORT_ORDER_DEFAULT = 'validityStart,asc';

const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 20;

@Component({
    selector: 'app-management',
    templateUrl: './management.component.html',
    styleUrls: ['./management.component.scss']
})
export class ManagementComponent extends AbstractEngineController implements OnInit, AfterViewInit {

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    displayedColumns = ['validityStart', 'validityEnd', 'system', 'systemDescription', 'sapCode', 'registerResponsible', 'emailResponsible'];
    dataSource: ManagementDataSource;
    highlightedRows = [];
    currentObj: ISystemControl;

    constructor(private dateUtils: DateUtils,
                private service: ManagementService,
                private userServive: UserService,
                private dialog: MatDialog,
                private toastr: ToastsManager,
                public permission: PermissionService) {
        super();
    }

    ngOnInit() {
        this.sort.active = SORT_ACTIVE_DEFAULT;
        this.sort.direction = <SortDirection>SORT_DIRECTION_DEFAULT;
        this.loadData();
    }

    ngAfterViewInit() {
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page).pipe(tap(() => this.loadData())).subscribe();
    }

    public loadData() {
        this.dataSource = new ManagementDataSource(this.dateUtils, this.service);

        const sortValue = !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
            `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT;
        const pageIndexValue = !isNullOrUndefined(this.paginator) && !isNullOrUndefined(this.paginator.pageIndex) ? this.paginator.pageIndex :
            PAGE_INDEX_DEFAULT;
        const pageSizeValue = !isNullOrUndefined(this.paginator) && !isNullOrUndefined(this.paginator.pageSize) ? this.paginator.pageSize :
            PAGE_SIZE_DEFAULT;
        this.dataSource.loadData(sortValue, pageIndexValue, pageSizeValue);
    }

    onSelectRow(row: ISystemControl) {
        const index = this.highlightedRows.findIndex((a: ISystemControl) => a.id === row.id);

        if (index === -1 && this.highlightedRows.length < 1) { // permite adicionar somente 1 elemento por vez
            this.highlightedRows.push(row);
        } else {
            this.highlightedRows.splice(index, 1);
            this.highlightedRows.push(row);
        }

        this.currentObj = row;
    }

    reset() {
        this.highlightedRows = [];
    }

    approve() {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1) {
            return;
        }

        const dialogRef = this.dialog.open(ApproveDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.approveEventEmitter.subscribe(() => {
            this.doApproveRequest();
        });
    }

    disapprove() {
        const dialogRef = this.dialog.open(DisapproveDialogComponent, {
            width: '100ex',
            data: {dto: this.currentObj}
        });

        dialogRef.componentInstance.doDisapprove.subscribe((result) => {
            this.doDisapproveRequest(result);
        });
    }

    private doApproveRequest() {
        this.setApprovalResponsible();

        this.service.approve(this.currentObj).subscribe(() => {
            this.loadData();
            this.reset();

            setTimeout(() => {
                this.toastr.info('Solicitação aprovada com sucesso');
            }, 500);

        }, (error: HttpErrorResponse) => {
            if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                this.toastr.error(error.error.msg, null, {toastLife: 3000});
            }
        });
    }

    private setApprovalResponsible() {
        this.currentObj.approvalResponsible = this.userServive.currentUser.username;
    }

    private doDisapproveRequest(result: string) {
        if (!isNullOrUndefined(result)) {
            this.setFeedBack(result);
        }

        this.service.disapprove(this.currentObj).subscribe(() => {
            this.loadData();
            this.reset();

            setTimeout(() => {
                this.toastr.info('Solicitação rejeitada com sucesso');
            }, 500);

        }, (error: HttpErrorResponse) => {
            if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                this.toastr.error(error.error.msg, null, {toastLife: 3000});
            }
        });
    }

    private setFeedBack(result) {
        this.currentObj.feedback = result;
    }
}
