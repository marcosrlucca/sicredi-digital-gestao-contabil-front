import {Component, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../../router.animations';
import {RegisterMaintenanceComponent} from './register-maintenance/register-maintenance.component';
import {TabComponent} from 'angular-tabs-component';
import {isNullOrUndefined} from 'util';
import {PermissionService} from '../../../shared/services/permission.service';
import {UserService} from '../../../configuration/user.service';
import {Authorization} from '../../../shared/domain/authorization.model';
import {ManagementComponent} from './management/management.component';

const TITLE_ADM_REQUEST = 'Administração';
const TITLE_REGISTER = 'Cadastro';

@Component({
    selector: 'app-system',
    templateUrl: './system.component.html',
    styleUrls: ['./system.component.scss'],
    animations: [routerTransition()]
})
export class SystemComponent implements OnInit {

    @ViewChild('appManagement')
    private appManagement: ManagementComponent;
    @ViewChild('appRegisterMaintenance')
    private appRegisterMaintenance: RegisterMaintenanceComponent;

    public admAuthorized = false;
    public aprovadorAuthorized = false;
    public consultaAuthorized = false;

    constructor(private permission: PermissionService,
                private userService: UserService) {
    }

    ngOnInit() {
        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            authorities.permission.forEach(a => {
                switch (a) {
                    case(this.permission.getDigContabilAdm()):
                        this.admAuthorized = true;
                        break;
                    case(this.permission.getDigContabilAprovador()):
                        this.aprovadorAuthorized = true;
                        break;
                    case(this.permission.getDigContabilConsulta()):
                        this.consultaAuthorized = true;
                        break;
                }
            });
        });
    }

    onTabChange(event: TabComponent) {
        switch (event.tabTitle) {
            case TITLE_ADM_REQUEST:
                if (!isNullOrUndefined(this.appManagement)) {
                    this.appManagement.loadData();
                }
                break;
            case TITLE_REGISTER:
                if (!isNullOrUndefined(this.appRegisterMaintenance)) {
                    this.appRegisterMaintenance.reloadData();
                }
                break;
        }
    }
}
