import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {Authorization} from '../../shared/domain/authorization.model';
import {UserService} from '../../configuration/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-engine',
    templateUrl: './engine.html',
    styleUrls: ['./engine.scss'],
    animations: [routerTransition()]
})
export class EngineComponent implements OnInit {

    constructor(private userService: UserService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit(): void {
        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });
    }

    goToSystem() {
        this.router.navigate(['sistema'], {relativeTo: this.route});
    }

    goToTypeValue() {
        this.router.navigate(['tipo-valor'], {relativeTo: this.route});
    }

    goToMassiveRegistration() {
        this.router.navigate(['cadastramento-massivo-inconsistente'], {relativeTo: this.route});
    }
}
