import {NgModule} from '@angular/core';
import {NgbAlertModule, NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TabModule} from 'angular-tabs-component';
import {SharedModule} from '../../../shared/modules/shared/shared.module';
import {PageHeaderModule, SharedPipesModule, StatModule} from '../../../shared';
import {MaterialSharedModule} from '../../../shared/modules/shared/MaterialSharedModule';
import {ApproveDialogComponent} from './management/approve-dialog/approve-dialog.component';
import {DisapproveDialogComponent} from './management/disapprove-dialog/disapprove-dialog.component';
import {ManagementService} from './management/management.service';
import {ManagementComponent} from './management/management.component';
import {RegisterMaintenanceComponent} from './register-maintenance/register-maintenance.component';
import {EditRequestDialogComponent} from './register-maintenance/edit-request-dialog/edit-request-dialog.component';
import {FinalizeDialogComponent} from './register-maintenance/finalize-dialog/finalize-dialog.component';
import {RemoveDraftDialogComponent} from './register-maintenance/remove-draft-dialog/remove-draft-dialog.component';
import {RemoveRequestDialogComponent} from './register-maintenance/remove-request-dialog/remove-request-dialog.component';
import {SendDialogComponent} from './register-maintenance/send-dialog/send-dialog.component';
import {SaveDialogComponent} from './register-maintenance/save-dialog/save-dialog.component';
import {ValueTypeRoutingModule} from './value-type-routing.module';
import {ValueTypeService} from './register-maintenance/value-type.service';
import {ControlService} from './register-maintenance/control.service';
import {ValueTypeComponent} from './value-type.component';

@NgModule({
    imports:
        [
            SharedModule,
            SharedPipesModule,
            MaterialSharedModule,
            TabModule,
            FormsModule,
            NgbCarouselModule.forRoot(),
            NgbAlertModule.forRoot(),
            ValueTypeRoutingModule,
            ReactiveFormsModule,
            PageHeaderModule,
            StatModule
        ],
    entryComponents: [
        ApproveDialogComponent,
        DisapproveDialogComponent,
        EditRequestDialogComponent,
        FinalizeDialogComponent,
        RemoveDraftDialogComponent,
        RemoveRequestDialogComponent,
        SaveDialogComponent,
        SendDialogComponent
    ],
    providers: [
        ValueTypeService,
        ControlService,
        ManagementService
    ],
    declarations:
        [
            ValueTypeComponent,
            ManagementComponent,
            RegisterMaintenanceComponent,
            ApproveDialogComponent,
            DisapproveDialogComponent,
            EditRequestDialogComponent,
            FinalizeDialogComponent,
            RemoveDraftDialogComponent,
            RemoveRequestDialogComponent,
            SaveDialogComponent,
            SendDialogComponent
        ]
})
export class ValueTypeModule {
}
