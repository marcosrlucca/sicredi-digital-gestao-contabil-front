import {MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-finalize-dialog',
    templateUrl: 'finalize-dialog.component.html',
    styleUrls: ['finalize-dialog.component.scss']
})
export class FinalizeDialogComponent {
    @Output() finalizeEventEmitter = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<FinalizeDialogComponent>) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onYesClick() {
        this.dialogRef.close();
        this.finalizeEventEmitter.emit();
    }

}
