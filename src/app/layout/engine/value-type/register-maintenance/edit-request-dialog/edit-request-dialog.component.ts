import {MAT_DIALOG_DATA, MatDatepickerInputEvent, MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {isNullOrUndefined} from 'util';
import {MyErrorStateMatcher} from '../../../../../shared/services/my-error-state-matcher';
import {DateUtils} from '../../../../../shared/utils/date.utils';
import * as constants from '../../../constants';
import {IControl} from '../../value-type.model';

@Component({
    selector: 'app-edit-request-dialog',
    templateUrl: 'edit-request-dialog.component.html',
    styleUrls: ['edit-request-dialog.component.scss']
})
export class EditRequestDialogComponent implements OnInit {

    @Output() doSave = new EventEmitter();
    @Output() doCancel = new EventEmitter();
    startDate;
    endDate;
    matcher;
    startCalendarFormControl: FormControl;
    endCalendarFormControl: FormControl;
    valueFormControl: FormControl;
    valueDescriptionFormControl: FormControl;
    control: IControl;

    constructor(private dateUtils: DateUtils,
                public dialogRef: MatDialogRef<EditRequestDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: IControl) {
    }

    ngOnInit(): void {
        this.startCalendarFormControl = new FormControl('', [Validators.required]);
        this.endCalendarFormControl = new FormControl('', []);
        this.valueFormControl = new FormControl('', [Validators.required]);
        this.valueDescriptionFormControl = new FormControl('', [Validators.required, Validators.maxLength(100)]);

        this.control = this.data['parameter'];
        this.matcher = new MyErrorStateMatcher();

        if (!isNullOrUndefined(this.data['parameter'].validityStart)) {
            this.startDate = this.dateUtils.parseStringDateToMaterial(this.data['parameter'].validityStart);
        }
        if (!isNullOrUndefined(this.data['parameter'].validityEnd)) {
            this.endDate = this.dateUtils.parseStringDateToMaterial(this.data['parameter'].validityEnd);
        }
        if (isNullOrUndefined(this.data['parameter'].validityStart)) {
            this.startCalendarFormControl = new FormControl('', Validators.required);
            this.startDate = '';
        }
        if (isNullOrUndefined(this.data['parameter'].validityEnd)) {
            this.endCalendarFormControl = new FormControl('');
            this.endDate = '';
        }
    }

    onNoClick(): void {
        this.doCancel.emit();
        this.dialogRef.close();
    }

    onReset() {
        this.startCalendarFormControl.reset();
        this.endCalendarFormControl.reset();
        this.valueFormControl.reset();
        this.valueDescriptionFormControl.reset();
    }

    onSend() {
        if (!this.validate()) {
            return;
        }

        this.control.validityStart = this.startDate;
        this.control.validityEnd = this.endDate;

        this.control.status = constants.SENT_VALUE;

        this.doSave.emit(this.control);
        this.dialogRef.close();
    }

    onSave() {
        if (!this.validate()) {
            return;
        }

        this.control.validityStart = this.startDate;
        this.control.validityEnd = this.endDate;

        this.control.status = constants.DRAFT_VALUE;

        this.doSave.emit(this.control);
        this.dialogRef.close();
    }

    private validate(): boolean {
        if (!this.startCalendarFormControl.valid || !this.endCalendarFormControl.valid || !this.valueFormControl.valid ||
            !this.valueDescriptionFormControl.valid) {
            return false;
        }

        if (!isNullOrUndefined(this.endDate) && this.endDate !== '' && this.startDate > this.endDate) {
            // noinspection TypeScriptValidateTypes
            this.endCalendarFormControl.setErrors(Validators.compose);
            return false;
        } else {
            this.endCalendarFormControl.setErrors(null);
        }

        return true;
    }

    startDateChange(event: MatDatepickerInputEvent<Date>) {
        if (!isNullOrUndefined(event.value)) {
            this.startDate.value = event.value;
            this.data['parameter'].validityStart = this.getDateAsString(event);
        }
    }

    endDateChange(event: MatDatepickerInputEvent<Date>) {
        if (!isNullOrUndefined(event.value)) {
            this.endDate.value = event.value;
            this.data['parameter'].validityEnd = this.getDateAsString(event);
        } else {
            this.data['parameter'].validityEnd = null;
        }
    }

    getDateAsString(event: MatDatepickerInputEvent<Date>) {
        const date = event.value.getDate() < 10 ? `0${event.value.getDate()}` : event.value.getDate();
        const month = event.value.getMonth() < 10 ? `0${event.value.getMonth() + 1}` : event.value.getMonth() + 1;
        const year = event.value.getFullYear();

        return `${date}/${month}/${year}`;
    }
}
