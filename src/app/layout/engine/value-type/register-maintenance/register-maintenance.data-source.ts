import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {isNullOrUndefined} from 'util';
import {DateUtils} from '../../../../shared/utils/date.utils';
import {IValueType, IValueTypeResponse, ValueTypeResponseImpl} from '../value-type.model';
import {ValueTypeService} from './value-type.service';

export class RegisterMaintenanceDataSource implements DataSource<IValueType> {

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public data = new BehaviorSubject<IValueType[]>([]);
    public loading$ = this.loadingSubjects.asObservable();

    public response: IValueTypeResponse;

    public noDataFound = false;

    constructor(private dateUtils: DateUtils,
                private service: ValueTypeService) {
        this.response = new ValueTypeResponseImpl();
    }

    connect(collectionViewer: CollectionViewer): Observable<IValueType[]> {
        return this.data.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.data.complete();
        this.loadingSubjects.complete();
    }

    loadData(startDate, endDate, statusValue, sortOrder, pageIndex, pageSize) {
        this.loadingSubjects.next(true);

        this.service.findAll(startDate, endDate, statusValue, sortOrder, pageIndex, pageSize)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false))
            )
            .subscribe((response: IValueTypeResponse) => {
                if (response !== null && response.totalElements > 0) {
                    this.data.next(response.content);
                    this.response = response;
                    this.noDataFound = false;

                    this.parseDateToLocalePtBR();
                } else {
                    this.noDataFound = true;
                }
            });
    }

    private parseDateToLocalePtBR() {
        this.data.getValue().forEach(a => {
            if (!isNullOrUndefined(a.validityStart)) {
                a.validityStartLocalePtBR = this.dateUtils.parseStringDateToLocalePtBR(a.validityStart);
            }
            if (!isNullOrUndefined(a.validityEnd)) {
                a.validityEndLocalePtBR = this.dateUtils.parseStringDateToLocalePtBR(a.validityEnd);
            }
            if (!isNullOrUndefined(a.control)) {
                if (!isNullOrUndefined(a.control.validityStart)) {
                    a.control.validityStartLocalePtBR = this.dateUtils.parseStringDateToLocalePtBR(a.control.validityStart);
                }
                if (!isNullOrUndefined(a.control.validityEnd)) {
                    a.control.validityEndLocalePtBR = this.dateUtils.parseStringDateToLocalePtBR(a.control.validityEnd);
                }
            }
        });
    }

    isDataSourceEmpty() {
        return isNullOrUndefined(this.data) || isNullOrUndefined(this.data.getValue()) || this.data.getValue().length === 0;
    }
}
