import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';
import {IValueType, IValueTypeResponse} from '../value-type.model';

@Injectable()
export class ValueTypeService {

    private path = `${environment._api_value_type}/value-type`;

    constructor(private httpClient: HttpClient) {
    }

    findAll(startDate, endDate, statusValue, sortOrder, pageIndex, pageSize): Observable<IValueTypeResponse> {
        let params = new HttpParams();

        params = !isNullOrUndefined(statusValue) ? params.set('status', statusValue) : params;
        params = !isNullOrUndefined(startDate) ? params.set('startDate', startDate) : params;
        params = !isNullOrUndefined(endDate) ? params.set('endDate', endDate) : params;
        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;
        params = !isNullOrUndefined(pageIndex) ? params.set('page', pageIndex.toString()) : params;
        params = !isNullOrUndefined(pageSize) ? params.set('size', pageSize.toString()) : params;

        return this.httpClient.get<IValueTypeResponse>(this.path, {params}).catch(this.errorHandler);
    }

    public getStatus() {
        return Observable.of(
            {value: 'actual', viewValue: 'Ativo'},
            {value: 'sent', viewValue: 'Enviado'},
            {value: 'finalized', viewValue: 'Finalizado'},
            {value: 'draft', viewValue: 'Rascunho'},
            {value: 'disapproved', viewValue: 'Reprovado'}
        );
    }

    saveOrUpdate(currentObj: IValueType): Observable<any> {
        if (isNullOrUndefined(currentObj.id)) {
            return this.httpClient.post(this.path, currentObj).catch(this.errorHandler);
        }

        return this.httpClient.put(this.path, currentObj).catch(this.errorHandler);
    }

    remove(currentObj: IValueType): Observable<any> {
        if (isNullOrUndefined(currentObj) || isNullOrUndefined(currentObj.id)) {
            return Observable.throw('ID não pode estar vazio');
        }
        if (isNullOrUndefined(currentObj.registerResponsible)) {
            return Observable.throw('Usuário não pode estar vazio');
        }

        const options = {body: currentObj};

        return this.httpClient.request('DELETE', `${this.path}/${currentObj.id}`, options).catch(this.errorHandler);
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }
}
