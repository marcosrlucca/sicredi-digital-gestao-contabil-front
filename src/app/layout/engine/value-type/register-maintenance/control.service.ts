import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {isNullOrUndefined} from 'util';
import {environment} from '../../../../../environments/environment';
import {IControl} from '../value-type.model';

@Injectable()
export class ControlService {

    private path = `${environment._api_value_type}/value-type/control`;

    constructor(private httpClient: HttpClient) {
    }

    saveOrUpdate(currentObj: IControl): Observable<any> {
        if (isNullOrUndefined(currentObj.registerResponsible)) {
            return Observable.throw('Usuário não pode estar vazio');
        }

        return this.httpClient.put(this.path, currentObj).catch(this.errorHandler);
    }

    remove(currentObj: IControl): Observable<any> {
        if (isNullOrUndefined(currentObj) || isNullOrUndefined(currentObj.id)) {
            return Observable.throw('ID não pode estar vazio');
        }

        const options = {body: currentObj};

        return this.httpClient.request('DELETE', `${this.path}/${currentObj.id}`, options).catch(this.errorHandler);
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }

}
