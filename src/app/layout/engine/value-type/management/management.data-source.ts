import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {isNullOrUndefined} from 'util';
import {catchError, finalize} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {DateUtils} from '../../../../shared/utils/date.utils';
import {ManagementService} from './management.service';
import {ControlResponseImpl, IControl, IControlResponse} from '../value-type.model';

export class ManagementDataSource implements DataSource<IControl> {

    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public data = new BehaviorSubject<IControl[]>([]);
    public loading$ = this.loadingSubjects.asObservable();

    public response: IControlResponse;

    public noDataFound = false;

    constructor(private dateUtils: DateUtils, private service: ManagementService) {
        this.response = new ControlResponseImpl();
    }

    connect(collectionViewer: CollectionViewer): Observable<IControl[]> {
        return this.data.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.data.complete();
        this.loadingSubjects.complete();
    }

    loadData(sortOrder, pageIndex, pageSize) {
        this.loadingSubjects.next(true);

        this.service.findAll(sortOrder, pageIndex, pageSize)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false))
            )
            .subscribe((response: IControlResponse) => {
                if (response !== null && response.totalElements > 0) {
                    this.data.next(response.content);
                    this.response = response;
                    this.noDataFound = false;

                    this.parseDateToLocalePtBR();
                } else {
                    this.noDataFound = true;
                }
            });
    }

    private parseDateToLocalePtBR() {
        this.data.getValue().forEach(a => {
            if (!isNullOrUndefined(a.validityStart)) {
                a.validityStartLocalePtBR = this.dateUtils.parseStringDateToLocalePtBR(a.validityStart);
            }
            if (!isNullOrUndefined(a.validityEnd)) {
                a.validityEndLocalePtBR = this.dateUtils.parseStringDateToLocalePtBR(a.validityEnd);
            }
        });
    }

    isDataSourceEmpty() {
        return isNullOrUndefined(this.data) || isNullOrUndefined(this.data.getValue()) || this.data.getValue().length === 0;
    }
}
