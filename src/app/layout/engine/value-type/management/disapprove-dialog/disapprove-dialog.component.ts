import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Inject, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {IControl} from '../../value-type.model';

@Component({
    selector: 'app-disapprove-dialog',
    templateUrl: 'disapprove-dialog.component.html',
    styleUrls: ['disapprove-dialog.component.scss']
})
export class DisapproveDialogComponent implements OnInit {

    @Output() doDisapprove = new EventEmitter();
    @Output() doCancel = new EventEmitter();
    feedbackFormControl: FormControl;

    constructor(public dialogRef: MatDialogRef<DisapproveDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: IControl) {
    }

    ngOnInit(): void {
        this.feedbackFormControl = new FormControl('', [Validators.maxLength(200)]);
    }

    onNoClick(): void {
        this.doCancel.emit();
        this.dialogRef.close();
    }

    onReset() {
        this.feedbackFormControl.reset();
    }

    onSave() {
        this.doDisapprove.emit(this.feedbackFormControl.value);
        this.dialogRef.close();
    }
}
