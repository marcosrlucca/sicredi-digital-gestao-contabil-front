import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {isNullOrUndefined} from 'util';
import {IControl, IControlResponse} from '../value-type.model';

@Injectable()
export class ManagementService {

    private pathManagement = `${environment._api_value_type}/value-type/management`;

    constructor(private httpClient: HttpClient) {
    }

    findAll(sortOrder: any, pageIndex: any, pageSize: any): Observable<any> {
        let params = new HttpParams();

        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;
        params = !isNullOrUndefined(pageIndex) ? params.set('page', pageIndex) : params;
        params = !isNullOrUndefined(pageSize) ? params.set('size', pageSize) : params;

        return this.httpClient.get<IControlResponse>(this.pathManagement, {params}).catch(this.errorHandler);
    }

    approve(dto: IControl) {
        return this.httpClient.put(`${this.pathManagement}/actions/approve`, dto);
    }

    disapprove(dto: IControl) {
        return this.httpClient.put(`${this.pathManagement}/actions/disapprove`, dto).catch(this.errorHandler);
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }
}
