import {MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-approve-dialog',
    templateUrl: 'approve-dialog.component.html',
    styleUrls: ['approve-dialog.component.scss']
})
export class ApproveDialogComponent {
    @Output() approveEventEmitter = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<ApproveDialogComponent>) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onYesClick() {
        this.dialogRef.close();
        this.approveEventEmitter.emit();
    }

}
