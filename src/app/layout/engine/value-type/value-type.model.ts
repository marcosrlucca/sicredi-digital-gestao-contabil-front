import {Pageable, PageResponse, Sort} from '../../../shared/domain/page.model';

// tslint:disable-next-line: no-use-before-declare
export class ControlResponseImpl implements IControlResponse {
    content: IControl[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    pageable: Pageable;
    size: number;
    sort: Sort;
    totalElements: number;
    totalPages: boolean;
}

// tslint:disable-next-line: no-use-before-declare
export class ValueTypeResponseImpl implements IValueTypeResponse {
    content: IValueType[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    pageable: Pageable;
    size: number;
    sort: Sort;
    totalElements: number;
    totalPages: boolean;
}

// tslint:disable-next-line: no-use-before-declare
export class ValueTypeImpl implements IValueType {
    id: number;
    value: string;
    valueDescription: string;
    registerResponsible: string;
    validityStart: any;
    validityEnd: any;
    validityStartLocalePtBR: any;
    validityEndLocalePtBR: any;
    status: string;
    control: IControl;
}

// tslint:disable-next-line: no-use-before-declare
export class ControlImpl implements IControl {
    id: number;
    value: string;
    valueDescription: string;
    registerResponsible: string;
    validityStart: any;
    validityEnd: any;
    validityStartLocalePtBR: any;
    validityEndLocalePtBR: any;
    status: string;
    feedback: string;
    approvalResponsible: string;
}

export interface IValueTypeResponse extends PageResponse {
    content: IValueType[];
}

export interface IControlResponse extends PageResponse {
    content: IControl[];
}

export interface IValueType {
    id: number;
    value: string;
    valueDescription: string;
    registerResponsible: string;
    validityStart: any;
    validityEnd: any;
    validityStartLocalePtBR: any;
    validityEndLocalePtBR: any;
    status: string;
    control: IControl;
}

export interface IControl {
    id: number;
    value: string;
    valueDescription: string;
    registerResponsible: string;
    validityStart: any;
    validityEnd: any;
    validityStartLocalePtBR: any;
    validityEndLocalePtBR: any;
    status: string;
    feedback: string;
    approvalResponsible: string;
}
