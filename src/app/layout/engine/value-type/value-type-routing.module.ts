import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ValueTypeComponent} from './value-type.component';

const routes: Routes = [
    {
        path: '',
        component: ValueTypeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ValueTypeRoutingModule {
}
