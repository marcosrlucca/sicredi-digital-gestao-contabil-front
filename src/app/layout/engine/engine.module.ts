import {NgModule} from '@angular/core';

import {NgbAlertModule, NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/modules/shared/shared.module';
import {StatModule} from '../../shared/modules/stat/stat.module';
import {PageHeaderModule} from '../../shared/modules/page-header/page-header.module';
import {EngineComponent} from './engine.component';
import {EngineRoutingModule} from './engine-routing.module';
import {MassiveRegistrationComponent} from './massive-registration/massive-registration.component';
import {MassiveRegistrationService} from './massive-registration/massive-registration.service';
import {MaterialSharedModule} from '../../shared/modules/shared/MaterialSharedModule';
import {SharedPipesModule} from '../../shared';

@NgModule({
    imports:
        [
            SharedModule,
            SharedPipesModule,
            MaterialSharedModule,
            FormsModule,
            NgbCarouselModule.forRoot(),
            NgbAlertModule.forRoot(),
            EngineRoutingModule,
            ReactiveFormsModule,
            PageHeaderModule,
            StatModule
        ],
    providers: [
        MassiveRegistrationService
    ],
    declarations:
        [
            EngineComponent,
            MassiveRegistrationComponent
        ]
})
export class EngineModule {
}
