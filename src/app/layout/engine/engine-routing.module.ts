import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EngineComponent} from './engine.component';
import {MassiveRegistrationComponent} from './massive-registration/massive-registration.component';

const routes: Routes = [
    {
        path: '', component: EngineComponent
    },
    {
        path: 'cadastramento-massivo-inconsistente', component: MassiveRegistrationComponent
    },
    {
        path: 'tipo-valor',
        loadChildren: './value-type/value-type.module#ValueTypeModule'
    },
    {
        path: 'sistema',
        loadChildren: './system-parameter/system.module#SystemModule'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EngineRoutingModule {
}
