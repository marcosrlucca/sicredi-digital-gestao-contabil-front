import * as constants from './constants';

export abstract class AbstractEngineController {

    public getDraft() {
        return constants.DRAFT_NAME;
    }

    public getSent() {
        return constants.SENT_NAME;
    }

    public getDisapproved() {
        return constants.DISAPPROVED_NAME;
    }

    public getActual() {
        return constants.ACTUAL_NAME;
    }

    public getFinalized() {
        return constants.FINALIZED_NAME;
    }

    public getDraftDescription() {
        return constants.DRAFT_VALUE;
    }

    public getSentDescription() {
        return constants.SENT_VALUE;
    }

    public getDisapprovedDescription() {
        return constants.DISAPPROVED_VALUE;
    }

    public getActualDescription() {
        return constants.ACTUAL_VALUE;
    }

    public getFinalizedDescription() {
        return constants.FINALIZED_VALUE;
    }

    public getWaitingDeleteApproval(): string {
        return constants.WAITING_DELETE_APPROVAL;
    }

    public getRejectedRemoval(): string {
        return constants.REJECTED_REMOVAL;
    }

    public getEditing(): string {
        return constants.EDITING;
    }

    public getNotEditing(): string {
        return constants.NOT_EDITING;
    }
}
