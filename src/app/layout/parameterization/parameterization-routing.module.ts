import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ParameterizationComponent} from './parameterization.component';
import {TypeMovementComponent} from './type-movement/type-movement.component';
import {CenterComponent} from './center/center.component';
import {BalanceIntegrateComponent} from './balance-integrate/balance-integrate.component';

const routes: Routes = [
    {
        path: '', component: ParameterizationComponent
    },
    {
        path: 'tipo-movimento', component: TypeMovementComponent
    },
    {
        path: 'centro', component: CenterComponent
    },
    {
        path: 'integracao-saldo', component: BalanceIntegrateComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ParameterizationRoutingModule {
}
