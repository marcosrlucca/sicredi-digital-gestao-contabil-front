import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable} from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { BalanceIntegrate } from '../parameterization.model';
import { BalanceIntegrateService } from './balance-integrate.service';
import { Page } from '../../../shared/domain/page.model';

export class BalanceIntegrateDataSource implements DataSource<BalanceIntegrate> {

    page = new Page();
    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public data = new BehaviorSubject<BalanceIntegrate[]>([]);
    public loading$ = this.loadingSubjects.asObservable();

    public noDataFound = false;


    constructor(private service: BalanceIntegrateService) {
        this.page.pageSizeOptions = [50, 100, 200];
        this.page.numberOfElementsPerPage = 50;
    }

    connect(collectionViewer: CollectionViewer): Observable<BalanceIntegrate[]> {
        return this.data.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.data.complete();
        this.loadingSubjects.complete();
    }

    loadData(sortOrder, parameter: BalanceIntegrate, pageIndex, pageSize) {
        this.loadingSubjects.next(true);

        this.service.findPaging(sortOrder, parameter, pageIndex, pageSize)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false))
            )
            .subscribe(response => {
                if (response !== null) {

                    const responseContent = response['content'];
                    responseContent.map(center => {
                        if (center.validityStart != null) {
                            center.validityStart = new Date(center.validityStart[0], center.validityStart[1] - 1, center.validityStart[2]);
                        }

                        if (center.validityEnd != null) {
                            center.validityEnd = new Date(center.validityEnd[0], center.validityEnd[1] - 1, center.validityEnd[2]);
                        }
                    });

                    this.data.next(responseContent);
                    this.page = response;

                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }

}
