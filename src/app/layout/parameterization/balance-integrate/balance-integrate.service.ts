import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { isNullOrUndefined } from 'util';
import { BalanceIntegrate } from '../parameterization.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BalanceIntegrateService {

    private _api_url = `${environment._api_accounting_management_parameterization_url}/balance-integrate`;

    constructor(private httpClient: HttpClient) {
    }

    findAll(sortOrder, parameter: BalanceIntegrate) {
        let params = new HttpParams();
        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;

        if (!isNullOrUndefined(parameter)) {
            params = !isNullOrUndefined(parameter.systemId) ? params.set('systemId', parameter.systemId.toString()) : params;
            params = !isNullOrUndefined(parameter.accountHeading) ? params.set('accountheading', parameter.accountHeading) : params;
            params = !isNullOrUndefined(parameter.startDate) ? params.set('startdate', parameter.startDate) : params;
            params = !isNullOrUndefined(parameter.endDate) ? params.set('enddate', parameter.endDate) : params;
        }

        return this.httpClient.get(this._api_url, {params});
    }

    findPaging(sortOrder, parameter: BalanceIntegrate, pageIndex, pageSize): Observable<any> {
        let params = new HttpParams();
        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;
        params = !isNullOrUndefined(parameter) ? params.set('centerDTO', JSON.stringify(parameter)) : params;
        params = params.set('page', pageIndex.toString());
        params = params.set('size', pageSize.toString());

        return this.httpClient.get(this._api_url, {params});
    }

    remove(balanceIntegrate: BalanceIntegrate) {
        return this.httpClient.post(`${this._api_url}/${balanceIntegrate.id}`, balanceIntegrate);
    }

    saveOrUpdate(currentParameter: BalanceIntegrate) {
        return this.httpClient.post(this._api_url, currentParameter);
    }
}
