import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../../router.animations';
import {MatDatepickerInputEvent, MatDialog, MatSort, SortDirection, MatPaginator} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {ToastsManager} from 'ng2-toastr';
import {tap} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {DatePipe, DOCUMENT} from '@angular/common';
import {RemoveDialogComponent} from '../type-movement/remove-dialog/remove-dialog.component';
import {BalanceIntegrateDataSource} from './balance-integrate.data-source';
import {BalanceIntegrateService} from './balance-integrate.service';
import {BalanceIntegrate, ResponsibleParam} from '../parameterization.model';
import {HttpStatus} from '../../../shared/domain/http-status';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {UserService} from '../../../configuration/user.service';
import {System} from '../../management-error/allot-evaluation/allot-evaluation.model';
import {AllotEvaluationService} from '../../management-error/allot-evaluation/allot-evaluation.service';
import {Subscription} from 'rxjs/Subscription';
import {merge} from 'rxjs/observable/merge';

const SORT_ACTIVE_DEFAULT = 'startDate';
const SORT_DIRECTION_DEFAULT = 'desc';
const SORT_ORDER_DEFAULT = 'startDate,desc';
const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 50;

@Component({
    selector: 'app-balance-integrate',
    templateUrl: './balance-integrate.component.html',
    styleUrls: ['./balance-integrate.component.scss'],
    animations: [routerTransition()]
})
export class BalanceIntegrateComponent implements OnInit, AfterViewInit {
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    sizePerPage = 50;
    dataSource: BalanceIntegrateDataSource;
    displayedColumns = ['systemName', 'accountHeading', 'startDate', 'endDate', 'responsibleRegister', 'responsibleValidity'];

    highlightedRows = [];
    form: FormGroup;
    currentParameterSearch: BalanceIntegrate;
    currentParameter: BalanceIntegrate;

    startCalendar;
    endCalendar;
    startDate;
    endDate;

    selectedSystem: number;
    systems: Array<System>;
    system;
    systemSubscription: Subscription;

    labelParameter = 'Cadastrar';
    canFinishValidity = false;

    toggleRegisterChecked = false;
    position = 'after';

    constructor(private service: BalanceIntegrateService,
                private formBuilder: FormBuilder,
                public dialog: MatDialog,
                private userService: UserService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                private toastr: ToastsManager,
                private managementErrorService: AllotEvaluationService,
                @Inject(DOCUMENT) document) {
    }

    ngOnInit() {

        this.translatePaginatorActionButtons();

        this.dataSource = new BalanceIntegrateDataSource(this.service);

        this.form = this.formBuilder.group({
            accountHeading: new FormControl('', [Validators.required]),
            system: new FormControl('', [Validators.required])
        });

        this.startCalendar = new FormControl('', Validators.required);
        this.endCalendar = new FormControl('');

        this.currentParameter = new BalanceIntegrate();

        this.system = new FormControl('', []);

        this.sort.active = SORT_ACTIVE_DEFAULT;
        this.sort.direction = <SortDirection>SORT_DIRECTION_DEFAULT;

        this.loadSystem();
    }

    translatePaginatorActionButtons() {
        this.paginator._intl.firstPageLabel = 'Primeira página';
        this.paginator._intl.itemsPerPageLabel = 'Itens por página';
        this.paginator._intl.previousPageLabel = 'Página anterior';
        this.paginator._intl.nextPageLabel = 'Próxima página';
        this.paginator._intl.lastPageLabel = 'Última página';
    }

    reset(formDirective: FormGroupDirective) {
        this.form = this.formBuilder.group({
            accountHeading: new FormControl('', [Validators.required]),
            system: new FormControl('', [Validators.required])
        });

        this.startCalendar = new FormControl('', Validators.required);
        this.endCalendar = new FormControl('');

        this.startDate = '';
        this.endDate = '';

        this.selectedSystem = null;

        this.highlightedRows = [];

        this.labelParameter = 'Cadastrar';
        this.canFinishValidity = false;

        if (!isNullOrUndefined(formDirective)) {
            formDirective.resetForm();
        }


        if (!isNullOrUndefined(this.currentParameter.id)) {
            this.dataSource.loadData(
                !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` :
                SORT_ORDER_DEFAULT, null, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT);
            this.paginator.firstPage();
        }

        this.currentParameter = new BalanceIntegrate();
    }

    ngAfterViewInit() {
        this.sort.sortChange.pipe(
            tap(() => this.loadData())
        ).subscribe();

        merge(this.paginator.page).pipe(tap(() => this.loadData())).subscribe();
    }

    private loadSystem() {
        this.systemSubscription = this.managementErrorService.getSystems().subscribe(systems => this.systems = systems);
    }

    private loadData() {
        this.dataSource = new BalanceIntegrateDataSource(this.service);

        this.currentParameter.startDate = this.getDateOfMonth(this.startDate);
        this.currentParameter.endDate = this.getDateOfMonth(this.endDate);

        let currentParameterLoad = this.currentParameter;
        if (this.toggleRegisterChecked) {
            /*Quando não está marcado como busca, usamos o currentParameterSearch da busca para pesquisar.*/
            currentParameterLoad = this.currentParameterSearch;
        }

        this.dataSource.loadData(
            !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
            `${this.sort.active},${this.sort.direction}` :
            SORT_ORDER_DEFAULT, currentParameterLoad, this.paginator.pageIndex, this.paginator.pageSize);
    }

    changeToggle(event: any) {
        if (this.toggleRegisterChecked) {
            this.toggleRegisterChecked = false;
        } else {
            this.toggleRegisterChecked = true;
        }

        if (!this.toggleRegisterChecked) {
            this.reset(null);
            this.startCalendar = new FormControl('', []);
            this.form = this.formBuilder.group({
                accountHeading: new FormControl('', []),
                system: new FormControl('', [])
            });
        }
    }

    onSelectRow(row: BalanceIntegrate) {
        const index = this.highlightedRows.findIndex((a: BalanceIntegrate) => a.id === row.id);

        if (index === -1 && this.highlightedRows.length < 1) { // permite adicionar somente 1 elemento por vez
            this.highlightedRows.push(row);
        } else {
            this.highlightedRows.splice(index, 1);
            this.highlightedRows.push(row);
        }

        this.currentParameter = row;

        if (!isNullOrUndefined(row.startDate)) {
            this.startDate = this.getDate(row.startDate);
        }
        if (!isNullOrUndefined(row.endDate)) {
            this.endDate = this.getDate(row.endDate);
            this.canFinishValidity = false;
        } else {
            this.canFinishValidity = true;
        }
        if (isNullOrUndefined(row.startDate)) {
            this.startCalendar = new FormControl('', Validators.required);
            this.startDate = '';
        }
        if (isNullOrUndefined(row.endDate)) {
            this.endCalendar = new FormControl('');
            this.endDate = '';
        }

        this.labelParameter = 'Editar';
        this.toggleRegisterChecked = true;
    }

    getDateAsString(event: MatDatepickerInputEvent<Date>) {
        const date = event.value.getDate() < 10 ? `0${event.value.getDate()}` : event.value.getDate();
        const month = event.value.getMonth() < 10 ? `0${event.value.getMonth() + 1}` : event.value.getMonth() + 1;
        const year = event.value.getFullYear();

        return `${date}/${month}/${year}`;
    }

    startDateChange(event: MatDatepickerInputEvent<Date>) {
        if (!isNullOrUndefined(event.value)) {
            this.currentParameter.startDateToView = this.getDateAsString(event);
            this.currentParameter.startDate = this.currentParameter.startDateToView;
            this.startDate.value = event.value;
        }
    }

    endDateChange(event: MatDatepickerInputEvent<Date>) {
        if (!isNullOrUndefined(event.value)) {
            this.currentParameter.endDateToView = this.getDateAsString(event);
            this.currentParameter.endDate = this.currentParameter.endDateToView;
            this.endDate.value = event.value;
        }
    }

    getDate(date) {
        const datePipe = new DatePipe('en-US');
        const dateReturn = datePipe.transform(date, 'dd/MM/yyyy');
        return new Date(date);
    }

    search(formDirective: FormGroupDirective) {
        this.currentParameter.startDate = this.getDateOfMonth(this.startDate);
        this.currentParameter.endDate = this.getDateOfMonth(this.endDate);

        this.dataSource = new BalanceIntegrateDataSource(this.service);

        this.dataSource.loadData(SORT_ORDER_DEFAULT, this.currentParameter, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT);
        this.reset(formDirective);
    }

    getDateOfMonth(dateToTransform) {
        if (!isNullOrUndefined(dateToTransform) && dateToTransform !== '') {
            let dayStartDate = dateToTransform.getDate();
            let monthStartDate = dateToTransform.getMonth() + 1;
            const yearStartDate = dateToTransform.getFullYear();

            if (dayStartDate < 10) {
                dayStartDate = '0' + dayStartDate;
            }
            if (monthStartDate < 10) {
                monthStartDate = '0' + monthStartDate;
            }

            return `${yearStartDate}-${monthStartDate}-${dayStartDate}`;
        }
        return null;
    }

    save(formDirective: FormGroupDirective) {
        const formIsValid: boolean = this.validate();

        if (!formIsValid) {
            return;
        }

        this.currentParameter.startDate = this.startDate;
        this.currentParameter.endDate = this.endDate;
        let responsibleParam = this.currentParameter.responsibleParam;
        responsibleParam = !responsibleParam ? new ResponsibleParam() : responsibleParam;

        if ( !this.currentParameter.id ) {
            responsibleParam.responsibleRegister = this.userService.currentUser.username;
        }

        if ( this.currentParameter.endDate ) {
            responsibleParam.responsibleValidity = this.userService.currentUser.username;
        }

        this.currentParameter.responsibleParam = responsibleParam;

        this.service.saveOrUpdate(this.currentParameter).subscribe(
            () => {
                this.dataSource.loadData(
                    !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                    `${this.sort.active},${this.sort.direction}` :
                    SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);

                this.reset(formDirective);

                setTimeout(() => {
                    this.toastr.info('Parâmetro salvo com sucesso', null, {toastLife: 3000});
                }, 500);
            }, (response) => {
                if (response.status === HttpStatus.NOT_ACCEPTABLE) {
                    this.toastr.error('Parâmetro não encontrado',
                        null, {toastLife: 3000, showCloseButton: true});
                } else if (response.status === HttpStatus.CONFLICT) {
                    if ( response.error === 'ERROR-001' ) {
                        this.toastr.error('Parâmetro já existente cadastrado nesse sistema.',
                            null, {toastLife: 10000, showCloseButton: true});
                    } else if (response.error === 'ERROR-002') {
                        this.toastr.error('Parâmetro com essa conta contábil já está cadastrado para outro sistema.',
                            null, {toastLife: 100000, showCloseButton: true});
                    } else {
                        /*Para manter compatibilidade*/
                        this.toastr.error('Parâmetro já existente.',
                            null, {toastLife: 3000, showCloseButton: true});
                    }
                }

                this.dataSource.loadData(!isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` :
                SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);
            }
        );
    }

    onSubmit(formData: any, formDirective: FormGroupDirective) {
        if (this.toggleRegisterChecked) {
            this.save(formDirective);
        } else {
            this.search(formDirective);
        }
    }

    private validate() {
        let isValid = true;

        const system: number = this.currentParameter.systemId;
        if (isNullOrUndefined(system)) {
            this.toastr.warning('O campo Sistema deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }

        const accountHeading: string = this.currentParameter.accountHeading;
        if (isNullOrUndefined(accountHeading) || accountHeading.trim() === '' || accountHeading.length !== 10) {
            this.toastr.warning('O campo Rúbrica Contábil deve ser preenchido com 10 dígitos', null, {toastLife: 3000});
            isValid = false;
        }
        if (isNullOrUndefined(this.currentParameter.startDate) || this.currentParameter.startDate === '') {
            this.toastr.warning('O campo Vigência Início deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        } else if (isNullOrUndefined(this.startDate) || this.startDate === '') {
            this.toastr.warning('O campo Vigência Início deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }
        if (!isNullOrUndefined(this.startDate) && !isNullOrUndefined(this.endDate) && this.endDate !== '' && this.startDate >= this.endDate) {
            this.toastr.warning('Início da Vigência não pode ser maior ou igual que o Fim da Vigência', null, {toastLife: 3000});
            isValid = false;
        }

        return isValid;
    }

    private isStringEmpty(str): boolean {
        if (isNullOrUndefined(str) || str === '') {
            return true;
        }
        return false;
    }

    remove(formDirective: FormGroupDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1) {
            return;
        }

        // noinspection TypeScriptValidateTypes
        const dialogRef = this.dialog.open(RemoveDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.removeEventEmitter.subscribe(() => {
            this.removeRow(formDirective);
        });
    }

    selectionSystemChange(event) {
        this.currentParameter.systemId = event.value;
        this.selectedSystem = event.value;
    }

    private removeRow(formDirective: FormGroupDirective) {

        const balanceIntegrate: BalanceIntegrate = this.highlightedRows.pop();
        balanceIntegrate.responsibleParam = new ResponsibleParam();
        balanceIntegrate.responsibleParam.responsibleValidity = this.userService.currentUser.username;

        this.service.remove(balanceIntegrate).subscribe(
            () => {
                this.dataSource.loadData(!isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` :
                SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);

                this.reset(formDirective);

                setTimeout(() => {
                    this.toastr.info('Parâmetro finalizado com sucesso', null, {toastLife: 3000});
                }, 500);
            }, (error) => {
                if (error.status === HttpStatus.NOT_ACCEPTABLE) {
                    this.toastr.error('Parâmetro não encontrado',
                        null, {toastLife: 3000, showCloseButton: true});
                } else if (error.status === HttpStatus.CONFLICT) {
                    this.toastr.error('Parâmetro já existente',
                        null, {toastLife: 3000, showCloseButton: true});
                }

                this.dataSource.loadData(!isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active}, ${this.sort.direction}` :
                SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);
            }
        );
    }

    isCardHeaderOutside(): boolean {
        return document.getElementById('cdk-describedby-message-container') !== null;
    }
}
