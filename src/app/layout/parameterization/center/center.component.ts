import {AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../../router.animations';
import {MatDialog, MatPaginator, MatSelectChange, MatSort, SortDirection} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {ToastsManager} from 'ng2-toastr';
import {tap} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {DatePipe, DOCUMENT} from '@angular/common';
import {RemoveDialogComponent} from '../type-movement/remove-dialog/remove-dialog.component';
import {CenterDataSource} from './center.data-source';
import {Center, ResponsibleParam} from '../parameterization.model';
import {CenterService} from './center.service';
import {HttpStatus} from '../../../shared/domain/http-status';
import {AllotEvaluationService} from '../../management-error/allot-evaluation/allot-evaluation.service';
import {UserService} from '../../../configuration/user.service';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {Authorization} from '../../../shared/domain/authorization.model';
import {CenterGridModel} from './center-grid.model';
import {EventTypeService} from '../event-type.service';
import {Company, Event, EventType, System} from '../../management-error/allot-evaluation/allot-evaluation.model';
import {EventService} from '../event.service';
import {Subscription} from 'rxjs/Subscription';
import {merge} from 'rxjs/observable/merge';

const SORT_ACTIVE_DEFAULT = '';
const SORT_DIRECTION_DEFAULT = '';
const SORT_ORDER_DEFAULT = '';

const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 50;

@Component({
    selector: 'app-center',
    templateUrl: './center.component.html',
    styleUrls: ['./center.component.scss'],
    animations: [routerTransition()]
})
export class CenterComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild(MatSort) sort: MatSort;

    dataSource: CenterDataSource;
    sizePerPage = CenterComponent.PAGE_SIZE_DEFAULT;

    columnsToView = ['system', 'eventType', 'event', 'company', 'product', 'operation', 'associated', 'accountStart', 'fixedProfitCenter',
        'finalCompoundProfitCenter', 'validityStart', 'validityEnd', 'responsibleRegister', 'responsibleValidity'];
    displayedColumns = ['system', 'eventType', 'event', 'company', 'product', 'operation', 'associated', 'accountStart', 'fixedProfitCenter',
        'finalCompoundProfitCenter', 'validityStart', 'validityEnd', 'responsibleRegister', 'responsibleValidity'];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    highlightedRows = [];

    company;
    associated;
    companies: Array<Company> = [];
    associateds = [];
    selectedCompany: number;
    selectedAssociated: string;

    selectedSystem: string;
    system;
    systems: Array<System>;
    systemSubscription: Subscription;

    selectedEventType: number;
    eventTypes: Array<EventType> = [];
    eventTypeSubscription: Subscription;
    eventTypeBlank = new EventType();

    selectedEvent: number;
    eventsLoading = false;
    events: Array<Event> = [];
    eventSubscription: Subscription;
    eventBlank = new Event();

    form: FormGroup;

    currentParameter: Center;
    currentParameterSearch: Center;

    startCalendar;
    endCalendar;
    columnsView: string[] = CenterGridModel.getColumns().map(value => value.label);

    labelParameter = 'Cadastrar';
    canFinishValidity = false;

    toggleRegisterChecked = false;
    position = 'after';

    eventTypeSelect;
    eventSelect;

    constructor(private service: CenterService,
                private userService: UserService,
                private eventTypeService: EventTypeService,
                private eventService: EventService,
                private managementErrorService: AllotEvaluationService,
                private formBuilder: FormBuilder,
                public dialog: MatDialog,
                private toastr: ToastsManager,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                @Inject(DOCUMENT) document) {
    }

    ngOnInit() {

        this.translatePaginatorActionButtons();

        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });

        this.dataSource = new CenterDataSource(this.service);

        this.formControlDefine();

        this.currentParameter = new Center();
        this.currentParameter.event = new Event();
        this.currentParameter.eventType = new EventType();

        this.currentParameterSearch = this.currentParameter;

        this.sort.active = SORT_ACTIVE_DEFAULT;
        this.sort.direction = <SortDirection>SORT_DIRECTION_DEFAULT;

        this.loadSystem();
        this.companies = this.managementErrorService.getCompanies();
        this.service.getAssociatedSelect().subscribe(a => this.associateds.push(a));

        this.loadEventTypes();
        this.loadEvents();
    }

    translatePaginatorActionButtons() {
        this.paginator._intl.firstPageLabel = 'Primeira página';
        this.paginator._intl.itemsPerPageLabel = 'Itens por página';
        this.paginator._intl.previousPageLabel = 'Página anterior';
        this.paginator._intl.nextPageLabel = 'Próxima página';
        this.paginator._intl.lastPageLabel = 'Última página';
    }

    ngOnDestroy(): void {
        /*
        Fazemos o unsubscribe() para evitar vazamno de memória
         */
        this.eventSubscription.unsubscribe();
        this.eventTypeSubscription.unsubscribe();
        this.systemSubscription.unsubscribe();
    }

    public static get PAGE_SIZE_DEFAULT() {
        return PAGE_SIZE_DEFAULT;
    }

    private loadSystem() {
        this.systemSubscription = this.managementErrorService.getSystems().subscribe(systems => this.systems = systems);
    }

    private loadEventTypes() {
        if (this.toggleRegisterChecked) {
            this.eventTypeSubscription = this.eventTypeService
                .findAllValid()
                .subscribe(eventTypes => this.eventTypes = eventTypes);
        } else {
            this.eventTypeSubscription = this.eventTypeService
                .findAll()
                .subscribe(eventTypes => this.eventTypes = eventTypes);
        }
    }

    private filterSystem(system: System, index: number): boolean {
        return system.system === this.selectedSystem;
    }

    private loadEvents(eventSelect: MatSelectChange = null) {
        this.events = [];
        this.eventsLoading = true;

        let system: System = null;
        if (!isNullOrUndefined(this.systems)) {
            const sys: System = this.systems.find(s => s.system === this.selectedSystem);
            system = isNullOrUndefined(this.selectedSystem) ? null : (isNullOrUndefined(sys) ? null : sys);
        }


        if (this.eventSubscription != null) {
            this.eventSubscription.unsubscribe();
        }

        const idEventType = this.currentParameter.eventType ? this.currentParameter.eventType.id : null;

        this.eventSubscription = this.eventService
            .findBySystemAndEventType(isNullOrUndefined(system) ? null : system.id, idEventType, this.toggleRegisterChecked)
            .subscribe(events => {
                this.events = events;
                this.eventsLoading = false;
                if (eventSelect != null) {
                    if (this.events == null) {
                        this.currentParameter.event = this.eventBlank;
                    }
                }
            });
    }

    reset(formDirective: FormGroupDirective) {
        this.formControlDefine();

        this.highlightedRows = [];

        this.labelParameter = 'Cadastrar';
        this.canFinishValidity = false;

        if (!isNullOrUndefined(formDirective)) {
            formDirective.resetForm();
        }

        if (!isNullOrUndefined(this.currentParameter.id)) {
            this.dataSource.loadData(
                !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                    `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null,
                PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT);
            this.paginator.firstPage();
        }

        this.currentParameter.finalCompoundProfitCenter = '';
        this.currentParameter.fixedProfitCenter = '';
        this.currentParameter = new Center();
        this.currentParameter.eventType = new EventType();
        this.currentParameter.event = new Event();

        this.currentParameterSearch = this.currentParameter;

        this.loadEventTypes();
        this.loadEvents();
    }

    private formControlDefine() {
        this.form = this.formBuilder.group({
            product: new FormControl('', []),
            operation: new FormControl('', []),
            accountStart: new FormControl('', []),
            fixedProfitCenter: new FormControl('', []),
            finalCompoundProfitCenter: new FormControl('', []),
            company: new FormControl('', []),
            associated: new FormControl('', []),
            eventTypeSelect: new FormControl('', []),
            eventSelect: new FormControl('', [])
        });

        this.associated = new FormControl('', []);
        this.eventSelect = new FormControl('', []);
        this.eventTypeSelect = new FormControl('', []);
        this.startCalendar = new FormControl('', Validators.required);
        this.endCalendar = new FormControl('');
        this.system = new FormControl('', []);

        this.selectedSystem = null;
        this.selectedEventType = null;
        this.selectedEvent = null;
        this.selectedCompany = null;
        this.selectedAssociated = null;
    }

    ngAfterViewInit() {
        this.sort.sortChange.pipe(
            tap(() => this.loadData())
        ).subscribe();

        merge(this.paginator.page).pipe(tap(() => this.loadData())).subscribe();
    }

    private loadData() {
        this.dataSource = new CenterDataSource(this.service);

        let currentParameterLoad = this.currentParameter;
        if (this.toggleRegisterChecked) {
            /*Quando não está marcado como busca, usamos o currentParameterSearch da busca para pesquisar.*/
            currentParameterLoad = this.currentParameterSearch;
        }

        this.dataSource.loadData(
            !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, currentParameterLoad, this.paginator.pageIndex, this.paginator.pageSize);
    }

    changeToggle(event: any) {
        if (this.toggleRegisterChecked) {
            this.toggleRegisterChecked = false;
        } else {
            this.toggleRegisterChecked = true;
        }

        if (this.toggleRegisterChecked) {
            this.startCalendar = new FormControl('', Validators.required);
            /*
            Como na pesquisa é permitido pesquisar pelo campo Centro de Lucro Composto e tbm pelo campo Centro Lucro Fixo, então limpamos apenas o campo Composto quando estamos trocando para
            o cadastro de novo centro.
            */
            this.currentParameter.finalCompoundProfitCenter = null;
            if (this.currentParameter.fixedProfitCenter != null) {
                /*Bloqueamos o campos, pois é permitido apenas um ou outro no cadastro*/
                this.form.controls['finalCompoundProfitCenter'].disable();
            }
        } else {
            this.reset(null);
            this.startCalendar = new FormControl('', []);
        }

        this.eventTypeService.clear();
        this.loadEventTypes();
        this.loadEvents();
    }

    onSelectRow(row: Center) {
        const index = this.highlightedRows.findIndex((a: Center) => a.id === row.id);

        if (index === -1 && this.highlightedRows.length < 1) { // permite adicionar somente 1 elemento por vez
            this.highlightedRows.push(row);
        } else {
            this.highlightedRows.splice(index, 1);
            this.highlightedRows.push(row);
        }

        this.currentParameter = row;
        if (this.currentParameter.event === null) {
            this.currentParameter.event = new Event();
        }
        if (this.currentParameter.eventType === null) {
            this.currentParameter.eventType = new EventType();
        }

        this.selectedSystem = this.currentParameter.system;
        this.selectedCompany = Number(this.currentParameter.company);
        this.selectedAssociated = this.currentParameter.associated;
        this.selectedEventType = this.currentParameter.eventType.id;
        this.selectedEvent = this.currentParameter.event.id;

        if (!isNullOrUndefined(row.validityEnd)) {
            this.canFinishValidity = false;
        } else {
            this.canFinishValidity = true;
        }
        /*if (!isNullOrUndefined(row.validityStart)) {
            this.startCalendar.value = row.validityStart;
        }
        if (!isNullOrUndefined(row.validityEnd)) {
            this.endCalendar.value = row.validityEnd;
        }*/

        if (!this.isStringEmpty(this.currentParameter.fixedProfitCenter)) {
            this.form.controls['finalCompoundProfitCenter'].disable();
        } else {
            this.form.controls['finalCompoundProfitCenter'].enable();
        }

        if (!this.isStringEmpty(this.currentParameter.finalCompoundProfitCenter)) {
            this.form.controls['fixedProfitCenter'].disable();
        } else {
            this.form.controls['fixedProfitCenter'].enable();
        }

        /*this.updateSelect(this.currentParameter.system, this.systems, this.selectedSystem, this.system);
        this.updateSelect(this.currentParameter.company, this.companies, this.selectedCompany, this.company);
        this.updateSelect(this.currentParameter.associated, this.associateds, this.selectedAssociated, this.associated);*/

        this.labelParameter = 'Editar';
        this.toggleRegisterChecked = true;
        this.loadEvents();
    }

    private updateSelect(currentValueOfParameter, valuesOfSelect, selectedValue, componentSelect) {
        if (!this.isStringEmpty(currentValueOfParameter)) {
            const indexSystem = valuesOfSelect.findIndex(a => {
                const val = '' + a.value;
                return val === currentValueOfParameter;
            });

            if (indexSystem === -1) {
                selectedValue.push(valuesOfSelect[indexSystem].value);
                componentSelect.setValue(valuesOfSelect[indexSystem].value);
            } else {
                selectedValue.splice(valuesOfSelect[indexSystem].value, 1);
                selectedValue.push(valuesOfSelect[indexSystem].value);
                componentSelect.setValue(valuesOfSelect[indexSystem].value);
            }
        } else {
            // noinspection JSUnusedAssignment
            selectedValue = [];
            componentSelect.setValue(null);
        }
    }

    changeFixedProfitCenter(event) {
        if (this.toggleRegisterChecked) {
            if (!this.isStringEmpty(event.data)) {
                this.currentParameter.finalCompoundProfitCenter = null;
                this.form.controls['finalCompoundProfitCenter'].disable();
            } else {
                this.form.controls['finalCompoundProfitCenter'].enable();
            }
        }
    }

    changeFinalCompoundProfitCenter(event) {
        if (this.toggleRegisterChecked) {
            if (!this.isStringEmpty(event.data)) {
                this.currentParameter.fixedProfitCenter = null;
                this.form.controls['fixedProfitCenter'].disable();
            } else {
                this.form.controls['fixedProfitCenter'].enable();
            }
        }
    }

    startDateChange(event) {
        let eventValue: Date;

        if (event.type === 'change') {
            eventValue = this.getStringToDate(event.target.value);
        } else {
            eventValue = event.value;
        }
        if (!isNullOrUndefined(eventValue)) {
            this.currentParameter.validityStart = eventValue;
            this.startCalendar.value = eventValue;
        }
    }

    endDateChange(event) {
        let eventValue: Date;

        if (event.type === 'change') {
            eventValue = this.getStringToDate(event.target.value);
        } else {
            eventValue = event.value;
        }
        if (!isNullOrUndefined(eventValue)) {
            this.currentParameter.validityEnd = eventValue;
            this.endCalendar.value = eventValue;
        }
    }

    getStringToDate(dateStrBR: string): Date {
        const dateSplit = dateStrBR.split('/');
        const dateEn = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0] + ' 00:00:00';
        return new Date(dateEn);
    }

    getDate(date) {
        const datePipe = new DatePipe('en-US');
        const dateReturn = datePipe.transform(date, 'dd/MM/yyyy');
        return new Date(date);
    }

    selectionSystemChange(event) {
        this.currentParameter.system = event.value;
        this.selectedSystem = event.value;
        this.loadEvents();
    }

    selectionEventTypeChange(event: MatSelectChange) {
        if (this.currentParameter.eventType === null) {
            this.currentParameter.eventType = this.eventTypeBlank;
        }

        this.currentParameter.eventType = this.eventTypes.find(e => event.value !== null && e.id === event.value);
        this.currentParameter.eventType = (this.currentParameter.eventType !== null && this.currentParameter.eventType !== undefined)
            ? this.currentParameter.eventType : this.eventTypeBlank;
        this.loadEvents(event);
    }

    selectionEventChange(event) {
        if (this.currentParameter.event === null) {
            this.currentParameter.event = new Event();
        }
        if (this.events !== null) {
            this.currentParameter.event = this.events.find(e => e.id === event.value);
        }
    }

    selectionCompanyChange(event) {
        this.currentParameter.company = event.value;
        this.selectedCompany = event.value;
    }

    selectionAssociatedChange(event) {
        this.currentParameter.associated = event.value;
    }

    search(formDirective: FormGroupDirective) {
        this.currentParameter.validityStart = this.startCalendar.value;
        this.currentParameter.validityEnd = this.endCalendar.value;
        this.currentParameterSearch = this.currentParameter;

        this.dataSource = new CenterDataSource(this.service);

        this.paginator.firstPage();
        this.dataSource.loadData(SORT_ORDER_DEFAULT, this.currentParameter,
            PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT);
        /*this.reset(formDirective);*/
    }

    getDateOfMonth(dateToTransform) {
        if (!isNullOrUndefined(dateToTransform) && dateToTransform !== '') {
            let dayStartDate = dateToTransform.getDate();
            let monthStartDate = dateToTransform.getMonth() + 1;
            const yearStartDate = dateToTransform.getFullYear();

            if (dayStartDate < 10) {
                dayStartDate = '0' + dayStartDate;
            }
            if (monthStartDate < 10) {
                monthStartDate = '0' + monthStartDate;
            }

            return `${yearStartDate}-${monthStartDate}-${dayStartDate}`;
        }
        return null;
    }

    save(formDirective: FormGroupDirective) {
        const formIsValid: boolean = this.validate();

        if (!formIsValid) {
            return;
        }

        this.currentParameter.validityStart = this.startCalendar.value;
        this.currentParameter.validityEnd = this.endCalendar.value;
        let responsibleParam = this.currentParameter.responsibleParam;
        responsibleParam = !responsibleParam ? new ResponsibleParam() : responsibleParam;

        if (!this.currentParameter.id) {
            responsibleParam.responsibleRegister = this.userService.currentUser.username;
        }

        if (this.currentParameter.validityEnd) {
            responsibleParam.responsibleValidity = this.userService.currentUser.username;
        }

        this.currentParameter.responsibleParam = responsibleParam;

        this.service.saveOrUpdate(this.currentParameter).subscribe(
            () => {
                this.dataSource.loadData(
                    !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                        `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);

                this.reset(formDirective);

                setTimeout(() => {
                    this.toastr.info('Parâmetro salvo com sucesso', null, {toastLife: 3000});
                }, 500);
            }, (error) => {
                if (error.status === HttpStatus.NOT_ACCEPTABLE) {
                    this.toastr.error('Parâmetro não encontrado',
                        null, {toastLife: 3000, showCloseButton: true});
                } else if (error.status === HttpStatus.CONFLICT) {
                    this.toastr.error('Parâmetro já existente',
                        null, {toastLife: 3000, showCloseButton: true});
                }

                this.dataSource.loadData(!isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                    `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);
            }
        );
    }

    onSubmit(formData: any, formDirective: FormGroupDirective) {
        if (this.toggleRegisterChecked) {
            this.save(formDirective);
        } else {
            this.search(formDirective);
        }
    }

    private validate() {
        let isValid = true;
        if (isNullOrUndefined(this.currentParameter.validityStart)) {
            this.toastr.warning('O campo Vigência Início deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }
        if (this.isStringEmpty(this.currentParameter.fixedProfitCenter)
            && this.isStringEmpty(this.currentParameter.finalCompoundProfitCenter)) {
            this.toastr.warning('O campo Centro de Lucro Fixo OU o campo Centro de Lucro Composto deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }
        if (!isNullOrUndefined(this.currentParameter.validityStart)
            && !isNullOrUndefined(this.currentParameter.validityEnd)
            && this.currentParameter.validityStart >= this.currentParameter.validityEnd) {
            this.toastr.warning('Início da Vigência DEVE ser MENOR que o Fim da Vigência', null, {toastLife: 3000});
            isValid = false;
        }

        return isValid;
    }

    private isStringEmpty(str): boolean {
        if (isNullOrUndefined(str) || str === '') {
            return true;
        }
        return false;
    }

    remove(formDirective: FormGroupDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1) {
            return;
        }

        // noinspection TypeScriptValidateTypes
        const dialogRef = this.dialog.open(RemoveDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.removeEventEmitter.subscribe(() => {
            this.removeRow(formDirective);
        });
    }

    private removeRow(formDirective: FormGroupDirective) {
        const center: Center = this.highlightedRows.pop();
        center.responsibleParam = new ResponsibleParam();
        center.responsibleParam.responsibleValidity = this.userService.currentUser.username;

        this.service.remove(center).subscribe(
            () => {
                this.dataSource.loadData(!isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                    `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);

                this.reset(formDirective);

                setTimeout(() => {
                    this.toastr.info('Parâmetro atualizado com sucesso', null, {toastLife: 3000});
                }, 500);
            }, (error) => {
                if (error.status === HttpStatus.NOT_ACCEPTABLE) {
                    this.toastr.error('Parâmetro não encontrado',
                        null, {toastLife: 3000, showCloseButton: true});
                } else if (error.status === HttpStatus.CONFLICT) {
                    this.toastr.error('Parâmetro já existente',
                        null, {toastLife: 3000, showCloseButton: true});
                }

                this.dataSource.loadData(!isNullOrUndefined(this.sort)
                    && !isNullOrUndefined(this.sort.active)
                    && !isNullOrUndefined(this.sort.direction) ?
                    `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null,
                    this.paginator.pageIndex, this.paginator.pageSize);
            }
        );
    }

    isCardHeaderOutside(): boolean {
        return document.getElementById('cdk-describedby-message-container') !== null;
    }
}
