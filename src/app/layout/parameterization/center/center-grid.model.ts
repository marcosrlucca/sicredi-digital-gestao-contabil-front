export class CenterGridModel {

    private static columnsItems: Array<CenterGridModel>;

    label: string;
    displayedColumn: string;

    private static newItem(label: string, displayedColumn): CenterGridModel {
        const item = new CenterGridModel();
        item.label = label;
        item.displayedColumn = displayedColumn;
        return item;
    }

    static getColumns(): Array<CenterGridModel> {
        if ( !this.columnsItems ) {
            this.columnsItems = new Array<CenterGridModel>();
            this.columnsItems.push(this.newItem( 'Sistema', 'system' ));
            this.columnsItems.push(this.newItem( 'Empresa', 'company' ));
            this.columnsItems.push(this.newItem( 'Produto', 'product' ));
            this.columnsItems.push(this.newItem( 'Operação', 'operation' ));
            this.columnsItems.push(this.newItem( 'Associado', 'associated' ));
            this.columnsItems.push(this.newItem( 'Início da Conta', 'accountStart' ));
            this.columnsItems.push(this.newItem( 'Centro Lucro Fixo', 'fixedProfitCenter' ));
            this.columnsItems.push(this.newItem( 'Centro Lucro Composto', 'finalCompoundProfitCenter' ));
            this.columnsItems.push(this.newItem( 'Início da Vigência', 'validityStart' ));
            this.columnsItems.push(this.newItem( 'Início da Vigência', 'validityStart' ));
        }

        return this.columnsItems;
    }
}
