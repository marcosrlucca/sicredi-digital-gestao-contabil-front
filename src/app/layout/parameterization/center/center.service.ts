import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Center} from '../parameterization.model';
import {Observable} from 'rxjs/Observable';
import {isNullOrUndefined} from 'util';

@Injectable()
export class CenterService {

    private _api_url = `${environment._api_accounting_management_parameterization_url}/cost-center`;

    constructor(private httpClient: HttpClient) {
    }

    findPaging(sortOrder, parameter: Center, pageIndex, pageSize): Observable<any> {
        let params = new HttpParams();
        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;
        params = !isNullOrUndefined(parameter) ? params.set('centerDTO', JSON.stringify(parameter)) : params;
        params = params.set('page', pageIndex.toString());
        params = params.set('size', pageSize.toString());

        return this.httpClient.get(this._api_url, {params});
    }

    findAll(sortOrder, parameter: Center) {
        let params = new HttpParams();
        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;
        params = !isNullOrUndefined(parameter) ? params.set('centerDTO', JSON.stringify(parameter)) : params;

        const objectObservable = this.httpClient.get(this._api_url, {params});
        return objectObservable;
    }

    remove(center: Center) {
        return this.httpClient.post(`${this._api_url}/${center.id}`, center);
    }

    saveOrUpdate(currentParameter: Center) {
        return this.httpClient.post(this._api_url, currentParameter);
    }

    public getAssociatedSelect() {
        return Observable.of(
            {value: 'N', viewValue: 'Não'},
            {value: 'S', viewValue: 'Sim'}
        );
    }

    public getCenterTypeSelect() {
        return Observable.of(
            {value: 'C', viewValue: 'Custo'},
            {value: 'L', viewValue: 'Lucro'}
        );
    }
}
