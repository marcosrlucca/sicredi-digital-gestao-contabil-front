import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { catchError, finalize } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { TypeMovement } from '../parameterization.model';
import { TypeMovementService } from './type-movement.service';
import { Page } from '../../../shared/domain/page.model';

const PAGE_SIZE_DEFAULT = 50;

export class TypeMovementDataSource implements DataSource<TypeMovement> {

    page = new Page();
    private loadingSubjects = new BehaviorSubject<boolean>(false);

    public data = new BehaviorSubject<TypeMovement[]>([]);
    public loading$ = this.loadingSubjects.asObservable();

    public noDataFound = false;

    constructor(private service: TypeMovementService) {
        this.page.pageSizeOptions = [50, 100, 200];
        this.page.numberOfElementsPerPage = PAGE_SIZE_DEFAULT;
    }

    connect(collectionViewer: CollectionViewer): Observable<TypeMovement[]> {
        return this.data.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.data.complete();
        this.loadingSubjects.complete();
    }

    loadData(sortOrder, parameter: TypeMovement, pageIndex, pageSize) {

        this.loadingSubjects.next(true);

        this.service.findPaging(sortOrder, parameter, pageIndex, pageSize)
            .pipe(
                catchError(() => of([])),
                finalize(() => this.loadingSubjects.next(false))
            )
            .subscribe(response => {
                if (response !== null) {

                    const responseContent = response['content'];
                    responseContent.map(center => {
                        if (center.validityStart != null) {
                            var parts = center.validityStart.split("/");
                            center.validityStart = new Date(parts[2], parts[1] - 1, parts[0]);
                        }

                        if (center.validityEnd != null) {
                            var parts = center.validityEnd.split("/");
                            center.validityEnd = new Date(parts[2], parts[1] - 1, parts[0]);
                        }
                    });

                    this.data.next(responseContent);
                    this.page = response;

                    this.noDataFound = false;
                } else {
                    this.noDataFound = true;
                }
            });
    }
}
