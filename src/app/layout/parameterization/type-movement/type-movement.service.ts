import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { TypeMovement } from '../parameterization.model';
import { isNullOrUndefined } from 'util';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TypeMovementService {

    private _api_url = `${environment._api_accounting_management_parameterization_url}/type-movement`;

    constructor(private httpClient: HttpClient) {
    }

    // findAllTypeMovement(sortOrder, parameter: TypeMovement) {
    //     let params = new HttpParams();
    //     params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;

    //     if (!isNullOrUndefined(parameter)) {
    //         params = !isNullOrUndefined(parameter.account) ? params.set('account', parameter.account) : params;
    //         params = !isNullOrUndefined(parameter.operation) ? params.set('operation', parameter.operation) : params;
    //         params = !isNullOrUndefined(parameter.typeAccount) ? params.set('typeAccount', parameter.typeAccount) : params;
    //         params = !isNullOrUndefined(parameter.typeMovement) ? params.set('typeMovement', parameter.typeMovement) : params;
    //         params = !isNullOrUndefined(parameter.validityStart) && parameter.validityStart !== '' ? params.set('validityStart', parameter.validityStart) : params;
    //         params = !isNullOrUndefined(parameter.validityEnd) && parameter.validityEnd !== '' ? params.set('validityEnd', parameter.validityEnd) : params;
    //     }

    //     return this.httpClient.get(this._api_url, {params});
    // }

    findAll(sortOrder, parameter: TypeMovement) {
        let params = new HttpParams();
        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;

        if (!isNullOrUndefined(parameter)) {
            params = !isNullOrUndefined(parameter.account) ? params.set('account', parameter.account) : params;
            params = !isNullOrUndefined(parameter.operation) ? params.set('operation', parameter.operation) : params;
            params = !isNullOrUndefined(parameter.typeAccount) ? params.set('typeAccount', parameter.typeAccount) : params;
            params = !isNullOrUndefined(parameter.typeMovement) ? params.set('typeMovement', parameter.typeMovement) : params;
            params = !isNullOrUndefined(parameter.validityStart) && parameter.validityStart !== '' ? params.set('validityStart', parameter.validityStart) : params;
            params = !isNullOrUndefined(parameter.validityEnd) && parameter.validityEnd !== '' ? params.set('validityEnd', parameter.validityEnd) : params;
        }

        return this.httpClient.get(this._api_url, {params});
    }


    findPaging(sortOrder, parameter: TypeMovement, pageIndex, pageSize): Observable<any> {
        let params = new HttpParams();
        params = !isNullOrUndefined(sortOrder) ? params.set('sort', sortOrder) : params;
        params = !isNullOrUndefined(parameter) ? params.set('centerDTO', JSON.stringify(parameter)) : params;
        params = params.set('page', pageIndex.toString());
        params = params.set('size', pageSize.toString());

        return this.httpClient.get(this._api_url, {params});
    }

    remove(balanceIntegrate: TypeMovement) {
        return this.httpClient.post(`${this._api_url}/${balanceIntegrate.id}`, balanceIntegrate);
    }

    saveOrUpdate(currentParameter: TypeMovement) {
        return this.httpClient.post(this._api_url, currentParameter);
    }
}
