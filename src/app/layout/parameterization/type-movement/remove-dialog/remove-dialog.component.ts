import {MatDialogRef} from '@angular/material';
import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-remove-dialog',
    templateUrl: 'remove-dialog.component.html',
    styleUrls: ['remove-dialog.component.scss']
})
export class RemoveDialogComponent {
    @Output() removeEventEmitter = new EventEmitter();

    constructor(public dialogRef: MatDialogRef<RemoveDialogComponent>) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onYesClick() {
        this.dialogRef.close();
        this.removeEventEmitter.emit();
    }

}
