import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr';
import { routerTransition } from '../../../router.animations';
import { TypeMovementDataSource } from './type-movement.data-source';
import { tap } from 'rxjs/operators';
import { ResponsibleParam, TypeMovement } from '../parameterization.model';
import { MatDatepickerInputEvent, MatDialog, MatSort, SortDirection, MatPaginator } from '@angular/material';
import { RemoveDialogComponent } from './remove-dialog/remove-dialog.component';
import { isNullOrUndefined } from 'util';
import { DatePipe, DOCUMENT } from '@angular/common';
import { TypeMovementService } from './type-movement.service';
import { HttpStatus } from '../../../shared/domain/http-status';
import { NgxPermissionsService, NgxRolesService } from 'ngx-permissions';
import { UserService } from '../../../configuration/user.service';
import {merge} from 'rxjs/observable/merge';

const SORT_ACTIVE_DEFAULT = '';
const SORT_DIRECTION_DEFAULT = '';
const SORT_ORDER_DEFAULT = '';

const PAGE_INDEX_DEFAULT = 0;
const PAGE_SIZE_DEFAULT = 50;

@Component({
    selector: 'app-type-movement',
    templateUrl: './type-movement.component.html',
    styleUrls: ['./type-movement.component.scss'],
    animations: [routerTransition()]
})
export class TypeMovementComponent implements OnInit, AfterViewInit {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    dataSource: TypeMovementDataSource;
    sizePerPage = TypeMovementComponent.PAGE_SIZE_DEFAULT;

    displayedColumns = ['account', 'operation', 'typeMovement', 'typeAccount', 'validityStart', 'validityEnd', 'resposibleRegister', 'resposibleValidity'];
    highlightedRows = [];
    form: FormGroup;

    currentParameter: TypeMovement;
    currentParameterSearch: TypeMovement;

    startCalendar;
    endCalendar;
    startDate;
    endDate;

    labelParameter = 'Cadastrar';
    canFinishValidity = false;

    toggleRegisterChecked = false;
    position = 'after';

    constructor(private service: TypeMovementService,
                private formBuilder: FormBuilder,
                public dialog: MatDialog,
                private toastr: ToastsManager,
                private userService: UserService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                @Inject(DOCUMENT) document) {
    }

    ngOnInit() {
        this.translatePaginatorActionButtons();

        this.dataSource = new TypeMovementDataSource(this.service);

        this.form = this.formBuilder.group({
            account: new FormControl('', [Validators.required]),
            operation: new FormControl('', [Validators.required]),
            typeAccount: new FormControl('', [Validators.required]),
            typeMovement: new FormControl('', [Validators.required, Validators.minLength(0), Validators.maxLength(3)])
        });

        this.startCalendar = new FormControl('', Validators.required);
        this.endCalendar = new FormControl('');

        this.currentParameter = new TypeMovement();

        this.sort.active = SORT_ACTIVE_DEFAULT;
        this.sort.direction = <SortDirection>SORT_DIRECTION_DEFAULT;
    }

    translatePaginatorActionButtons() {
        this.paginator._intl.firstPageLabel = 'Primeira página';
        this.paginator._intl.itemsPerPageLabel = 'Itens por página';
        this.paginator._intl.previousPageLabel = 'Página anterior';
        this.paginator._intl.nextPageLabel = 'Próxima página';
        this.paginator._intl.lastPageLabel = 'Última página';
    }

    public static get PAGE_SIZE_DEFAULT() {
        return PAGE_SIZE_DEFAULT;
    }

    ngAfterViewInit() {
        this.sort.sortChange.pipe(
            tap(() => this.loadData())
        ).subscribe();

        merge(this.paginator.page).pipe(tap(() => this.loadData())).subscribe();
    }

    private loadData() {
        this.dataSource = new TypeMovementDataSource(this.service);

        let currentParameterLoad = this.currentParameter;
        if (this.toggleRegisterChecked) {
            /*Quando não está marcado como busca, usamos o currentParameterSearch da busca para pesquisar.*/
            currentParameterLoad = this.currentParameterSearch;
        }

        this.dataSource.loadData(
            !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, currentParameterLoad, this.paginator.pageIndex, this.paginator.pageSize);
    }


    changeToggle(event: any) {
        if (this.toggleRegisterChecked) {
            this.toggleRegisterChecked = false;
        } else {
            this.toggleRegisterChecked = true;
        }

        if (this.toggleRegisterChecked) {
            this.form = this.formBuilder.group({
                account: new FormControl('', [Validators.required]),
                operation: new FormControl('', [Validators.required]),
                typeAccount: new FormControl('', [Validators.required]),
                typeMovement: new FormControl('', [Validators.required, Validators.minLength(0), Validators.maxLength(3)])
            });

            this.startCalendar = new FormControl('', Validators.required);
        } else {
            this.reset(null);
            this.form = this.formBuilder.group({
                account: new FormControl(''),
                operation: new FormControl(''),
                typeAccount: new FormControl(''),
                typeMovement: new FormControl('', [Validators.minLength(0), Validators.maxLength(3)])
            });

            this.startCalendar = new FormControl('');
        }
    }

    onSelectRow(row: TypeMovement) {
        const index = this.highlightedRows.findIndex((a: TypeMovement) => a.id === row.id);

        if (index === -1 && this.highlightedRows.length < 1) { // permite adicionar somente 1 elemento por vez
            this.highlightedRows.push(row);
        } else {
            this.highlightedRows.splice(index, 1);
            this.highlightedRows.push(row);
        }

        this.currentParameter = row;

        if (!isNullOrUndefined(row.validityStart)) {
            this.startDate = this.getDate(row.validityStart);
        } else {
            this.startCalendar = new FormControl('', Validators.required);
            this.startDate = '';
        }

        if (!isNullOrUndefined(row.validityEnd)) {
            this.endDate = this.getDate(row.validityEnd);
            this.canFinishValidity = false;
        } else {
            this.canFinishValidity = true;

            this.endCalendar = new FormControl('');
            this.endDate = '';
        }

        this.labelParameter = 'Editar';
        this.toggleRegisterChecked = true;
    }

    getDateAsString(event: MatDatepickerInputEvent<Date>) {
        const date = event.value.getDate() < 10 ? `0${event.value.getDate()}` : event.value.getDate();
        const month = event.value.getMonth() < 10 ? `0${event.value.getMonth() + 1}` : event.value.getMonth() + 1;
        const year = event.value.getFullYear();

        return `${date}/${month}/${year}`;
    }

    startDateChange(event: MatDatepickerInputEvent<Date>) {
        if (!isNullOrUndefined(event.value)) {
            this.currentParameter.validityStartToView = this.getDateAsString(event);
        }
    }

    endDateChange(event: MatDatepickerInputEvent<Date>) {
        if (!isNullOrUndefined(event.value)) {
            this.currentParameter.validityEndToView = this.getDateAsString(event);
        }
    }

    getDate(date) {
        const datePipe = new DatePipe('en-US');
        const dateReturn = datePipe.transform(date, 'dd/MM/yyyy');
        return new Date(date);
    }

    save(formDirective: FormGroupDirective) {
        const formIsValid: boolean = this.validate();

        if (!formIsValid) {
            return;
        }

        this.currentParameter.validityStart = this.startDate;
        this.currentParameter.validityEnd = this.endDate;
        let responsibleParam = this.currentParameter.responsibleParam;
        responsibleParam = !responsibleParam ? new ResponsibleParam() : responsibleParam;

        if ( !this.currentParameter.id ) {
            responsibleParam.responsibleRegister = this.userService.currentUser.username;
        }

        if ( this.currentParameter.validityEnd ) {
            responsibleParam.responsibleValidity = this.userService.currentUser.username;
        }

        this.currentParameter.responsibleParam = responsibleParam;

        this.service.saveOrUpdate(this.currentParameter).subscribe(
            () => {
                this.dataSource.loadData(
                    !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                    `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);

                this.reset(formDirective);

                setTimeout(() => {
                    this.toastr.info('Parâmetro salvo com sucesso', null, {toastLife: 3000});
                }, 500);
            }, (error) => {
                if (error.status === HttpStatus.NOT_ACCEPTABLE) {
                    this.toastr.error('Parâmetro não encontrado',
                        null, {toastLife: 3000, showCloseButton: true});
                } else if (error.status === HttpStatus.CONFLICT) {
                    this.toastr.error('Parâmetro já existente',
                        null, {toastLife: 3000, showCloseButton: true});
                }

                this.dataSource.loadData(!isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);
            }
        );
    }

    search(formDirective: FormGroupDirective) {
        // if (isNullOrUndefined(this.currentParameter) ||
        //     (isNullOrUndefined(this.currentParameter.account) &&
        //         isNullOrUndefined(this.currentParameter.operation) &&
        //         (isNullOrUndefined(this.currentParameter.typeMovement) || this.currentParameter.typeMovement.trim() === '') &&
        //         (isNullOrUndefined(this.currentParameter.typeAccount) || this.currentParameter.typeAccount.trim() === '') &&
        //         (isNullOrUndefined(this.startDate)) &&
        //         (isNullOrUndefined(this.endDate)))) {
        //     this.toastr.warning('Pelo menos um campo deve ser preenchido para realizar a pesquisa', null, {toastLife: 3000});
        //     return;
        // }

        this.currentParameter.validityStart = this.getDateOfMonth(this.startDate);
        this.currentParameter.validityEnd = this.getDateOfMonth(this.endDate);

        this.dataSource = new TypeMovementDataSource(this.service);

        this.dataSource.loadData(SORT_ORDER_DEFAULT, this.currentParameter,
            PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT);

        this.reset(formDirective);
    }

    getDateOfMonth(dateToTransform) {
        if (!isNullOrUndefined(dateToTransform) && dateToTransform !== '') {
            let dayStartDate = dateToTransform.getDate();
            let monthStartDate = dateToTransform.getMonth() + 1;
            const yearStartDate = dateToTransform.getFullYear();

            if (dayStartDate < 10) {
                dayStartDate = '0' + dayStartDate;
            }
            if (monthStartDate < 10) {
                monthStartDate = '0' + monthStartDate;
            }

            return `${yearStartDate}-${monthStartDate}-${dayStartDate}`;
        }
        return null;
    }

    onSubmit(formData: any, formDirective: FormGroupDirective) {
        if (this.toggleRegisterChecked) {
            this.save(formDirective);
        } else {
            this.search(formDirective);
        }
    }

    private validate() {
        let isValid = true;

        if (isNullOrUndefined(this.currentParameter)) {
            this.toastr.warning('Campos incorretos', null, {toastLife: 3000});
            isValid = false;
        }
        if (isNullOrUndefined(this.currentParameter.account) || ('' + this.currentParameter.account).trim() === '') {
            this.toastr.warning('O campo Conta deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }
        if (isNullOrUndefined(this.currentParameter.operation) || ('' + this.currentParameter.operation).trim() === '') {
            this.toastr.warning('O campo Operação deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }
        if (isNullOrUndefined(this.currentParameter.typeMovement) || ('' + this.currentParameter.typeMovement).trim() === '') {
            this.toastr.warning('O campo Tipo de Movimento deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }
        if (isNullOrUndefined(this.currentParameter.typeAccount) || ('' + this.currentParameter.typeAccount).trim() === '') {
            this.toastr.warning('O campo Tipo da Operação deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }
        if (isNullOrUndefined(this.startDate) || !this.startCalendar.valid || this.startDate === '') {
            this.toastr.warning('O campo Início da Vigência deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        } else if (isNullOrUndefined(this.startDate) || this.startDate === '') {
            this.toastr.warning('O campo Vigência Início deve ser preenchido', null, {toastLife: 3000});
            isValid = false;
        }
        if (!isNullOrUndefined(this.startDate) && !isNullOrUndefined(this.endDate) && this.endDate !== '' && this.startDate > this.endDate) {
            this.toastr.warning('Início da Vigência não pode ser maior que o Fim da Vigência', null, {toastLife: 3000});
            isValid = false;
        }

        return isValid;
    }

    remove(formDirective: FormGroupDirective) {
        if (isNullOrUndefined(this.highlightedRows) || this.highlightedRows.length < 1) {
            return;
        }

        // noinspection TypeScriptValidateTypes
        const dialogRef = this.dialog.open(RemoveDialogComponent, {
            width: '69ex'
        });

        dialogRef.componentInstance.removeEventEmitter.subscribe(() => {
            this.removeRow(formDirective);
        });
    }

    private removeRow(formDirective: FormGroupDirective) {
        const typeMovement: TypeMovement = this.highlightedRows.pop();
        typeMovement.responsibleParam = new ResponsibleParam();
        typeMovement.responsibleParam.responsibleValidity = this.userService.currentUser.username;

        this.service.remove(typeMovement).subscribe(
            () => {
                this.dataSource.loadData(!isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null, this.paginator.pageIndex, this.paginator.pageSize);

                this.reset(formDirective);

                setTimeout(() => {
                    this.toastr.info('Parâmetro atualizado com sucesso', null, {toastLife: 3000});
                }, 500);
            }, (error) => {
                if (error.status === HttpStatus.NOT_ACCEPTABLE) {
                    this.toastr.error('Parâmetro não encontrado',
                        null, {toastLife: 3000, showCloseButton: true});
                } else if (error.status === HttpStatus.CONFLICT) {
                    this.toastr.error('Parâmetro já existente',
                        null, {toastLife: 3000, showCloseButton: true});
                }

                this.dataSource.loadData(!isNullOrUndefined(this.sort)
                    && !isNullOrUndefined(this.sort.active)
                    && !isNullOrUndefined(this.sort.direction) ?
                    `${this.sort.active},${this.sort.direction}` : SORT_ORDER_DEFAULT, null,
                    this.paginator.pageIndex, this.paginator.pageSize);
            }
        );
    }

    isSideBarActive(): boolean {
        if (document.getElementsByClassName('push-right').length <= 0) {
            return false;
        }
        return true;
    }

    reset(formDirective: FormGroupDirective) {
        this.form = this.formBuilder.group({
            account: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
            operation: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
            typeAccount: new FormControl('', [Validators.required]),
            typeMovement: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)])
        });

        this.startCalendar = new FormControl('', Validators.required);
        this.endCalendar = new FormControl('');

        this.startDate = '';
        this.endDate = '';

        this.highlightedRows = [];

        this.labelParameter = 'Cadastrar';
        this.canFinishValidity = false;

        if (!isNullOrUndefined(formDirective)) {
            formDirective.resetForm();
        }

        if (!isNullOrUndefined(this.currentParameter.id)) {
            this.dataSource.loadData(
                !isNullOrUndefined(this.sort) && !isNullOrUndefined(this.sort.active) && !isNullOrUndefined(this.sort.direction) ?
                `${this.sort.active},${this.sort.direction}` :
                SORT_ORDER_DEFAULT, null, PAGE_INDEX_DEFAULT, PAGE_SIZE_DEFAULT);
            this.paginator.firstPage();
        }

        this.currentParameter = new TypeMovement();
    }

    isCardHeaderOutside(): boolean {
        return document.getElementById('cdk-describedby-message-container') !== null;
    }
}

