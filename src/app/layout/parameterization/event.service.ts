import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Event} from '../management-error/allot-evaluation/allot-evaluation.model';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EventService {
    private _api_url = environment._api_accounting_management_parameterization_url;
    public pathGetEvent = `${this._api_url}/event`;

    public events: Observable<Array<Event>>;

    constructor(private httpClient: HttpClient) {
    }

    public findBySystemAndEventType(systemId: number, eventTypeId: number, isFindValid?: boolean) {
        let params = new HttpParams();
        params = !isNullOrUndefined(systemId) ? params.append('systemid', systemId.toString()) : params;
        params = !isNullOrUndefined(eventTypeId) ? params.append('eventtypeid', eventTypeId.toString()) : params;
        params = !isNullOrUndefined(isFindValid) ? params.append('isfindvalid', isFindValid.toString()) : params;

        return this.httpClient.get<Array<Event>>(`${this.pathGetEvent}/filter`, {params});
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server Error');
    }
}
