import { Component, OnInit} from '@angular/core';
import {routerTransition} from '../../router.animations';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {Authorization} from '../../shared/domain/authorization.model';
import {UserService} from '../../configuration/user.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-parameterization',
    templateUrl: './parameterization.html',
    styleUrls: ['./parameterization.scss'],
    animations: [routerTransition()]
})
export class ParameterizationComponent implements OnInit {

    constructor(private userService: UserService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                public router: Router) {
    }

    ngOnInit(): void {
        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });
    }

    goToTypeMovement() {
        this.router.navigate(['/parametrizacao/tipo-movimento']);
    }

    goToCenter() {
        this.router.navigate(['/parametrizacao/centro']);
    }

    goToBalanceIntegrate() {
        this.router.navigate(['/parametrizacao/integracao-saldo']);
    }
}
