import {NgModule} from '@angular/core';

import {NgbAlertModule, NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ParameterizationComponent} from './parameterization.component';
import {TypeMovementComponent} from './type-movement/type-movement.component';
import {CenterComponent} from './center/center.component';
import {SharedModule} from '../../shared/modules/shared/shared.module';
import {StatModule} from '../../shared/modules/stat/stat.module';
import {ParameterizationRoutingModule} from './parameterization-routing.module';
import {PageHeaderModule} from '../../shared/modules/page-header/page-header.module';
import {
    MatButtonModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule
} from '@angular/material';
import {RemoveDialogComponent} from './type-movement/remove-dialog/remove-dialog.component';
import {TypeMovementService} from './type-movement/type-movement.service';
import {CenterService} from './center/center.service';
import {BalanceIntegrateService} from './balance-integrate/balance-integrate.service';
import {BalanceIntegrateComponent} from './balance-integrate/balance-integrate.component';
import {AllotEvaluationService} from '../management-error/allot-evaluation/allot-evaluation.service';
import {EventTypeService} from './event-type.service';
import {EventService} from './event.service';

@NgModule({
    imports:
        [
            SharedModule,
            FormsModule,
            MatNativeDateModule,
            MatInputModule,
            MatFormFieldModule,
            MatDialogModule,
            MatButtonModule,
            MatTooltipModule,
            MatTableModule,
            MatSlideToggleModule,
            MatProgressSpinnerModule,
            MatNativeDateModule,
            MatDatepickerModule,
            MatProgressSpinnerModule,
            MatRadioModule,
            MatSelectModule,
            MatSortModule,
            MatPaginatorModule,
            NgbCarouselModule.forRoot(),
            NgbAlertModule.forRoot(),
            ParameterizationRoutingModule,
            ReactiveFormsModule,
            PageHeaderModule,
            StatModule
        ],
    entryComponents: [
        RemoveDialogComponent
        ],
    providers: [
        TypeMovementService,
        CenterService,
        BalanceIntegrateService,
        AllotEvaluationService,
        EventTypeService,
        EventService
    ],
    declarations:
        [
            ParameterizationComponent,
            TypeMovementComponent,
            CenterComponent,
            BalanceIntegrateComponent,
            RemoveDialogComponent
        ]
})
export class ParameterizationModule {
}
