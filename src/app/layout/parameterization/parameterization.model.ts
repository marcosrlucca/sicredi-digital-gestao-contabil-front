import {Event, EventType} from '../management-error/allot-evaluation/allot-evaluation.model';

export class ResponsibleParam {
    responsibleRegister: string;
    responsibleValidity: string;
}

export class BalanceIntegrate {
    id: string;
    systemId: number;
    systemName: String;
    accountHeading: string;
    startDate: string;
    startDateToView: string;
    endDate: string;
    endDateToView: string;
    responsibleParam: ResponsibleParam;
}

export class TypeMovement {
    id: number;
    account: string;
    operation: string;

    typeAccount: string;
    typeMovement: string;

    validityStart: string;
    validityEnd: string;
    validityStartToView: string;
    validityEndToView: string;
    responsibleParam: ResponsibleParam;

    accountHeading: string;
    systemId: number;
    startDate: string;
    endDate: string;
}

export class Center {
    id: string;

    system: string;
    eventType: EventType;
    event: Event;

    company: string;
    product: string;
    operation: string;

    associated: string;

    accountStart: string;
    fixedProfitCenter: string;
    finalCompoundProfitCenter: string;

    validityStart: Date;
    validityEnd: Date;
    responsibleParam: ResponsibleParam;
}
