import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {EventType} from '../management-error/allot-evaluation/allot-evaluation.model';
import {Observable} from 'rxjs/Observable';
import {isNullOrUndefined} from 'util';

@Injectable()
export class EventTypeService {
    private _api_url = environment._api_accounting_management_parameterization_url;
    public pathGetEventType = `${this._api_url}/eventtype`;

    public allEventTypes: Observable<Array<EventType>>;
    public allValidEventTypes: Observable<Array<EventType>>;

    constructor(private httpClient: HttpClient) {
    }

    public findAll(): Observable<Array<EventType>> {
        if ( isNullOrUndefined( this.allEventTypes ) ) {
            this.allEventTypes = this.httpClient.get<Array<EventType>>(`${this.pathGetEventType}/listall`);
        }
        return this.allEventTypes;
    }

    public findAllValid(): Observable<Array<EventType>> {
        if ( isNullOrUndefined( this.allValidEventTypes ) ) {
            this.allValidEventTypes = this.httpClient.get<Array<EventType>>(`${this.pathGetEventType}/listallvalid`);
        }
        return this.allValidEventTypes;
    }

    public clear() {
        this.allEventTypes = null;
        this.allValidEventTypes = null;
    }
}
