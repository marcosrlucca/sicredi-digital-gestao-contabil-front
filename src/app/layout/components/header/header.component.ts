import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {UserSessionService} from '../../../shared/services/user-session.service';
import {UserService} from '../../../configuration/user.service';
import {StatusManagementErrorService} from '../../../shared/services/status-management-error.service';
import {StatusEvaluationIntegrationService} from '../../../shared/services/status-evaluation-integration.service';
import {StatusTopazMovementService} from '../../../shared/services/status-topaz-movement.service';
import {isNullOrUndefined} from 'util';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {Authorization} from '../../../shared/domain/authorization.model';
import {StatusMassiveRegistrationService} from '../../../shared/services/status-massive-registration.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    pushRightClass = 'push-right';

    constructor(private translate: TranslateService,
                public statusManagementErrorService: StatusManagementErrorService,
                public statusEvaluationIntegrationService: StatusEvaluationIntegrationService,
                public statusTopazMovementService: StatusTopazMovementService,
                public statusMassiveRegistration: StatusMassiveRegistrationService,
                public userService: UserService,
                private userSessionService: UserSessionService,
                private permissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                public router: Router) {

        this.translate.addLangs(['pt-br']);
        this.translate.setDefaultLang('pt-br');
        const browserLang = this.translate.getBrowserLang();
        // this.translate.use(browserLang.match(/pt-br|fr|ur|es|it|fa|de/) ? browserLang : 'pt-br');
        this.translate.use('pt-br');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.statusManagementErrorService.initStatus();

        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.permissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });
    }

    goToTotalError() {
        this.statusManagementErrorService.chooseError();
        this.router.navigate(['/gestao-erro/avaliacao-lote']);
    }

    goToTotalNotSent() {
        this.statusManagementErrorService.chooseNotSent();
        this.router.navigate(['/gestao-erro/avaliacao-lote']);
    }

    goToTotalDisconsidered() {
        this.statusManagementErrorService.chooseDisconsidered();
        this.router.navigate(['/gestao-erro/avaliacao-lote']);
    }

    goToLogsKettle() {
        this.router.navigate(['/gestao-erro/erro-sistemico']);
    }

    goToMassiveRegistration() {
        this.statusMassiveRegistration.chooseError();
        this.router.navigate(['/engine/cadastramento-massivo-inconsistente']);
    }

    goToIntegrationEvaluation() {
        this.statusEvaluationIntegrationService.chooseError();
        this.router.navigate(['/gestao-erro/avaliacao-integracao']);
    }

    goToTopazMovementToViewNotIntegrated() {
        this.statusTopazMovementService.chooseNotIntegrated();
        this.router.navigate(['/gestao-erro/erro-falta-historico-topaz']);
    }

    goToAll() {
        this.router.navigate(['/gestao-erro/avaliacao-lote']);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    logout() {
        this.userSessionService.logout().subscribe(() => {
            this.userSessionService.currentUser = null;
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 300);
        });
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    getTotalErrorAllotment() {
        if (isNullOrUndefined(this.statusManagementErrorService.status) || isNullOrUndefined(this.statusManagementErrorService.status.totalError)) {
            return 0;
        }
        return this.statusManagementErrorService.status.totalError;
    }

    getTotalNotSentAllotment() {
        if (isNullOrUndefined(this.statusManagementErrorService.status) || isNullOrUndefined(this.statusManagementErrorService.status.totalNotSent)) {
            return 0;
        }
        return this.statusManagementErrorService.status.totalNotSent;
    }

    getTotalDisconsidered() {
        if (isNullOrUndefined(this.statusManagementErrorService.status) || isNullOrUndefined(this.statusManagementErrorService.status.totalDisconsidered)) {
            return 0;
        }
        return this.statusManagementErrorService.status.totalDisconsidered;
    }

    getTotalLogKettle() {
        if (isNullOrUndefined(this.statusManagementErrorService.status) || isNullOrUndefined(this.statusManagementErrorService.status.totalLogKettle)) {
            return 0;
        }
        return this.statusManagementErrorService.status.totalLogKettle;
    }

    getTotalErrorEvaluationIntegration() {
        if (isNullOrUndefined(this.statusEvaluationIntegrationService) || isNullOrUndefined(this.statusEvaluationIntegrationService.totalError)) {
            return 0;
        }
        return this.statusEvaluationIntegrationService.totalError;
    }

    getTotalNotIntegratedStatusTopazMovement() {
        if (isNullOrUndefined(this.statusTopazMovementService) || isNullOrUndefined(this.statusTopazMovementService.totalNotIntegrated)) {
            return 0;
        }
        return this.statusTopazMovementService.totalNotIntegrated;
    }

    getTotalMassiveRegistration() {
        if (isNullOrUndefined(this.statusMassiveRegistration) || isNullOrUndefined(this.statusMassiveRegistration.totalError)) {
            return 0;
        }
        return this.statusMassiveRegistration.totalError;
    }
}
