import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../../../configuration/user.service';
import {UserSessionService} from '../../../shared/services/user-session.service';
import {NgxPermissionsService, NgxRolesService} from 'ngx-permissions';
import {Authorization} from '../../../shared/domain/authorization.model';
import {PermissionService} from '../../../shared/services/permission.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    isActive = false;
    showMenu = '';
    pushRightClass = 'push-right';

    constructor(private translate: TranslateService,
                public userService: UserService,
                private userSessionService: UserSessionService,
                private ngxPermissionsService: NgxPermissionsService,
                private roleService: NgxRolesService,
                public permission: PermissionService,
                public router: Router) {
        this.translate.addLangs(['pt-br']);
        this.translate.setDefaultLang('pt-br');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/pt-br/) ? browserLang : 'pt-br');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit(): void {
        this.userService.getPermissions().subscribe((authorities: Authorization) => {
            this.ngxPermissionsService.loadPermissions(authorities.permission);
            this.roleService.addRole(authorities.role, authorities.permission);
        });
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    hideSidNav() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    logout() {
        this.userSessionService.logout().subscribe(() => {
            this.userSessionService.currentUser = null;
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 300);
        });
    }
}
