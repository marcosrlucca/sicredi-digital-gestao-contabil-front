import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {routerTransition} from '../router.animations';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserSessionService} from '../shared/services/user-session.service';
import {UserService} from '../configuration/user.service';
import {ToastsManager} from 'ng2-toastr/ng2-toastr';
import {HttpStatus} from '../shared/domain/http-status';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    form: FormGroup;

    constructor(public router: Router,
                private userSessionService: UserSessionService,
                private userService: UserService,
                private formBuilder: FormBuilder,
                private toastr: ToastsManager) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            username: [, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(64)])],
            password: [, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(32)])]
        });
    }

    login() {
        this.userSessionService.login(this.form.value)
            .subscribe(() => {
                    this.userService.getMyInfo().subscribe(() => {
                        this.router.navigate(['/']);
                        this.userSessionService.currentUser = this.userService.currentUser;
                    });
                },
                (error) => {
                    if (error && error.error && error.error.message &&
                        error.error.message === 'Authentication Failed: [LDAP: error code 49 - Invalid Credentials]') {
                        this.toastr.error('Usuário e/ou senha inválidos', null, {toastLife: 3000, showCloseButton: true});
                    } else if (error.status === HttpStatus.UNAUTHORIZED) {
                        this.toastr.error('Usuário não autorizado', null, {toastLife: 3000, showCloseButton: true});
                    } else if (error.status === HttpStatus.UNPROCESSABLE_ENTITY) {
                        this.toastr.error('Usuário não encontrado', null, {toastLife: 3000, showCloseButton: true});
                    }
                });
    }
}
