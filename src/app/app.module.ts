import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ConfigService} from './shared/services/config.service';
import {UserSessionService} from './shared/services/user-session.service';
import {AuthenticationModule} from './configuration/authentication.module';
import {UserService} from './configuration/user.service';
import {ToastModule} from 'ng2-toastr';
import {NgxPermissionsModule} from 'ngx-permissions';
import {CompanyService} from './shared/services/company.service';
import {StatusManagementErrorService} from './shared/services/status-management-error.service';
import {StatusEvaluationIntegrationService} from './shared/services/status-evaluation-integration.service';
import {StatusTopazMovementService} from './shared/services/status-topaz-movement.service';
import {SystemService} from './shared/services/system.service';
import {StatusMassiveRegistrationService} from './shared/services/status-massive-registration.service';
import {MessageHandler} from './shared/utils/message.handler';
import {DateUtils} from './shared/utils/date.utils';
import {PermissionService} from './shared/services/permission.service';
import localePt from '@angular/common/locales/pt';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function initUserFactory(userService: UserService) {
    return () => userService.initUser();
}

export function initCompanyFactory(companyService: CompanyService) {
    return () => companyService.initCompanies();
}

export function initStatusManagementErrorFactory(statusManagementErrorService: StatusManagementErrorService) {
    return () => statusManagementErrorService.initStatus();
}

export function initStatusEvaluationIntegrationFactory(statusEvaluationIntegrationService: StatusEvaluationIntegrationService) {
    return () => statusEvaluationIntegrationService.initStatus();
}

export function initStatusTopazMovementFactory(statusTopazMovementService: StatusTopazMovementService) {
    return () => statusTopazMovementService.initStatus();
}

export function initStatusMassiveRegistration(statusMassiveRegistration: StatusMassiveRegistrationService) {
    return () => statusMassiveRegistration.initStatus();
}


registerLocaleData(localePt);

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatSnackBarModule,
        ToastModule.forRoot(),
        HttpClientModule,
        AuthenticationModule,
        NgxPermissionsModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        DateUtils,
        UserSessionService,
        CompanyService,
        SystemService,
        MessageHandler,
        StatusManagementErrorService,
        PermissionService,
        StatusTopazMovementService,
        StatusEvaluationIntegrationService,
        StatusMassiveRegistrationService,
        ConfigService,
        {
            provide: LOCALE_ID, useValue: 'pt-BR'
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initUserFactory,
            multi: true,
            deps: [UserService]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initCompanyFactory,
            multi: true,
            deps: [CompanyService]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initStatusManagementErrorFactory,
            multi: true,
            deps: [StatusManagementErrorService]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initStatusEvaluationIntegrationFactory,
            multi: true,
            deps: [StatusEvaluationIntegrationService]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initStatusTopazMovementFactory,
            multi: true,
            deps: [StatusTopazMovementService]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: initStatusMassiveRegistration,
            multi: true,
            deps: [StatusMassiveRegistrationService]
        }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
