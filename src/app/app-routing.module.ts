import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthenticationGuard} from './configuration/authentication.guard';
import {LoginGuard} from './configuration/login.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: './layout/layout.module#LayoutModule',
        canActivate: [AuthenticationGuard]
    },
    {
        path: 'login',
        loadChildren: './login/login.module#LoginModule',
        canActivate: [LoginGuard]
    },
    {
        path: 'error',
        loadChildren: './server-error/server-error.module#ServerErrorModule',
        canActivate: [AuthenticationGuard]
    },
    {
        path: 'nao-autorizado',
        loadChildren: './access-denied/access-denied.module#AccessDeniedModule',
        canActivate: [AuthenticationGuard]
    },
    {
        path: 'nao-encontrado',
        loadChildren: './not-found/not-found.module#NotFoundModule',
        canActivate: [AuthenticationGuard]
    },
    {
        path: '**',
        redirectTo: 'nao-encontrado'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
