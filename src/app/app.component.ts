import {Component, OnInit, ViewContainerRef, ViewEncapsulation} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [
        './app.component.scss',
        '../../node_modules/@angular/material/prebuilt-themes/indigo-pink.css'
    ],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

    public viewContainerRef: ViewContainerRef;

    constructor(public toastr: ToastsManager, viewContainerRef: ViewContainerRef) {
        this.viewContainerRef = viewContainerRef;
        this.toastr.setRootViewContainerRef(viewContainerRef);
    }

    ngOnInit() {
    }
}
