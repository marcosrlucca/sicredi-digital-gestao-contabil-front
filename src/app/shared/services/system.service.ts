import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {isNullOrUndefined} from 'util';
import {System} from '../../layout/management-error/evaluation-integration/evaluation-integration.model';

@Injectable()
export class SystemService {

    private pathGetSystems = `${environment._api_accounting_management_url}/sistema`;
    public systems;

    constructor(private httpClient: HttpClient) {
        this.getSystems();
    }

    private getSystems() {
        if (isNullOrUndefined(this.systems)) {
            this.systems = [];

            this.httpClient.get(this.pathGetSystems).toPromise().then((a: Array<System>) => {
                if (!isNullOrUndefined(a)) {
                    a.forEach(b => this.systems.push({value: b['system'], viewValue: b['description']}));
                }
            }).catch(() => null);
        }

        return this.systems;
    }

}
