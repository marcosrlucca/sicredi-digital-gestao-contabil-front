import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

@Injectable()
export class ConfigService {

    private _api_authentication_url = environment._api_authentication_url;

    private _auth_url = this._api_authentication_url + '/auth';

    private _user_url = this._api_authentication_url + '/user';

    private _refresh_token_url = this._auth_url + '/refresh';

    private _login_url = this._auth_url + '/login';

    private _logout_url = this._auth_url + '/logout';

    private _whoami_url = this._user_url + '/whoami';

    get refresh_token_url(): string {
        return this._refresh_token_url;
    }

    get whoami_url(): string {
        return this._whoami_url;
    }

    get login_url(): string {
        return this._login_url;
    }

    get logout_url(): string {
        return this._logout_url;
    }


}
