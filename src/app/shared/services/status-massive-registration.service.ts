import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {TimerObservable} from 'rxjs/observable/TimerObservable';
import {isNullOrUndefined} from 'util';

@Injectable()
export class StatusMassiveRegistrationService implements OnDestroy {
    private pathCheckStatus = `${environment._api_inconsistence_massive}/massive-registration/count`;

    public subscription;
    public totalError;

    public errorChosed = false;

    constructor(private httpClient: HttpClient) {
    }

    initStatus() {
        if (!isNullOrUndefined(this.totalError)) {
            return;
        }

        let promise;

        /*5 minutes*/
        const timer = TimerObservable.create(0, 60000);

        this.subscription = timer.subscribe(t => {
            promise = this.httpClient.get(this.pathCheckStatus).toPromise()
                .then(s => this.totalError = s)
                .catch(() => null);
        });

        return promise;
    }

    updateStatus() {
        this.httpClient.get(this.pathCheckStatus).toPromise()
            .then(s => this.totalError = s);
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    chooseError() {
        this.errorChosed = true;
    }

    isErrorChosed() {
        if (this.errorChosed) {
            return true;
        }
        return false;
    }

    resetChoice() {
        this.errorChosed = false;
    }
}
