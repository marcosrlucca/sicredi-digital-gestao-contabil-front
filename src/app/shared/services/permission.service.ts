import {Injectable} from '@angular/core';
import {GlobalConstants} from '../domain/global.constants';

@Injectable()
export class PermissionService {

    constructor() {
    }

    public getDigContabilAdm(): string {
        return GlobalConstants.DIG_CONTABIL_ADM;
    }

    public getDigContabilConsulta(): string {
        return GlobalConstants.DIG_CONTABIL_CONSULTA;
    }

    public getDigContabilFiscalAdm(): string {
        return GlobalConstants.DIG_CONTABIL_FISCAL_ADM;
    }

    public getDigContabilFiscalConsulta(): string {
        return GlobalConstants.DIG_CONTABIL_FISCAL_CONSULTA;
    }

    public getDigContabilAprovador(): string {
        return GlobalConstants.DIG_CONTABIL_APROVADOR;
    }

    public getDigContabilFiscalAprovador(): string {
        return GlobalConstants.DIG_CONTABIL_FISCAL_APROVADOR;
    }
}
