import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {TimerObservable} from 'rxjs/observable/TimerObservable';
import {isNullOrUndefined} from 'util';

@Injectable()
export class StatusTopazMovementService implements OnDestroy {
    private pathStatusIntegrationEvaluation = `${environment._api_accounting_management_url}/topaz-movement/count-not-integrated`;

    public subscription;
    public totalNotIntegrated;

    public notIntegratedChosed = false;

    constructor(private httpClient: HttpClient) {
    }

    initStatus() {
        if (!isNullOrUndefined(this.totalNotIntegrated)) {
            return;
        }

        let promise;

        /*5 minutes*/
        const timer = TimerObservable.create(0, 60000);

        this.subscription = timer.subscribe(t => {
            promise = this.httpClient.get(this.pathStatusIntegrationEvaluation).toPromise()
                .then(s => this.totalNotIntegrated = s)
                .catch(() => null);
        });

        return promise;
    }

    updateStatus() {
        this.httpClient.get(this.pathStatusIntegrationEvaluation).toPromise()
            .then(s => this.totalNotIntegrated = s);
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    chooseNotIntegrated() {
        this.notIntegratedChosed = true;
    }

    isNotIntegratedChosed() {
        if (this.notIntegratedChosed) {
            return true;
        }
        return false;
    }

    resetChoice() {
        this.notIntegratedChosed = false;
    }
}
