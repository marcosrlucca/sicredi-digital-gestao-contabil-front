import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Status} from '../domain/status.model';
import {TimerObservable} from 'rxjs/observable/TimerObservable';
import {isNullOrUndefined} from 'util';

@Injectable()
export class StatusManagementErrorService implements OnDestroy {
    private _api_accounting_management_error_url = environment._api_accounting_management_url;
    private pathStatus = `${this._api_accounting_management_error_url}/avaliacao-lotes/status`;

    public status: Status;
    public subscription;

    public errorChosed = false;
    public disconsideredChosed = false;
    public notSentChosed = false;

    constructor(private httpClient: HttpClient) {
    }

    initStatus() {
        if (!isNullOrUndefined(this.status)) {
            return;
        }

        let promise;
        const timer = TimerObservable.create(0, 60000); /*1 minute*/

        this.subscription = timer.subscribe(t => {
            promise = this.httpClient.get(this.pathStatus).toPromise()
                .then((s: Status) => {
                    this.status = s;
                })
                .catch(() => null);
        });

        return promise;
    }

    updateStatus() {
        this.httpClient.get(this.pathStatus).toPromise()
            .then((s: Status) => {
                this.status = s;
            });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    chooseError() {
        this.errorChosed = true;
        this.notSentChosed = false;
    }

    chooseNotSent() {
        this.errorChosed = false;
        this.notSentChosed = true;
    }

    chooseDisconsidered() {
        this.errorChosed = false;
        this.notSentChosed = false;
        this.disconsideredChosed = true;
    }

    isAnyChoice() {
        if (this.errorChosed || this.notSentChosed || this.disconsideredChosed) {
            return true;
        }
        return false;
    }

    resetChoices() {
        this.errorChosed = false;
        this.notSentChosed = false;
        this.disconsideredChosed = false;
    }
}
