import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {isNullOrUndefined} from 'util';
import {Company} from '../../layout/management-error/allot-evaluation/allot-evaluation.model';

@Injectable()
export class CompanyService {

    private _api_url = environment._api_accounting_management_url;
    public pathGetCompany = `${this._api_url}/empresa`;

    public companies = [];

    constructor(private httpClient: HttpClient) {
    }

    public initCompanies() {
        if (this.companies.length > 0) {
            return;
        }

        const promise = this.httpClient.get(this.pathGetCompany).toPromise()
            .then((a: Array<Company>) => {
                if (!isNullOrUndefined(a)) {
                    a.forEach(b => this.companies.push({value: b['id'], viewValue: `${b['id']} - ${b['fantasyName']}`}));
                }
            }).catch(() => null);

        return promise;
    }

}
