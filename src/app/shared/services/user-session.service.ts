import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConfigService} from './config.service';

@Injectable()
export class UserSessionService {
    currentUser;
    token;

    constructor(private httpClient: HttpClient,
                private configService: ConfigService) {
    }

    login(user) {
        const body = `username=${user.username}&password=${user.password}`;
        return this.httpClient.post(this.configService.login_url, body).map(
            (response: Response) => {
                // login successful if there's a jwt token in the response
                const token = response['access_token'];

                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('access_token', this.token);

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            }
        );
    }

    logout() {
        localStorage.setItem('access_token', null);

        return this.httpClient.post(this.configService.logout_url, {}).map(() => this.currentUser = null);
    }
}
