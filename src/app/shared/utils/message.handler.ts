import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class MessageHandler {

    constructor(public snackbar: MatSnackBar) {
    }

    public handleError(err: HttpErrorResponse) {
        this.snackbar.open(err.error.msg, 'Fechar');
    }
}
