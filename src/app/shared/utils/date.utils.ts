import {isNullOrUndefined} from 'util';
import {Injectable} from '@angular/core';

@Injectable()
export class DateUtils {

    public parseStringDateToMaterial(date: string) {
        if (date.length !== 10) {
            return;
        }

        const yearStartDate = date.substring(0, 4);
        const monthStartDate = date.substring(5, 7);
        const dayStartDate = date.substring(8, 10);

        return new Date(`${monthStartDate}/${dayStartDate}/${yearStartDate}`);
    }

    public parseStringDateToLocalePtBR(date: string) {
        if (date.length !== 10) {
            return;
        }

        const yearStartDate = date.substring(0, 4);
        const monthStartDate = date.substring(5, 7);
        const dayStartDate = date.substring(8, 10);

        return `${dayStartDate}/${monthStartDate}/${yearStartDate}`;
    }

    public parseDateToLocaleEnUS(dateToTransform) {
        if (!isNullOrUndefined(dateToTransform) && dateToTransform !== '') {
            let dayStartDate = dateToTransform.getDate();
            let monthStartDate = dateToTransform.getMonth() + 1;
            const yearStartDate = dateToTransform.getFullYear();

            if (dayStartDate < 10) {
                dayStartDate = '0' + dayStartDate;
            }
            if (monthStartDate < 10) {
                monthStartDate = '0' + monthStartDate;
            }

            return `${yearStartDate}-${monthStartDate}-${dayStartDate}`;
        }

        return null;
    }

}
