export class Page {
    content: any[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElementsPerPage: number;
    size: number;
    sort: any;
    totalElements: number;
    totalPages: number;
    pageSizeOptions = [];
}

export interface PageResponse {
    content: any[];
    pageable: Pageable;
    totalElements: number;
    last: boolean;
    totalPages: boolean;
    size: number;
    number: number;
    sort: Sort;
    numberOfElements: number;
    first: boolean;
}

export interface Pageable {
    sort: Sort;
    offset: number;
    pageSize: number;
    pageNumber: number;
    paged: boolean;
    unpaged: boolean;
}

export interface Sort {
    sorted: boolean;
    unsorted: boolean;
}
