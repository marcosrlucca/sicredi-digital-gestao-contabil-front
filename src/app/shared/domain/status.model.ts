export class Status {
    totalError: number;
    totalSuccess: number;
    totalNotSent: number;
    totalDisconsidered: number;
    totalLogKettle: number;
}
