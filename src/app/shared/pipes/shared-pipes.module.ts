import {NgModule} from '@angular/core';
import {DateFormatPipe} from './date-format.pipe';

@NgModule({
    exports: [
        DateFormatPipe
    ],
    declarations: [
        DateFormatPipe
    ]
})
export class SharedPipesModule { }
