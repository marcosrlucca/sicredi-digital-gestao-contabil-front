import {NgModule} from '@angular/core';
import {
    MatButtonModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule, MatNativeDateModule, MatTabsModule, MatPaginatorIntl, MatPaginatorModule,
    MatProgressSpinnerModule, MatRadioModule, MatSelectModule, MatSortModule, MatTableModule,
    MatTooltipModule
} from '@angular/material';
import {MatPaginatorIntlBr} from '../../services/mat-paginator-intl-br';

@NgModule({
    imports: [
        MatNativeDateModule,
        MatInputModule,
        MatFormFieldModule,
        MatDialogModule,
        MatTabsModule,
        MatButtonModule,
        MatTooltipModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSelectModule,
        MatSortModule,
        MatPaginatorModule
    ],
    exports: [
        MatNativeDateModule,
        MatInputModule,
        MatFormFieldModule,
        MatDialogModule,
        MatTabsModule,
        MatButtonModule,
        MatTooltipModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatNativeDateModule,
        MatDatepickerModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSelectModule,
        MatSortModule,
        MatPaginatorModule
    ],
    providers: [
        {provide: MatPaginatorIntl, useClass: MatPaginatorIntlBr}
    ]
})
export class MaterialSharedModule {
}
