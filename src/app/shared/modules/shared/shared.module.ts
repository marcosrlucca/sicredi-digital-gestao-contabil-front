import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxPermissionsModule} from 'ngx-permissions';
import {SharedPipesModule} from '../..';

@NgModule({
    imports: [
        CommonModule,
        NgxPermissionsModule,
        SharedPipesModule
    ],
    exports: [
        CommonModule,
        NgxPermissionsModule,
        SharedPipesModule
    ]
})
export class SharedModule {
}
