import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
    @Input() urlFirstLevel: string;
    @Input() titleFirstLevel: string;
    @Input() iconFirstLevel: string;

    @Input() urlSecondLevel: string;
    @Input() titleSecondLevel: string;
    @Input() iconSecondLevel: string;

    @Input() titleCurrent: string;
    @Input() iconCurrent: string;

    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    goToLinkFirstLevel() {
        this.router.navigate([this.urlFirstLevel]);
    }

    goToLinkSecondLevel() {
        this.router.navigate([this.urlSecondLevel]);
    }
}
