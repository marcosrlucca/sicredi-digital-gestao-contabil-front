import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent} from '@angular/common/http';
import {Router} from '@angular/router';
import {HttpStatus} from '../shared/domain/http-status';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Observable';
import {MessageHandler} from '../shared/utils/message.handler';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

    constructor(private errorHandler: MessageHandler,
                private router: Router) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler) {
        const isLoginRequest = request.url.indexOf('auth') >= 0;

        request = request.clone({
            setHeaders: {
                transactionId: '123',
                'App-Name': 'sicredi-digital-gestao-contabil-frontend'
            }
        });

        if (isLoginRequest) {
            request = request.clone({
                setHeaders: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
        } else {
            request = request.clone({
                setHeaders: {
                    'Content-Type': 'application/json',
                    withCredentials: 'true',
                    'AUTH-TOKEN': localStorage.getItem('access_token')
                }
            });
        }

        return next.handle(request)
            .do((event: HttpEvent<any>) => {
            }, (err: any) => {
                if (err instanceof HttpErrorResponse &&
                    (err.status === HttpStatus.BAD_REQUEST || err.status === HttpStatus.INTERNAL_SERVER_ERROR)) {
                    this.errorHandler.handleError(err);
                }
            })
            .catch((error, caught) => {
                if (error.status === HttpStatus.UNAUTHORIZED) {
                    this.router.navigate(['nao-autorizado']);
                } else if (error.status === HttpStatus.NOT_FOUND) {
                    this.router.navigate(['nao-encontrado']);
                }
                return Observable.throw(error);
            }) as any;
    }
}
