import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {UserSessionService} from '../shared/services/user-session.service';

@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(private router: Router,
                private userSessionService: UserSessionService) {
    }

    canActivate(): boolean {
        if (this.userSessionService.currentUser) {
            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
