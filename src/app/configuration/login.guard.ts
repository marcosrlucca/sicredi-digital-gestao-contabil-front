import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {UserSessionService} from '../shared/services/user-session.service';

@Injectable()
export class LoginGuard implements CanActivate {

    constructor(private router: Router, private userSessionService: UserSessionService) {
    }

    canActivate(): boolean {
        if (this.userSessionService.currentUser) {
            this.router.navigate(['/']);
            return false;
        } else {
            return true;
        }
    }
}
