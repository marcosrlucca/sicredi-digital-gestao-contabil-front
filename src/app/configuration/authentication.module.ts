import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {NgModule} from '@angular/core';

import {AuthenticationGuard} from './authentication.guard';
import {AuthenticationInterceptor} from './authentication.interceptor';
import {LoginGuard} from './login.guard';
import {UserService} from './user.service';
import {CommonModule} from '@angular/common';
import {UserSessionService} from '../shared/services/user-session.service';
import {ConfigService} from '../shared/services/config.service';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    entryComponents: [],
    providers: [
        UserSessionService,
        ConfigService,
        AuthenticationGuard,
        LoginGuard,
        UserService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthenticationInterceptor,
            multi: true
        }
    ],
})
export class AuthenticationModule {
}
