import {Injectable} from '@angular/core';
import {UserSessionService} from '../shared/services/user-session.service';
import {ConfigService} from '../shared/services/config.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Authorization} from '../shared/domain/authorization.model';
import {isNullOrUndefined} from 'util';

@Injectable()
export class UserService {

    currentUser;
    authorities: Authorization;

    constructor(private httpClient: HttpClient,
                private userSessionService: UserSessionService,
                private config: ConfigService) {
    }

    initUser() {
        const promise = this.httpClient.get(this.config.refresh_token_url).toPromise()
            .then(res => {
                if (res !== null) {
                    return this.getMyInfo().toPromise()
                        .then(user => {
                            this.currentUser = user;
                            this.userSessionService.currentUser = user;
                        });
                }
            })
            .catch(() => null);
        return promise;
    }

    getMyInfo() {
        return this.httpClient.get(this.config.whoami_url).map(user => this.currentUser = user);
    }

    getPermissions() {
        this.authorities = new Authorization();
        this.authorities.permission = [];

        const authoritiesTmp: string[] = this.currentUser.authorities;

        authoritiesTmp.forEach(a => {
            this.authorities.permission.push(a['authority'].toUpperCase());
        });

        return Observable.of(this.authorities);
    }

}
