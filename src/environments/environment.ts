export const environment = {
    production: true,
    _api_authentication_url: 'http://sicredi-product-backoffice.dev-sicredi.in/api/v1',
    _api_accounting_management_url: 'http://sicredi-accounting-management-backend.dev-sicredi.in/api/v1',
    _api_accounting_management_parameterization_url: 'http://localhost:8081',
    _api_accounting_evaluation_integration_process: 'http://accounting-evaluation-integration-process.dev-sicredi.in/api/v1',
    _api_accounting_act_not_act_url: 'http://accounting-act-not-act.dev-sicredi.in/api/v1',
    _api_engine_system_par: 'http://accounting-parameterization-system.dev-sicredi.in/api/v1',
    _api_inconsistence_massive: 'http://inconsistent-massive-registration.dev-sicredi.in/api/v1',
    _api_value_type: 'http://accounting-value-type.dev-sicredi.in/api/v1',
    _api_backoffice_status: 'http://accounting-backoffice-status.dev-sicredi.in/api/v1'
};
