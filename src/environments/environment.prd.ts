export const environment = {
    production: true,
    _api_authentication_url: 'https://sicredi-product-backoffice.prd-sicredi.in/api/v1',
    _api_accounting_management_url: 'https://sicredi-accounting-management-backend.prd-sicredi.in/api/v1',
    _api_accounting_management_parameterization_url: 'https://accounting-management-parameterization.prd-sicredi.in/api/v1',
    _api_accounting_evaluation_integration_process: 'https://accounting-evaluation-integration-process.prd-sicredi.in/api/v1',
    _api_accounting_act_not_act_url: 'https://accounting-act-not-act.prd-sicredi.in/api/v1',
    _api_engine_system_par: 'https://accounting-parameterization-system.prd-sicredi.in/api/v1',
    _api_inconsistence_massive: 'https://inconsistent-massive-registration.prd-sicredi.in/api/v1',
    _api_value_type: 'https://accounting-value-type.prd-sicredi.in/api/v1',
    _api_backoffice_status: 'https://accounting-backoffice-status.prd-sicredi.in/api/v1'
};
