export const environment = {
    production: true,
    _api_authentication_url: 'https://sicredi-product-backoffice.uat-sicredi.in/api/v1',
    _api_accounting_management_url: 'https://sicredi-accounting-management-backend.uat-sicredi.in/api/v1',
    _api_accounting_management_parameterization_url: 'https://accounting-management-parameterization.uat-sicredi.in/api/v1',
    _api_accounting_evaluation_integration_process: 'https://accounting-evaluation-integration-process.uat-sicredi.in/api/v1',
    _api_accounting_act_not_act_url: 'https://accounting-act-not-act.uat-sicredi.in/api/v1',
    _api_engine_system_par: 'https://accounting-parameterization-system.uat-sicredi.in/api/v1',
    _api_inconsistence_massive: 'https://inconsistent-massive-registration.uat-sicredi.in/api/v1',
    _api_value_type: 'https://accounting-value-type.uat-sicredi.in/api/v1',
    _api_backoffice_status: 'https://accounting-backoffice-status.uat-sicredi.in/api/v1'
};
