import { Injectable } from '@angular/core';
import * as moment from 'moment';

const QUANTITY_OF_DAYS = 15;

@Injectable()
export class DateUtilService {

  getDateString(date: Date) {
    return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`;
  }

  formatDate(date: string) {
      return moment(date, 'DD/MM/YYYY').toDate();
  }

  formatMomentDate(date: string) {
      return moment(date, 'YYYY-MM-DD').toDate();
  }

  startDateIsBeforefinalDate(startDate: Date, finalDate: Date) {
      if (moment(startDate).isBefore(finalDate)) {
          return true;
      }
      return false;
  }

  startDateIsGreatherThanfinalDate(startDate: Date, finalDate: Date) {
    if (moment(startDate).isAfter(finalDate)) {
        return true;
    }
    return false;
  }

  dateIsValid(startDate: Date, finalDate: Date) {
    const isValid = moment(startDate).add(QUANTITY_OF_DAYS, 'days');
    if (moment(startDate).isBefore(finalDate) && moment(finalDate).isSameOrBefore(isValid, 'day')) {
        return true;
    }
  }
}
